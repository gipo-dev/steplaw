<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['as' => 'page.'], function () {

    Route::get('/', [\App\Http\Controllers\IndexController::class, 'index'])->name('index');

    Route::group(['prefix' => 'uslugi', 'as' => 'services.'], function () {

        Route::get('/', [\App\Http\Controllers\ServiceController::class, 'all'])->name('all');
        Route::get('{category}/', [\App\Http\Controllers\ServiceCategoryController::class, 'index'])->name('category');
        Route::get('{category}/{service}/', [\App\Http\Controllers\ServiceController::class, 'index'])->name('service');
        Route::get(
            '{category}/{subcategory1}/{subcategory2}/{subcategory3?}/{subcategory4?}/',
            [\App\Http\Controllers\ServiceController::class, 'index']
        )->middleware([\App\Http\Middleware\ServiceMiddleware::class])->name('service.subcategory');

    });

    Route::group(['prefix' => 'advokaty', 'as' => 'persons.'], function () {

        Route::get('/', [\App\Http\Controllers\PersonController::class, 'all'])->name('all');
        Route::get('/{person}', [\App\Http\Controllers\PersonController::class, 'index'])->name('person');

    });

    Route::group(['prefix' => 'cases', 'as' => 'cases.'], function () {

        Route::get('/', [\App\Http\Controllers\CaseController::class, 'all'])->name('all');
        Route::get('/{case}/', [\App\Http\Controllers\CaseController::class, 'index'])->name('case');

    });

    Route::group(['prefix' => 'questions', 'as' => 'qa.'], function () {

        Route::get('/', [\App\Http\Controllers\QaController::class, 'all'])->name('all');
        Route::get('ajax', [\App\Http\Controllers\QaController::class, 'ajax'])->name('ajax');
        Route::get('/{qa}/', [\App\Http\Controllers\QaController::class, 'index'])->name('item');

    });

    Route::group(['prefix' => 'contacts', 'as' => 'contacts.'], function () {

        Route::get('/', [\App\Http\Controllers\ContactsController::class, 'index'])->name('index');

    });

    Route::group(['prefix' => 'journal', 'as' => 'posts.'], function () {

        Route::get('/', [\App\Http\Controllers\PostCategoryController::class, 'all'])->name('all');
        Route::get('tag/{tag}/', [\App\Http\Controllers\TagController::class, 'index'])->name('tag');
        Route::get('{category}/', [\App\Http\Controllers\PostCategoryController::class, 'index'])->name('category');
        Route::get('{category}/{post}', [\App\Http\Controllers\PostController::class, 'index'])->name('post');

    });

    Route::get('/about/', [\App\Http\Controllers\PageController::class, 'default'])->name('about');
    Route::get('/history/', [\App\Http\Controllers\PageController::class, 'default'])->name('officeHistory');

    Route::group(['prefix' => 'payment', 'as' => 'payment.'], function () {
        Route::get('pay', [\App\Http\Controllers\PaymentController::class, 'pay'])->name('pay');
    });
});

Route::group(['prefix' => 'modal', 'as' => 'modal.'], function () {

    Route::get('partner', [\App\Http\Controllers\PartnerController::class, 'modal'])->name('partner');

});

Route::group(['prefix' => 'blocks', 'as' => 'blocks.'], function () {

    Route::get('services/tab/{service}', [\App\Http\Controllers\ServiceController::class, 'tab'])->name('services.tab');
    Route::get('services/social', [\App\Http\Controllers\ServiceController::class, 'social'])->name('services.social');
    Route::get('services/{category?}', [\App\Http\Controllers\ServiceCategoryController::class, 'list'])->name('services');
    Route::get('services/related/{category}', [\App\Http\Controllers\ServiceCategoryController::class, 'related'])->name('services.related');
    Route::get('services/faq/{service}/{category}', [\App\Http\Controllers\FaqController::class, 'list'])->name('services.faq');
    Route::get('services/person/{person}', [\App\Http\Controllers\PersonController::class, 'big'])->name('services.person');
    Route::get('mentioning/{year?}', [\App\Http\Controllers\MentioningController::class, 'list'])->name('mentioning');
    Route::post('callback/store', [\App\Http\Controllers\CallbackController::class, 'store'])->name('callback.store');
    Route::get('thank/modal', [\App\Http\Controllers\ThankContoller::class, 'modal'])->name('thank.modal');
    Route::get('thank/carousel', [\App\Http\Controllers\ThankContoller::class, 'carousel'])->name('thank.carousel');
    Route::get('case/modal', [\App\Http\Controllers\CaseController::class, 'modal'])->name('case.modal');
    Route::get('help', [\App\Http\Controllers\ServiceController::class, 'help'])->name('help');
});

Route::group(['prefix' => 'comments', 'as' => 'comments.'], function () {
    Route::post('{post}/get', [\App\Http\Controllers\CommentController::class, 'get'])->name('get');
    Route::post('store', [\App\Http\Controllers\CommentController::class, 'store'])->name('store');
});

Route::get('yandex_130936c9dc22b6e7.html', function () {
    return view('pages.verification');
});
