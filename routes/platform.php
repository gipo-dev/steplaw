<?php

declare(strict_types=1);

use App\Http\Controllers\Admin\EditorController;
use App\Http\Controllers\Admin\GalleryController;
use App\Orchid\CrudRoutes;
use App\Orchid\Screens\Callback\CallbackListScreen;
use App\Orchid\Screens\Callback\CallbackViewScreen;
use App\Orchid\Screens\CasePage\CasePageEditScreen;
use App\Orchid\Screens\CasePage\CasePageListScreen;
use App\Orchid\Screens\Page\BlockEditScreen;
use App\Orchid\Screens\QA\QaEditScreen;
use App\Orchid\Screens\QA\QaListScreen;
use App\Orchid\Screens\Comment\CommentEditScreen;
use App\Orchid\Screens\Comment\CommentListScreen;
use App\Orchid\Screens\FaqTag\FaqTagEditScreen;
use App\Orchid\Screens\FaqTag\FaqTagListScreen;
use App\Orchid\Screens\Mention\MentionEditScreen;
use App\Orchid\Screens\Mention\MentionListScreen;
use App\Orchid\Screens\MentionSource\MentionSourceEditScreen;
use App\Orchid\Screens\MentionSource\MentionSourceListScreen;
use App\Orchid\Screens\Page\PageEditScreen;
use App\Orchid\Screens\Page\PageListScreen;
use App\Orchid\Screens\Person\PersonEditScreen;
use App\Orchid\Screens\Person\PersonListScreen;
use App\Orchid\Screens\PhotoGroup\PhotoGroupEditScreen;
use App\Orchid\Screens\PhotoGroup\PhotoGroupListScreen;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Post\PostEditScreen;
use App\Orchid\Screens\Post\PostListScreen;
use App\Orchid\Screens\PostCategory\PostCategoryEditScreen;
use App\Orchid\Screens\PostCategory\PostCategoryListScreen;
use App\Orchid\Screens\PhotoReview\PhotoReviewEditScreen;
use App\Orchid\Screens\PhotoReview\PhotoReviewListScreen;
use App\Orchid\Screens\Review\ReviewEditScreen;
use App\Orchid\Screens\Review\ReviewListScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\Service\ServiceEditScreen;
use App\Orchid\Screens\Service\ServiceListScreen;
use App\Orchid\Screens\ServiceCategory\ServiceCategoryEditScreen;
use App\Orchid\Screens\ServiceCategory\ServiceCategoryListScreen;
use App\Orchid\Screens\Setting\SettingScreen;
use App\Orchid\Screens\Faq\FaqEditScreen;
use App\Orchid\Screens\Faq\FaqListScreen;
use App\Orchid\Screens\Thank\ThankEditScreen;
use App\Orchid\Screens\Thank\ThankListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use App\Orchid\Screens\User\UserProfileScreen;
use App\Orchid\Screens\Video\VideoEditScreen;
use App\Orchid\Screens\Video\VideoListScreen;
use App\Orchid\Screens\WorkCase\WorkCaseEditScreen;
use App\Orchid\Screens\WorkCase\WorkCaseListScreen;
use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)
    ->name('platform.main');

// Platform > Profile
Route::screen('profile', UserProfileScreen::class)
    ->name('platform.profile')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Profile'), route('platform.profile'));
    });

// Platform > System > Users
Route::screen('users/{user}/edit', UserEditScreen::class)
    ->name('platform.systems.users.edit');

// Platform > System > Users > Create
Route::screen('users/create', UserEditScreen::class)
    ->name('platform.systems.users.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.users')
            ->push(__('Create'), route('platform.systems.users.create'));
    });

// Platform > System > Users > User
Route::screen('users', UserListScreen::class)
    ->name('platform.systems.users')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Users'), route('platform.systems.users'));
    });

// Platform > System > Roles > Role
Route::screen('roles/{roles}/edit', RoleEditScreen::class)
    ->name('platform.systems.roles.edit')
    ->breadcrumbs(function (Trail $trail, $role) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Role'), route('platform.systems.roles.edit', $role));
    });

// Platform > System > Roles > Create
Route::screen('roles/create', RoleEditScreen::class)
    ->name('platform.systems.roles.create')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.systems.roles')
            ->push(__('Create'), route('platform.systems.roles.create'));
    });

// Platform > System > Roles
Route::screen('roles', RoleListScreen::class)
    ->name('platform.systems.roles')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push(__('Roles'), route('platform.systems.roles'));
    });

// Platform > Services
Route::screen('services', ServiceListScreen::class)
    ->name('platform.services')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Услуги', route('platform.services'));
    });

// Platform > Services > Edit
Route::screen('services/{service}/edit', ServiceEditScreen::class)
    ->name('platform.services.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.services')
            ->push('Редактирование');
    });

// Platform > Services > Add
Route::screen('services/add', ServiceEditScreen::class)
    ->name('platform.services.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.services')
            ->push('Новая услуга');
    });

// Platform > Service categories
Route::screen('service-categories', ServiceCategoryListScreen::class)
    ->name('platform.service_categories')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Категории услуг', route('platform.service_categories'));
    });

// Platform > Service categories > Edit
Route::screen('service-categories/{category}/edit', ServiceCategoryEditScreen::class)
    ->name('platform.service_categories.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.service_categories')
            ->push('Редактирование');
    });

// Platform > Service categories > Add
Route::screen('service-categories/add', ServiceCategoryEditScreen::class)
    ->name('platform.service_categories.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.service_categories')
            ->push('Новая категория услуг');
    });

// Platform > Persons
Route::screen('persons', PersonListScreen::class)
    ->name('platform.persons')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Адвокаты', route('platform.persons'));
    });

// Platform > Persons > Edit
Route::screen('persons/{person}/edit', PersonEditScreen::class)
    ->name('platform.persons.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.persons')
            ->push('Редактирование');
    });

// Platform > Persons > Add
Route::screen('persons/add', PersonEditScreen::class)
    ->name('platform.persons.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.persons')
            ->push('Новый адвокат');
    });

// Platform > Reviews
Route::screen('reviews', ReviewListScreen::class)
    ->name('platform.reviews')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Адвокаты', route('platform.reviews'));
    });

// Platform > Reviews > Edit
Route::screen('reviews/{review}/edit', ReviewEditScreen::class)
    ->name('platform.reviews.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.reviews')
            ->push('Редактирование');
    });

// Platform > Reviews > Add
Route::screen('reviews/add', ReviewEditScreen::class)
    ->name('platform.reviews.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.reviews')
            ->push('Новый отзыв');
    });

// Platform > Cases
Route::screen('cases', WorkCaseListScreen::class)
    ->name('platform.cases')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Кейсы', route('platform.cases'));
    });

// Platform > Cases > Edit
Route::screen('cases/{case}/edit', WorkCaseEditScreen::class)
    ->name('platform.cases.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.cases')
            ->push('Редактирование');
    });

// Platform > Cases > Add
Route::screen('cases/add', WorkCaseEditScreen::class)
    ->name('platform.cases.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.cases')
            ->push('Новый кейс');
    });

// Platform > Photo Reviews
CrudRoutes::routes('photoreview', 'photoreviews',
    PhotoReviewListScreen::class, PhotoReviewEditScreen::class);

// Platform > Case Pages
CrudRoutes::routes('casepage', 'casepages',
    CasePageListScreen::class, CasePageEditScreen::class);

// Platform > Thanks
Route::screen('thanks', ThankListScreen::class)
    ->name('platform.thanks')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Благодарности', route('platform.thanks'));
    });

// Platform > Thanks > Edit
Route::screen('thanks/{thank}/edit', ThankEditScreen::class)
    ->name('platform.thanks.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.thanks')
            ->push('Редактирование');
    });

// Platform > Thanks > Add
Route::screen('thanks/add', ThankEditScreen::class)
    ->name('platform.thanks.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.thanks')
            ->push('Новая благодарность');
    });

// Platform > Mention Source
Route::screen('mention_sources', MentionSourceListScreen::class)
    ->name('platform.mention_sources')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Источники СМИ', route('platform.mention_sources'));
    });

// Platform > Mention Source > Edit
Route::screen('mention_sources/{mention_source}/edit', MentionSourceEditScreen::class)
    ->name('platform.mention_sources.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.mention_sources')
            ->push('Редактирование');
    });

// Platform > Mention Source > Add
Route::screen('mention_sources/add', MentionSourceEditScreen::class)
    ->name('platform.mention_sources.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.mention_sources')
            ->push('Новый источник СМИ');
    });

// Platform > Mention
Route::screen('mentions', MentionListScreen::class)
    ->name('platform.mentions')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Пуликации в СМИ', route('platform.mentions'));
    });

// Platform > Mention > Edit
Route::screen('mentions/{mention}/edit', MentionEditScreen::class)
    ->name('platform.mentions.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.mentions')
            ->push('Редактирование');
    });

// Platform > Mention > Add
Route::screen('mentions/add', MentionEditScreen::class)
    ->name('platform.mentions.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.mentions')
            ->push('Новая публикация');
    });

// Platform > Video > Edit
Route::screen('videos/{video}/edit', VideoEditScreen::class)
    ->name('platform.videos.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.videos')
            ->push('Редактирование');
    });

// Platform > Video > Add
Route::screen('videos/add', VideoEditScreen::class)
    ->name('platform.videos.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.videos')
            ->push('Новое видео');
    });

// Platform > Video
Route::screen('videos', VideoListScreen::class)
    ->name('platform.videos')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Видео', route('platform.videos'));
    });

// Platform > Page > Edit
Route::screen('pages/{page}/edit', PageEditScreen::class)
    ->name('platform.pages.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.pages')
            ->push('Редактирование');
    });

// Platform > Page > Add
Route::screen('pages/add', BlockEditScreen::class)
    ->name('platform.pages.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.pages')
            ->push('Новая страница');
    });

// Platform > Page
Route::screen('pages', PageListScreen::class)
    ->name('platform.pages')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Страница', route('platform.pages'));
    });

// Platform > Post > Edit
Route::screen('posts/{page}/edit', PostEditScreen::class)
    ->name('platform.posts.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.posts')
            ->push('Редактирование');
    });

// Platform > Post > Add
Route::screen('posts/add', PostEditScreen::class)
    ->name('platform.posts.add')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.posts')
            ->push('Новая публикация');
    });

// Platform > Post
Route::screen('posts', PostListScreen::class)
    ->name('platform.posts')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Публикации', route('platform.posts'));
    });

// Platform > Post Category
CrudRoutes::routes('post_category', 'post_categories', PostCategoryListScreen::class, PostCategoryEditScreen::class);

// Platform > Photo Group
CrudRoutes::routes('album', 'albums', PhotoGroupListScreen::class, PhotoGroupEditScreen::class);

// Platform > Comment
CrudRoutes::routes('comment', 'comments', CommentListScreen::class, CommentEditScreen::class);

// Platform > Faq
CrudRoutes::routes('faq', 'faqs', FaqListScreen::class, FaqEditScreen::class);

// Platform > Faq Tag
CrudRoutes::routes('faqtag', 'faqtags', FaqTagListScreen::class, FaqTagEditScreen::class);

// Platform > QA
CrudRoutes::routes('qa', 'qas', QaListScreen::class, QaEditScreen::class);

// Platform > Callback > Edit
Route::screen('callbacks/{callback}/edit', CallbackViewScreen::class)
    ->name('platform.callbacks.edit')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.callbacks')
            ->push('Просмотр');
    });

// Platform > Callback
Route::screen('callbacks', CallbackListScreen::class)
    ->name('platform.callbacks')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Запросы обратного звонка', route('platform.callbacks'));
    });

// Platform > Settings
Route::screen('settings', SettingScreen::class)
    ->name('platform.settings')
    ->breadcrumbs(function (Trail $trail) {
        return $trail
            ->parent('platform.index')
            ->push('Настройки');
    });


//Route::screen('idea', 'Idea::class','platform.screens.idea');


Route::group(['as' => 'platform.'], function () {
    Route::get('block/options', [EditorController::class, 'options'])->name('block.options');
    Route::get('block/render', [EditorController::class, 'render'])->name('block.render');
});
