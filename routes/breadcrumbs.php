<?php

use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

// Главная
Breadcrumbs::for('index', function (BreadcrumbTrail $trail) {
    $trail->push(__('breadcrumbs.index'), route('page.index'));
});

// Главная > Услуги
Breadcrumbs::for('services', function (BreadcrumbTrail $trail) {
    $trail->parent('index');
    $trail->push(__('breadcrumbs.services'), route('page.services.all'));
});

// Главная > Услуги > [Категория услуги]
Breadcrumbs::for('services.category', function (BreadcrumbTrail $trail, \App\Models\ServiceCategory $category) {
    $trail->push($category->name, route('page.services.category', $category));
});

// Главная > Услуги > [Услуга]
Breadcrumbs::for('services.service', function (BreadcrumbTrail $trail, \App\Models\ServiceCategory $category, \App\Models\Service $service) {
    $trail->parent('services');
    $trail->parent('services.category', $category);
    $trail->push($service->name, route('page.services.service', compact('category', 'service')));
});
