<?php

namespace Tests\Unit;

use App\Listeners\Callback\CheckSpam;
use PHPUnit\Framework\TestCase;

class CallbackSpamTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_callback_spam_function()
    {
        $checker = new CheckSpam();
        $this->assertFalse($checker->isPhoneSpam('+7 (958) 588-25-99'));
        $this->assertFalse($checker->isPhoneSpam('+7 (899) 988-25-9922'));
        $this->assertTrue($checker->isPhoneSpam('8 999 721 9005'));
        $this->assertTrue($checker->isPhoneSpam('89997219005'));
        $this->assertTrue($checker->isPhoneSpam('fffheaven@gmail.com'));
    }
}
