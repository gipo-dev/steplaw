<div class="page_service_long__persons__current">
    <div class="page_service_long__persons__current__info__wrap">
        <div class="page_service_long__persons__current__photo__wrap">
            <a href="{{ route('page.persons.person', $person) }}"
               class="page_service_long__persons__current__photo">
                <img src="{{ $person->getFirstMediaUrl('face') }}" alt="{{ $person->name }}">
            </a>
        </div>
        <div class="page_service_long__persons__current__info">
            <a href="{{ route('page.persons.person', $person) }}"
               class="page_service_long__persons__current__name">
                {{ $person->name }}
            </a>
            <p class="page_service_long__persons__current__post">{{ $person->about }}</p>
            <p class="page_service_long__persons__current__title">
                Специализация
            </p>
            <div class="page_service_long__persons__current__list">
                {!! $person->page['body']['profile'] ?? '' !!}
                {{--            <li>Споры с участием средств массовой информации, дела о клевете, защите чести и достоинства</li>--}}
                {{--            <li>Уголовное право</li>--}}
                {{--            <li>Корпоративные споры, осложненные уголовно-правовым элементом</li>--}}
                {{--            <li>Споры, осложненные иностранным элементом</li>--}}
            </div>
        </div>
    </div>
    <div class="page_service_long__persons__current__callback">
        <p class="page_service_long__persons__current__callback__title">
            Контактная информация
        </p>
        <p class="page_service_long__persons__current__callback__time">Удобное время для связи:
            ежедневно с 10:00 до 20:00</p>
        <div class="page_service_long__persons__current__callback__buttons">
            <a href="{{ route('page.persons.person', $person) }}" class="page_service_long__persons__current__callback__btn">
                Подробнее
            </a>
        </div>
        <div class="page_service_long__persons__current__callback__buttons">
            <a class="page_service_long__persons__current__callback__btn _primary modal-callback-open">
                Связаться
            </a>
        </div>
    </div>
</div>
