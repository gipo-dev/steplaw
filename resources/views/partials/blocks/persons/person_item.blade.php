<a href="{{ route('page.persons.person', $person) }}" class="splide__slide__container block text-center person">
    <div class="person__image"
         style="background-image:url('{{ $person->getFirstMediaUrl('image') }}');"></div>
    <p class="font-semibold text-primary text-xl mb-1 person__name">
        {{ $person->name }}
    </p>
    <p class="font-medium mb-1">
        {{ $person->position }}
    </p>
    <p class="person__about whitespace-pre-line">{{ $person->about }}</p>
</a>
