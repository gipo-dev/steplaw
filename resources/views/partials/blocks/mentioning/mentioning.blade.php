<ul class="flex flex-wrap justify-center leading-none text-lg
        text-primary font-medium mb-2 mentioning__categories"
    data-url="{{ route('blocks.mentioning') }}">
    @foreach(\App\Models\Mention::get('year')->unique('year')->pluck('year')->sortDesc() as $year)
        <li>
            <span data-year="{{ $year }}" class="mx-3 border-primary inline-block mb-3 cursor-pointer">
                {{ $year }}
            </span>
        </li>
    @endforeach
</ul>
@include('partials.blocks.mentioning.mentioning_wrap')
