<a href="{{ $mention->link ?? '' }}" target="_blank" class="flex bg-white p-2 py-4 md:p-5 mention">
    <div class="w-2/5 mr-5 mention__img">
        <img src="{{ $mention->image }}" alt="{{ $mention->name }}">
    </div>
    <div class="w-3/5 text-sm md:text-lg flex items-center">
        {{ $mention->text }}
    </div>
</a>
