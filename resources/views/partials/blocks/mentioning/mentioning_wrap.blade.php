<div class="mentioning__wrap {{ \Agent::isMobile() ? '' : 'flex flex-wrap justify-start' }}">
    @if(\Agent::isMobile())
        <ul class="mentioning__carousel">
            @foreach($mentions->chunk(4) as $chunk)
                <li class="w-full md:w-1/2">
                    <div class="flex flex-wrap splide__slide__container">
                        @foreach($chunk->sortByDesc('id')->take(10) as $mention)
                            <div class="w-full py-2 md:p-4">
                                @include('partials.blocks.mentioning.mention_item')
                            </div>
                        @endforeach
                    </div>
                </li>
            @endforeach
        </ul>
        <div class="splide__arrows text-primary mt-2">
            <button class="splide__arrow splide__arrow--prev">
                @include('partials.icons.arrow_rigth')
            </button>
            <button class="splide__arrow splide__arrow--next">
                @include('partials.icons.arrow_rigth')
            </button>
        </div>
    @else
        @foreach($mentions->sortByDesc('id')->take(12) as $mention)
            <div class="md:w-1/2 py-2 md:p-4">
                @include('partials.blocks.mentioning.mention_item')
            </div>
        @endforeach
    @endif
</div>
