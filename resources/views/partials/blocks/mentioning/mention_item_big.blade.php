<a href="{{ $mention->link ?? '#' }}" target="_blank" rel="nofollow"
   class="flex bg-white pr-2 py-4 md:pr-5 mention">
    <div class="w-2/5 mr-5 mention__img">
        <img src="{{ $mention->image }}" alt="{{ $mention->name }}">
    </div>
    <div class="w-3/5 text-sm md:text-lg flex justify-center flex-col">
        <h5 class="font-semibold block mention__title">{{ $mention->source->name }}</h5>
        <p class="mention__text">{{ $mention->text }}</p>
    </div>
</a>
