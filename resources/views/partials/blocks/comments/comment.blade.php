<div class="comment" data-id="{{ $comment->id }}">
    @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
        <a href="{{ route('platform.comments.edit', $comment->id) }}"
           target="_blank" class="lg:float-right btn btn_admin btn_sm inline-block mb-2">
            Редактировать
        </a>
    @endif
    <div class="flex">
        <div class="w-16 mr-5">
        <span class="w-16 h-16 uppercase comment__image">
            @if(is_null($comment->person))
                {{ mb_substr($comment->name ?? '', 0, 1) }}
            @else
                <img lazy src="@include('partials.icons.null_image')"
                     data-src="{{ $comment->person->getFirstMediaUrl('face') }}"
                     alt="{{ $comment->person->name ?? '' }}">
            @endif
        </span>
        </div>
        <div>
            <div class="comment__info">
                <span class="font-semibold comment__name">
                    {{ $comment->person->name ?? $comment->name ?? '' }}
                </span>
                @if(!is_null($comment->person))
                    <div class="block lg:inline-block">
                        <span class="text-sm font-bold px-3 py-1 bg-green-300 text-white rounded ml-1">
                            @if($comment->person->id == ($post->author->id ?? false))
                                {{ __('blocks.comments.author') }}
                            @else
                                {{ __('blocks.comments.person') }}
                            @endif
                        </span>
                    </div>
                @endif
                <span class="comment__date">
                    {{ $comment->created_at->diffForHumans() }}
                </span>
            </div>
            <p class="comment__text">
                {{ $comment->text ?? '' }}
            </p>
            <div class="comment__actions">
                <a href="#" class="comment_reply">{{ __('blocks.comments.add.reply') }}</a>
            </div>
        </div>
    </div>
    @if($comment->replies->count() > 0)
        <div class="w-full pl-8 lg:pl-16 comment__replies">
            @foreach($comment->replies as $reply)
                @include('partials.blocks.comments.comment', ['comment' => $reply])
            @endforeach
        </div>
    @endif
</div>
