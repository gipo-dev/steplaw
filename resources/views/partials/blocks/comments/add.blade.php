<div class="block__comments__add">
    <p class="font-semibold text-xl">{{ __('blocks.comments.add.title') }}</p>
    <p>{{ $comment ?? $post->data['comment'] }}</p>

    <form action="{{ route('comments.store') }}" method="POST" class="mb-10 form_round block__comments__form">
        <div class="mb-4 py-2 bg-green-500 text-center text-white font-semibold hidden sent_success">
            {{ __('blocks.comments.add.success') }}
        </div>
        <input type="hidden" name="comment[commentable_id]" value="{{ $commentable_id }}">
        <input type="hidden" name="comment[commentable_type]" value="{{ $commentable_type }}">
        <input type="hidden" name="comment[reply_to]">
        <div class="fields">
            @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
                <select name="comment[person_id]">
                    @foreach(\App\Models\Person::get() as $person)
                        <option
                            value="{{ $person->id }}" {{ $person->id == ($post->author_id ?? false) ? 'selected' : '' }}>
                            {{ $person->name ?? '' }}
                        </option>
                    @endforeach
                </select>
                <input type="hidden" name="comment[name]" value="Аноним">
            @else
                <input type="text" name="comment[name]"
                       class="form-control" required
                       placeholder="{{ __('blocks.forms.inputs.name') }}">
            @endif
            <textarea name="comment[text]" rows="5" class="form-control" required
                      placeholder="{{ __('blocks.forms.inputs.message') }}"></textarea>
            <div class="sent_wrap">
                <span>{{ __('blocks.forms.thanks.title') }}</span>
                <p>{!! __('blocks.forms.thanks.text') !!}</p>
            </div>
        </div>
        <div class="flex justify-center">
            <button class="mt-3 form-control btn_main btn_sm">
                {{ __('blocks.contacts.button') }}
            </button>
        </div>
        @csrf
        <div class="sent_wrap">
            <span class="title">{{ __('blocks.forms.thanks.title') }}</span>
            <p>{!! __('blocks.forms.thanks.text') !!}</p>
            <div class="flex justify-center mt-8">
                <span class="modal-callback-close btn btn_primary">{{ __('blocks.forms.modal.close') }}</span>
            </div>
        </div>
    </form>
</div>
