@if(count($comments) > 0)
    <div class="mb-4 block__comments">
        <p class="text-xl font-semibold">
            {{ trans_choice('blocks.comments.title', $comments->count(), ['count' => $comments->count()]) }}
        </p>
        <ul class="list_unstyled block__comments__list">
            @foreach($comments as $comment)
                @include('partials.blocks.comments.comment')
            @endforeach
        </ul>
    </div>
@endif
