<div class="mb-6 block__image _single">
    <a href="{{ $data['file']['url'] ?? '' }}" title="{{ $data['caption'] }}"
       data-jbox-image="post_image_{{ $id ?? '' }}">
        <img src="{{ $data['file']['url'] ?? '' }}" alt="{{ $data['file']['caption'] ?? '' }}">
    </a>
    @if($data['caption'] ?? false)
        <p class="mt-2 text-grey-400">{{ $data['caption'] ?? '' }}</p>
    @endif
</div>
