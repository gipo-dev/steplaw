<ul itemscope itemtype="http://schema.org/FAQPage" >
    @foreach($qa['items'] as $i => $item)
        <li class="page_service_long__faq__item {{ $i == 0 ? '_active' : '' }}" itemprop="mainEntity" itemscope itemtype="http://schema.org/Question">
            <p class="page_service_long__faq__item__title" itemprop="name">
                {{ $item['title'] }}
            </p>
            <div itemscope itemprop="acceptedAnswer" itemtype="http://schema.org/Answer">
                <p class="page_service_long__faq__item__text" itemprop="text">
                    {{ $item['text'] }}
                </p>
                <div>
        </li>
    @endforeach
    @if($qa['hasMore'])
        <li data-faq="{{ route('blocks.services.faq', ['service' => $service, 'category' => -1, 'page' => 2]) }}">
            <a class="page_service_long__faq__link">
                Смотреть еще
                @include('partials.icons.arrow_rigth')
            </a>
        </li>
    @endif
</ul>
