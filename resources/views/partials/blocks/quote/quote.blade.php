<div class="block__quote__wrap block__quote page_person__face"
     style="background-image:url('{{ $person->getFirstMediaUrl('face') }}');">
    <blockquote class="mt-5 mb-16"><span class="block mb-1 text-xl text-primary"
        >{{ $person->name }}, {{ $person->position }}:</span>«<span editable>{!! $text ?? '' !!}</span>»
    </blockquote>
</div>
