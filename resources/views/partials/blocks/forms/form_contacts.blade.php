<div class="relative contacts__form callback-form-wrap">
    <p class="text-xl text-primary">
        <strong>{{ __('blocks.contacts.title') }}</strong>
    </p>
    <form action="{{ route('blocks.callback.store') }}" method="POST" class="mb-10 callback-form">
        <div class="fields">
            <input type="text" name="name"
                   class="form-control" required
                   placeholder="{{ __('blocks.forms.inputs.name') }}">
            <input type="text" name="phone"
                   class="form-control" required
                   placeholder="{{ __('blocks.forms.inputs.phone') }}">
            <textarea name="message" rows="5" class="form-control" required
                      placeholder="{{ __('blocks.forms.inputs.message') }}"></textarea>
            <div class="sent_wrap">
                <span>{{ __('blocks.forms.thanks.title') }}</span>
                <p>{!! __('blocks.forms.thanks.text') !!}</p>
            </div>
        </div>
        <div class="flex justify-center">
            <button class="mt-3 form-control btn_main btn_sm">
                {{ __('blocks.contacts.button') }}
            </button>
        </div>
        @csrf
    </form>
</div>
