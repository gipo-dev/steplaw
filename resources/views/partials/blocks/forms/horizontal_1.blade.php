<div class="block_callback block_callback_horizontal_1 callback-form-wrap">
    <form action="{{ route('blocks.callback.store') }}" method="POST" class="text-center relative callback-form">
        <div class="fields">
            @csrf
            <p class="text-primary font-semibold mb-2 text-2xl md:text-3xl uppercase">
                {{ __('blocks.forms.title') }}
            </p>
            <p class="md:text-xl font-medium mb-10">
                {{ __('blocks.forms.description') }}
            </p>
            <div class="flex flex-wrap justify-center">
                <input type="text" name="name" class="form-control max-w-xs mr-0 mb-3 md:mb-0 md:mr-5"
                       placeholder="{{ __('blocks.forms.inputs.name') }}">
                <input type="text" name="phone" class="form-control max-w-xs ml-0 md:ml-5"
                       placeholder="{{ __('blocks.forms.inputs.phone') }}" minlength="18" required>
                @if($service_id ?? false)
                    <input type="hidden" name="service_id" value="{{ $service_id }}">
                @endif
            </div>
            <div class="flex flex-wrap justify-center mb-8">
                <button class="form-control btn_main mt-8">
                    {{ __('blocks.forms.inputs.submit') }}
                </button>
            </div>
            <div class="sent_wrap">
                <span>{{ __('blocks.forms.thanks.title') }}</span>
                <p>{!! __('blocks.forms.thanks.text') !!}</p>
            </div>
        </div>
        <div class="flex flex-col font-medium text-primary">
            <p class="md:text-2xl text-black">{{ __('blocks.forms.subtext') }}</p>
            <a href="tel:{{ \Settings::getPhoneLink('phone_1') }}" class="text-2xl md:text-4xl">
                {{ \Settings::get('phone_1') }}
            </a>
        </div>
    </form>
</div>
