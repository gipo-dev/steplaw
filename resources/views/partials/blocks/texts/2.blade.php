<div class="flex flex-wrap block__text" data-show-more="{{ __('blocks.show_more.text') }}">
    <div class="lg:w-1/2 px-2 lg:pr-6">{!! $col_1 ?? '' !!}</div>
    <div class="lg:w-1/2 px-2 lg:pl-6">{!! $col_2 ?? '' !!}</div>
</div>
