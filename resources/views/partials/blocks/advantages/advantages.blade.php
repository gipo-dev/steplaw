<div class="section block__advantages text-center text-white font-semibold mt-5">
    <p class="text-3xl uppercase lg:whitespace-pre block__advantages__title">{{ $title ?? __('blocks.advantages.title') }}</p>
    <div class="flex flex-wrap">
        <div class="md:w-1/3 lg:px-12 flex flex-col mb-5 md:mb-0 block__advantages__item">
            <div class="flex md:flex-col">
                <span class="block__advantages__number">5</span>
                <span class="block__advantages__item__title">
                    {{ __('blocks.advantages.rows.lawyers.title') }}</span>
            </div>
            <span class="block__advantages__item__desc">{{ __('blocks.advantages.rows.lawyers.description') }}</span>
        </div>
        <div class="md:w-1/3 px-12 flex flex-col mb-5 md:mb-0 block__advantages__item">
            <div class="flex md:flex-col">
                <span class="block__advantages__number">5000</span>
                <span class="block__advantages__item__title">{{ __('blocks.advantages.rows.cases.title') }}</span>
            </div>
            <span class="block__advantages__item__desc">{{ __('blocks.advantages.rows.cases.description') }}</span>
        </div>
        <div class="md:w-1/3 px-10 flex flex-col mb-5 md:mb-0 block__advantages__item">
            <div class="flex md:flex-col">
                <span class="block__advantages__number">24/7</span>
                <span class="block__advantages__item__title">{{ __('blocks.advantages.rows.feature.title') }}</span>
            </div>
            <span class="block__advantages__item__desc">{{ __('blocks.advantages.rows.feature.description') }}</span>
        </div>
    </div>
</div>
