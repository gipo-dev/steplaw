<div class="px-8 py-6 text-{{ $data['alignment'] ?? 'left' }} block__warning">
    @if(trim($data['caption']) != '')
        <p class="block__warning__title">
            {{ $data['caption'] ?? '' }}
        </p>
    @endif
    <p class="block__warning__text">{!! $data['text'] ?? '' !!}</p>
</div>
