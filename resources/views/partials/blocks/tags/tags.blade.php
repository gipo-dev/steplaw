@if(count($tags) > 0)
    <p class="font-semibold">{{ __('blocks.tags.title') }}</p>
    <ul class="list_unstyled block__tags">
        @foreach($tags as $tag)
            <li>
                <a href="{{ route('page.posts.tag', $tag) }}">
                    {{ $tag->name }}
                </a>
            </li>
        @endforeach
    </ul>
@endif
