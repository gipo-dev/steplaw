@foreach($thanks as $thank)
    <li class="w-full md:w-1/2 md:px-4">
        <div class="splide__slide__container block thank">
            <div class="bg-white px-4 py-6">
                <h5 class="mt-2 font-semibold text-primary text-2xl" data-id="{{ $thank->id }}"
                    data-url="{{ route('blocks.thank.modal', ['id' => $thank->id ]) }}">
                    {{ $thank->name ?? '' }}
                </h5>
                @if($thank->service)
                    <p class="mb-2">
                                    <span class="text-sm text-grey-400">{{ __('blocks.thanks.service') }}
                                        <a
                                           class="text-primary border-b border-primary">
                                            {{ $thank->service->name ?? '' }}
                                        </a>
                                    </span>
                    </p>
                @endif
                <p class="font-medium thank__text" data-id="{{ $thank->id }}"
                   data-url="{{ route('blocks.thank.modal', ['id' => $thank->id ]) }}">{!! $thank->body !!}</p>
            </div>
        </div>
    </li>
@endforeach
