<div class="splide splide_white">
    <ul class="partner__carousel _custom">
        @foreach($videos as $video)
            <li class="splide__slide w-full md:w-1/3 md:mr-6">
                <div class="splide__slide__container block partner__item">
                    <div class="relative flex">
                        <span data-url="{{ route('modal.partner', ['id' => $video->id]) }}"
                              class="absolute top-0 right-0 bottom-0 left-0 cursor-pointer"
                              data-partner data-id="{{ $video->id }}"></span>
                        {{--                        <iframe width="560" height="315" data-src="{{ $video->src }}"--}}
                        {{--                                title="YouTube video player" frameborder="0"--}}
                        {{--                                allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"--}}
                        {{--                                allowfullscreen></iframe>--}}
                        <img class="_img" src="{{ $video->preview }}">
                        <p class="partner__item__title">
                            {{ $video->title ?? '' }}
                        </p>
                        <div class="partner__item__source">
                            <img src="/img/logo_square 1.png" alt="Moment istiny">
                            <span class="partner__item__source__name">{{ __('pages.service.long.social.source.name') }}</span>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="splide__arrows text-white mt-2">
        <button class="splide__arrow splide__arrow--prev">
            @include('partials.icons.arrow_rigth')
        </button>
        <button class="splide__arrow splide__arrow--next">
            @include('partials.icons.arrow_rigth')
        </button>
    </div>
</div>
