<div class="splide splide_miniable ">
    <ul class="lg:mb-12 mt-6 list_unstyled persons__carousel">
        @foreach(\App\Models\Person::get()->sortBy('order') as $person)
            <li class="w-1/2 md:w-1/3 xl:w-1/5 splide__slide">
                @include('partials.blocks.persons.person_item')
            </li>
        @endforeach
    </ul>
    @if(\Agent::isMobile())
        <div class="splide__arrows text-primary mt-2 lg:hidden lg:mt-10">
            <button class="splide__arrow splide__arrow--prev">
                @include('partials.icons.arrow_rigth')
            </button>
            <button class="splide__arrow splide__arrow--next">
                @include('partials.icons.arrow_rigth')
            </button>
        </div>
    @endif
</div>
