<div>
    <ul class="list_unstyled mb-2 thanks__carousel" @if($ajax ?? true) data-url="{{ route('blocks.thank.carousel') }}" @endif>
        @include('partials.blocks.carousel.thanks_inner', ['thanks' => $thanks->take($count)])
    </ul>
    <div class="splide__arrows text-primary mt-2">
        <button class="splide__arrow splide__arrow--prev">
            @include('partials.icons.arrow_rigth')
        </button>
        <button class="splide__arrow splide__arrow--next">
            @include('partials.icons.arrow_rigth')
        </button>
    </div>
</div>
