<div class="splide splide_miniable">
    <ul class="mb-6 list_unstyled persons__carousel persons__carousel_small">
        @foreach(\App\Models\Person::get()->sortBy('order') as $person)
            <li class="w-full md:w-1/2 xl:w-1/4">
                <a href="{{ route('page.persons.person', $person) }}"
                   class="splide__slide__container block text-center person">
                    <div class="person__image"
                         style="background-image:url('{{ $person->getFirstMediaUrl('face') }}');"></div>
                    <p class="font-semibold text-primary text-xl mb-1 person__name">
                        {{ $person->name }}
                    </p>
                    <p class="font-medium mb-1">
                        {{ $person->position }}
                    </p>
                    <p class="person__about whitespace-pre-line">{{ $person->about }}</p>
                </a>
            </li>
        @endforeach
    </ul>
    <div class="splide__arrows text-primary mt-2">
        <button class="splide__arrow splide__arrow--prev">
            @include('partials.icons.arrow_rigth')
        </button>
        <button class="splide__arrow splide__arrow--next">
            @include('partials.icons.arrow_rigth')
        </button>
    </div>
</div>
