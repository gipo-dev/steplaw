<ol class="block__list">
    @foreach($data['items'] ?? [] as $item)
        <li>
            <span class="content">{!! $item['content'] ?? '' !!}</span>
            @if(count($item['items']) > 0)
                @include('partials.blocks.list.ordered', ['data' => $item])
            @endif
        </li>
    @endforeach
</ol>
