<ul class="block__list">
    @foreach($data['items'] ?? [] as $item)
        <li>
            {!! $item['content'] ?? '' !!}
            @if(count($item['items']) > 0)
                @include('partials.blocks.list.ordered', ['data' => $item])
            @endif
        </li>
    @endforeach
</ul>
