<div class="bg-white block__post @if($small ?? false) _small @endif">
    {{--    <a href="{{ $post->getLink() }}" class="block__post__image">--}}
    {{--        <img src="@include('partials.icons.null_image')" lazy data-src="{{ $post->image() }}" alt="{{ $post->name ?? '' }}">--}}
    {{--    </a>--}}
    @if($small ?? false)
        <div class="py-2 mb-2">
            @else
                <div class="px-6 lg:px-12 py-4 lg:py-8">
                    @endif
                    @if($post->category)
                        <a
{{--                            href="{{ route('page.posts.category', $post->category) }}"--}}
                           class="block font-bold text-primary mb-1">
                            {{ $post->category->name ?? '' }}
                        </a>
                    @endif
                    <a href="{{ $post->getLink() }}">
                        <p class="text-xl xl:text-2xl leading-tight font-bold mb-2 block__post__title">
                            {{ $post->name ?? '' }}
                        </p>
                    </a>

                    <div class="block__post__info">
                        @if($post->data['read_time'] ?? false)
                            <span>
                    @include('partials.icons.clock')
                                {{ __('pages.posts.data.read_time') }} {{ $post->data['read_time'] ?? '' }}
                </span>
                        @else
                            @if($post->metrics->views() > 0)
                                <span>
                    @include('partials.icons.eye')
                                    {{ $post->metrics->views() }}
                </span>
                            @endif
                            @if($post->comments->count() > 0)
                                <span>
                    @include('partials.icons.comment')
                                    {{ $post->comments->count() }}
                </span>
                            @endif
                        @endif
                    </div>
                </div>
        </div>
