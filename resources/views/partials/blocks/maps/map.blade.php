@if(count($data) > 0)
    <p class="font-semibold">{{ __('blocks.map.title') }}</p>
    <ul class="list_unstyled block__map">
        @foreach($data as $i => $block)
            <li>
                <a href="#block-{{ $i }}">
                    {!! $block['data']['text'] !!}
                </a>
            </li>
        @endforeach
    </ul>
@endif
