<section class="page_service_long__contacts__wrap">
    <div class="page_service_long__contacts__map">
        <div id="map" style="height: 100%;width: 100%;"></div>
        {{--        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A315e44a933f8f2cfde66cb82a7bdbe80f04d477e40c11eea7f20dff0da8162a6&amp;source=constructor"--}}
        {{--                width="100%" height="100%" frameborder="0"></iframe>--}}
    </div>
    <div class="container">
        <div class="page_service_long__contacts">
            <h3 class="page_service_long__contacts__title">
                {{ __('pages.contacts.title') }}
            </h3>

            <div class="page_service_long__contacts__item">
                <p class="page_service_long__contacts__item__title">
                    {{ __('pages.contacts.address') }}
                </p>
                <p class="page_service_long__contacts__item__text">
                    {{ \Settings::get('address') }}
                </p>
            </div>

            <div class="page_service_long__contacts__item">
                <p class="page_service_long__contacts__item__title">
                    {{ __('pages.contacts.work_time') }}
                </p>
                <p class="page_service_long__contacts__item__text">
                    {{ \Settings::get('work_time') }}
                </p>
            </div>

            <div class="page_service_long__contacts__item page_service_long__contacts__item__social">
                <div>
                    <p class="page_service_long__contacts__item__title">
                        {{ __('pages.service.long.contacts.phones') }}
                    </p>
                    <p class="page_service_long__contacts__item__text">
                        <a href="tel:{{ \Settings::getPhoneLink('phone_1') }}">
                            {{ \Settings::get('phone_1') }}
                        </a>
                        <a href="tel:{{ \Settings::getPhoneLink('phone_2') }}">
                            {{ \Settings::get('phone_2') }}
                        </a>
                        <a href="mailto:{{ \Settings::get('email') }}">
                            {{ \Settings::get('email') }}
                        </a>
                    </p>
                </div>
                <div class="page_service_long__contacts__item__r">
                    <a href="{{ \Settings::get('telegram') }}"
                       target="_blank" class="_telegram">
                        @include('partials.icons.telegram')
                    </a>
                    <a href="{{ \Settings::get('whatsapp') }}"
                       target="_blank" class="_whatsapp">
                        @include('partials.icons.whatsapp')
                    </a>
                </div>
            </div>

            <a class="btn btn_main page_service_long__contacts__link modal-callback-open">
                {{ __('pages.service.long.contacts.link') }}
            </a>

        </div>
    </div>
</section>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=534cb9cd-da65-4e29-bc9c-0e8d118acbe6"
        type="text/javascript"></script>
<script>
    ymaps.ready(function () {
        var zoomControl = new ymaps.control.ZoomControl();
        // var routeControl = new ymaps.control.RoutePanel({options: {position: {right: 5, top: 5}, autofocus: false}});

        var myMap = new ymaps.Map('map', {
              center: [55.742977, 37.670176],
              zoom: 17,
              controls: [zoomControl],
          }, {
              searchControlProvider: 'yandex#search'
          }),

          // Создаём макет содержимого.
          MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
          ),

          myPlacemark = new ymaps.Placemark([55.743036, 37.671346], {
              hintContent: '',
              balloonContent: '',
              iconContent: ''
          }, {
              // Опции.
              // Необходимо указать данный тип макета.
              iconLayout: 'default#imageWithContent',
              // Своё изображение иконки метки.
              iconImageHref: '/img/map_icon_alt.png',
              // Размеры метки.
              iconImageSize: [48, 72],
              // Смещение левого верхнего угла иконки относительно
              // её "ножки" (точки привязки).
              iconImageOffset: [-24, -72],
              // Смещение слоя с содержимым относительно слоя с картинкой.
              iconContentOffset: [15, 15],
              // Макет содержимого.
              iconContentLayout: MyIconContentLayout
          });

        myMap.behaviors.disable('scrollZoom');
        myMap.controls.remove();

        myMap.geoObjects
          .add(myPlacemark);

        // routeControl.routePanel.state.set({
        //     from: "метро Площадь Ильича",
        //     to: "Большая Андроньевская улица, 13с2",
        // });
        //
        // myMap.setCenter([55.742977, 37.670176], 17, {
        //     checkZoomRange: true
        // });
        //
        // var location = ymaps.geolocation.get();
        //
        // location.then(function (res) {
        //     var userTextLocation = res.geoObjects.get(0).properties.get('text');
        //     routeControl.routePanel.state.set({
        //         from: userTextLocation,
        //         to: 'Большая Андроньевская улица, 13с2'
        //     });
        //
        //     setTimeout(function () {
        //         myMap.setCenter([55.742977, 37.670176], 17, {
        //             // checkZoomRange: true
        //         });
        //     }, 5000);
        // });

    });
</script>
