@if($author ?? false)
    <div class="flex mb-5 block__author">
        <div class="w-20 h-20 mr-4 flex">
            <img class="object-cover rounded-full h-full w-full" lazy src="@include('partials.icons.null_image')"
                 data-src="{{ $author->getFirstMediaUrl('face') }}" alt="{{ $author->name ?? '' }}">
        </div>
        <div class="block__author__about">
            <p>
                <a href="{{ route('page.persons.person', $author) }}">{{ $author->name ?? '' }}</a>
            </p>
            <div class="block__author__info">
                @if(isset($post) && $post->created_at)
                    <p>
                        {{ $post->created_at->format('H:i d.m.Y') }}
                        @if($post->updated_at && $post->updated_at->diffInDays($post->created_at) > 1)
                            ({{ __('blocks.person.updated_at') }} {{ $post->updated_at->format('H:i d.m.Y') }})
                        @endif
                    </p>
                @endif
                <p>
                    {{ $author->position }}
                </p>
            </div>
        </div>
    </div>
@endif
