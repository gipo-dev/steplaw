<div id="block-{{ $id ?? '' }}" class="block__header">
{{--    @if(($data['level'] ?? 2) < 3)--}}
{{--        @include('partials.blocks.hr.hr_1')--}}
{{--    @endif--}}
    <h{{ $data['level'] ?? 2 }} class="text-primary block__header__h">
    {{ $data['text'] ?? '' }}
    </h{{ $data['level'] ?? 2 }}>
</div>
