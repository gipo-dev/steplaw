<table class="block__table">
    <tbody>
    @foreach($data['content'] ?? [] as $row)
        <tr>
            @foreach($row as $td)
                <td>{{ $td ?? '' }}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
