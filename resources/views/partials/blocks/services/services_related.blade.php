<div class="page_service_long__related">
    <div class="container py-10">

        <h3 class="page_service_long__related__title">
            {{ __('pages.service.long.related.title') }}
        </h3>

        <div class="page_service_long__related__categories__wrap">
            <ul class="page_service_long__related__categories"
                @if(\Agent::isMobile())
                    data-flickity='{ "draggable": true, "freeScroll": true, "cellAlign": "left", "pageDots": false, "prevNextButtons": false, "adaptiveHeight": true }'
                    @endif>
                @foreach($categories as $i => $category)
                    <li class="page_service_long__related__categories__item {{ $i == 0 ? '_active' : '' }}"
                        data-category="{{ route('blocks.services.related', ['category' => $category]) }}">
                        {{ $category->name }}
                    </li>
                @endforeach
            </ul>
        </div>
        <ul class="page_service_long__related__services" id="related-services-items"
            @if(\Agent::isMobile()) data-is-mobile="true" @endif>
            @include('partials.blocks.services.services_related_items')
        </ul>

    </div>
</div>
