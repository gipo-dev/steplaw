<a href="{{ $service->getLink() }}" class="block h-full bg-white px-8 py-4 sm:py-8 hover:shadow-lg
        transition-shadow duration-200 ease-in-out overflow-hidden service">
    <p class="text-xl xl:text-2xl leading-tight text-primary font-semibold mb-2">
        {{ $service->name ?? '' }}
    </p>
    <p class="text-black font-medium mb-3">
        {!! $service->description ?? '' !!}
    </p>
    <p class="text-grey-400 font-medium text-15 underline mb-8">
        {{ $service->tags ?? '' }}
    </p>
    @if($service->victories > 0)
        <div class="flex justify-center">
            <span class="px-2 py-1 text-black font-bold uppercase text-xs bg-grey-100">
                @if(in_array($service->id, [62,]))
                    {{ __('blocks.services.participation', ['number' => $service->victories]) }}
                @else
                    {{ __('blocks.services.victories', ['number' => $service->victories]) }}
                @endif
            </span>
        </div>
    @endif
</a>
