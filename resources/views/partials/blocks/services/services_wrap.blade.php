<div class="services__items {{ \Agent::isMobile() ? '' : 'flex flex-wrap ' }} "
     @if($id ?? false) data-id="{{ $id }}" @endif>
    @if(\Agent::isMobile())
        <div class="services__carousel  @if($primary ?? false) @else splide_white @endif">
            @foreach($services->chunk(1) as $chunk)
                <li class="flex flex-wrap w-full md:w-1/2">
                    @foreach($chunk as $service)
                        <div class="w-full p-2 sm:p-2 md:p-4">
                            @include('partials.blocks.services.services_item')
                        </div>
                    @endforeach
                </li>
            @endforeach
        </div>
        <div class="splide__arrows @if($primary ?? false) text-primary @else text-white @endif mt-2">
            <button class="splide__arrow splide__arrow--prev">
                @include('partials.icons.arrow_rigth')
            </button>
            <button class="splide__arrow splide__arrow--next">
                @include('partials.icons.arrow_rigth')
            </button>
        </div>
    @else
        @foreach($services as $service)
            <div class="sm:w-1/2 lg:w-1/3 py-2 sm:p-2 md:p-4">
                @include('partials.blocks.services.services_item')
            </div>
        @endforeach
    @endif
</div>
