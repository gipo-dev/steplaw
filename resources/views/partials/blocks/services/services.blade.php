<div class="services overflow-hidden">
    <div class="services__categories mb-4" data-url="{{ route('blocks.services') }}">
        <ul class="flex justify-evenly flex-wrap text-md md:text-lg font-medium pb-4">
            @foreach(\App\Models\ServiceCategory::get() as $category)
                <li>
                    <span class="px-2 cursor-pointer" data-id="{{ $category->slug }}">
                        <span class="border-b-2 border-white">
                        {{ $category->name }}
                        </span>
                    </span>
                </li>
            @endforeach
        </ul>
    </div>
    @include('partials.blocks.services.services_wrap')
    <div class="flex justify-center mt-6">
        <a href="{{ route('page.services.all') }}" class="btn_primary sm:w-auto sm:px-32">
            {{ __('blocks.services.all') }}
        </a>
    </div>
</div>
