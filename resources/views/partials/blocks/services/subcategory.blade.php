<div class="@if($ml ?? false) md:pl-4 @else md:pr-4 @endif mb-10 w-full md:w-1/2">
    <div class="border-t-4 border-primary h-full subcategory">
        <div class="border border-t-0 border-gray-400 pt-5 pb-6 px-6 h-full">
            <p class="font-semibold mb-4 subcategory_title">
                {{ $subcategory->name }}
            </p>
{{--            <p class="text-grey-500 subcategory_text">--}}
{{--                {{ $subcategory->description }}--}}
{{--            </p>--}}
            <a href="{{ $subcategory->getLink() }}"
               class="text-primary font-semibold subcategory_link">
                {{ __('pages.service.more') }}
            </a>
        </div>
    </div>
</div>
