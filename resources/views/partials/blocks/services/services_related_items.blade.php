@if(\Agent::isMobile())
    @foreach($services->chunk(4) as $group)
        <li>
            @foreach($group as $service)
                <a href="{{ $service->getLink() }}"
                   class="page_service_long__related__services__item">
                    {{ $service->name }}
                </a>
            @endforeach
        </li>
    @endforeach
@else
    @foreach($services as $service)
        <li>
            <a href="{{ $service->getLink() }}"
               class="page_service_long__related__services__item">
                {{ $service->name }}
            </a>
        </li>
    @endforeach
@endif
