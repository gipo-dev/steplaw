<p>
    <iframe width="705" height="400" src="{{ $code ?? '' }}" title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
    @if($text ?? false)
        <span class="block mt-2 text-grey-400 italic text-center">{{ $text ?? '' }}</span>
    @endif
</p>
