<li class="w-full">
    <div class="splide__slide__container block thank">
        <div>
            <h5 class="mt-2 font-semibold text-2xl" data-id="{{ $thank->id }}"
                data-url="{{ route('blocks.thank.modal', ['id' => $thank->id ]) }}">
                {{ $thank->name ?? '' }}
            </h5>
            @if($thank->service)
                <p class="mb-2">
                    <span class="text-sm">{{ __('blocks.thanks.service') }}
                        <a href="{{ $thank->service->getLink() }}"
                           class="text-primary border-b border-primary">
                            {{ $thank->service->name ?? '' }}
                        </a>
                    </span>
                </p>
            @endif
            <p class="font-medium thank__text" data-id="{{ $thank->id }}"
               data-url="{{ route('blocks.thank.modal', ['id' => $thank->id ]) }}">
                {!! $thank->body !!}
            </p>
        </div>
    </div>
</li>
