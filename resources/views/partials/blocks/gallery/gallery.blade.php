@if(count($images) > 0)
    @php $images = collect($images); @endphp
    <div class="my-2 block__gallery">
        @if($images->count() == 1 || $images->count() == 3)
            @include('partials.blocks.gallery.gallery_1_3')
        @else
            @include('partials.blocks.gallery.gallery_2')
        @endif
    </div>
@endif
