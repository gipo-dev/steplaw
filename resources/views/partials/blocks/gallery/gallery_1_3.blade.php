<div class="flex flex-wrap block__gallery__3">
    <a href="{{ $images->first()['url'] }}" title="{{ $images->first()['caption'] }}"
       data-jbox-image="reviews-{{ $id }}" class="block w-full p-1 block__gallery__image _big">
        <img lazy data-src="{{ $images->first()['url'] }}" alt="{{ $images->first()['caption'] }}">
    </a>
    <div class="flex">
        @foreach($images->skip(1) as $image)
            <a href="{{ $image['url'] }}" title="{{ $image['caption'] }}"
               data-jbox-image="reviews-{{ $id }}" class="block w-1/2 p-1 block__gallery__image">
                <img lazy data-src="{{ $image['url'] }}" alt="{{ $image['caption'] }}">
            </a>
        @endforeach
    </div>
</div>
