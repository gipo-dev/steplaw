<div class="flex flex-wrap block__gallery__2">
    <div class="flex flex-wrap">
        @foreach($images as $image)
            <a href="{{ $image['url'] }}" title="{{ $image['caption'] }}"
               data-jbox-image="reviews-{{ $id }}" class="block w-1/2 p-1 block__gallery__image">
                <img lazy data-src="{{ $image['url'] }}" alt="{{ $image['caption'] }}">
            </a>
        @endforeach
    </div>
</div>
