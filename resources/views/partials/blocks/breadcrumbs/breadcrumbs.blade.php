@if(count($breadcrumbs) > 0)
        <ol class="flex flex-wrap my-6 text-primary font-medium block__breadcrumbs">
            @foreach ($breadcrumbs as $breadcrumb)

                @if (!is_null($breadcrumb['url'] ?? null))
                    <li class="mr-3 border-b border-opacity-25 border-primary breadcrumb-item">
                        <a href="{{ $breadcrumb['url'] ?? '' }}">
                            {{ $breadcrumb['title'] ?? '' }}
                        </a>
                    </li>
                @else
                    <li class="text-grey-500 breadcrumb-item active">
                        {{ $breadcrumb['title'] ?? '' }}
                    </li>
                @endif

            @endforeach
        </ol>
@endif
