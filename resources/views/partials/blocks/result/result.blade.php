<div class="block__result">
    <p class="text-3xl font-bold block__result__title">{{ __('blocks.result.title') }}</p>
    {!! $text ?? '' !!}
    @include('partials.blocks.hr.hr_2')
</div>
