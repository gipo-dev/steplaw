<div class="block_reviews">
    <ul class="list_unstyled mb-6 reviews__carousel">
        @foreach($reviews as $i => $review)
            <li class="w-full md:w-1/3 md:px-4">
                <div class="splide__slide__container block review">
                    <div class="bg-white py-6">
                        <a href="{{ $review->getFirstMediaUrl('image') }}" data-jbox-image="reviews-gallery">
                            <img src="{{ $review->getFirstMediaUrl('image') }}"
                                 class="w-full" alt="Отзыв клиента №{{ $i + 1 }}">
                        </a>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="splide__arrows text-primary mt-2">
        <button class="splide__arrow splide__arrow--prev">
            @include('partials.icons.arrow_rigth')
        </button>
        <button class="splide__arrow splide__arrow--next">
            @include('partials.icons.arrow_rigth')
        </button>
    </div>
</div>
