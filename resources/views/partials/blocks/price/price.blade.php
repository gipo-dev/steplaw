<div class="block__price">
    @if(count($price->groupBy('category_id')) >= 2)
        <ul class="list_unstyled flex block__price__menu">
            <li class="w-1/2 text-right pr-4">
                <a href="#" class="btn-price active" data-category="0">
                    {{ __('blocks.price.category.0') }}
                </a>
            </li>
            <li class="w-1/2 ml-4 text-left">
                <a href="#" class="btn-price" data-category="1">
                    {{ __('blocks.price.category.1') }}
                </a>
            </li>
        </ul>
    @endif
    <div>
        @foreach($price->groupBy('category_id') as $i => $category)
            <ul class="list_unstyled lg:text-lg {{ (count($price->groupBy('category_id')) < 2 || $i == 0) ? '' : 'hidden' }} price-category"
                id="price-category-{{ $i }}">
                @foreach($category as $item)
                    <li class="flex flex-wrap lg:flex-no-wrap pb-3 lg:pb-2">
                        <div class="w-full lg:w-7/12 px-6 bg-primary text-white font-medium py-1">
                            {!! $item->name ?? '' !!}
                        </div>
                        <div class="w-7/12 lg:w-3/12 bg-grey-100 px-6 py-1 flex justify-center items-center">
                            {{ __('blocks.price.price', ['price' => number_format($item->price ?? 0, 0, ',', ' ')]) }}
                        </div>
                        <div class="w-5/12 lg:w-2/12 px-6 py-1 flex justify-center items-center">
                            <a href="#callback-block"
                               class="border-b-2 border-primary text-primary leading-tight whitespace-no-wrap">
                                {{ __('blocks.price.button') }}
                            </a>
                        </div>
                    </li>
                @endforeach
            </ul>
        @endforeach
    </div>
</div>
