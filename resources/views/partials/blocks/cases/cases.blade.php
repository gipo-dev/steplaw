<div class="splide overflow-hidden">
    <ul class="mb-6 cases__carousel">
        @foreach($cases as $case)
            <li class="w-full splide__slide">
                <div class="splide__slide__container block case overflow-hidden">
                    <div class="bg-grey-50 px-10 py-8 font-medium">
                        <p class="mb-1 text-xl font-bold uppercase">{{ $case->name ?? '' }}</p>
                        {!! $case->body ?? '' !!}
                    </div>
                    <div class="bg-primary px-10 py-8 text-white relative">
                        <img src="/img/stamp.png" alt="Штамп" class="float-left mr-8 stamp">
                        <p class="mb-1 text-xl font-bold uppercase">{{ __('blocks.cases.result') }}</p>
                        <div class="font-medium overflow-hidden">
                            {!! $case->result !!}
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="splide__arrows text-primary mt-2">
        <button class="splide__arrow splide__arrow--prev">
            @include('partials.icons.arrow_rigth')
        </button>
        <button class="splide__arrow splide__arrow--next">
            @include('partials.icons.arrow_rigth')
        </button>
    </div>
</div>
