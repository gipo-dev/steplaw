<div class="page_service_long__cases__case _small">
    <div class="page_service_long__cases__case__top">
        <a href="{{ route('page.cases.case', $case) }}" class="page_service_long__cases__case__title">
            {{ $case->name }}
        </a>
        <div class="page_service_long__cases__case__content">
            {!! mb_substr(strip_tags($case->page->body['description'] ?? ''), 0, 90)  !!}...
        </div>
        @if(count($case->files ?? []) > 0)
            <div class="page_service_long__cases__case__files">
                @foreach($case->files as $file)
                    <img src="/img/document.jpg" alt="document">
                @endforeach
            </div>
        @endif
    </div>
    <div class="page_service_long__cases__case__bottom">
        <a href="{{ route('page.cases.case', $case) }}" class="page_service_long__cases__case__person__link">
            {{ __('pages.service.long.cases.more') }}

{{--            @include('partials.icons.arrow_rigth')--}}
        </a>
    </div>
</div>
