<div class="page_service_long__cases__case _big">
    <div class="page_service_long__cases__case__top">
        <a href="{{ route('page.cases.case', $case) }}" class="page_service_long__cases__case__title">
            {{ $case->name }}
        </a>
        <div class="page_service_long__cases__case__content">
            {!! mb_substr(strip_tags($case->page->body['description'] ?? ''), 0, 335)  !!}...
        </div>
        @if(count($case->files ?? []) > 0)
            <div class="page_service_long__cases__case__files">
                @foreach($case->files as $file)
                    <img src="/img/document.jpg" alt="document">
                @endforeach
            </div>
        @endif
    </div>
    <div class="page_service_long__cases__case__bottom">
        @if($case->person)
            <a href="{{ route('page.persons.person', $case->person) }}" class="page_service_long__cases__case__person">
                <div class="page_service_long__cases__case__person__image">
                    <img src="{{ $case->person->getFirstMediaUrl('face') }}" alt="{{ $case->person->name }}">
                </div>
                <div class="page_service_long__cases__case__person__name">
                    <p>{{ $case->person->name }}</p>
                </div>
            </a>
        @endif
        <a href="{{ route('page.cases.case', $case) }}" class="page_service_long__cases__case__person__link">
            {{ __('pages.service.long.cases.more') }}

            @include('partials.icons.arrow_rigth')
        </a>
    </div>
</div>
