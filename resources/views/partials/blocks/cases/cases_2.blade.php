<div class="splide splide_white">
    <ul class="mb-6 list_unstyled cases__carousel cases__carousel_small">
        @foreach($cases as $case)
            <li class="w-full md:w-1/2 md:px-4">
                <div class="splide__slide__container block case" data-id="{{ $case->id }}"
                     data-url="{{ route('blocks.case.modal', ['id' => $case->id ]) }}">
                    <div class="bg-grey-50 px-10 py-8">
                        <h4 class="mb-1 font-bold text-grey-400 lg:text-xl uppercase">
                            {{ __('blocks.cases.summary') }}
                        </h4>
                        <div class="font-medium case__text">{!! $case->body ?? '' !!}</div>
                        <h4 class="mt-4 mb-1 font-bold text-grey-400 lg:text-xl uppercase">
                            {{ __('blocks.cases.result') }}
                        </h4>
                        <div class="font-medium case__text">{!! $case->result !!}</div>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <div class="splide__arrows text-white mt-2">
        <button class="splide__arrow splide__arrow--prev">
            @include('partials.icons.arrow_rigth')
        </button>
        <button class="splide__arrow splide__arrow--next">
            @include('partials.icons.arrow_rigth')
        </button>
    </div>
</div>
