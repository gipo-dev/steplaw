<header class="bg-primary text-white py-1 lg:py-6 relative header">
    <div class="container mx-auto flex">
        <div class="w-1/6 lg:hidden">
            <span class="flex justify-center items-center h-full header__menu__button" id="btn_menu">
            @include('partials.icons.menu')
            </span>
        </div>
        <div class="w-4/6 lg:w-1/2 py-1">
            <a href="/" class="header__logo">
                @include('partials.icons.logo')
            </a>
        </div>
        <div class="w-1/6 lg:hidden">
            <span id="btn_call" class="flex justify-center h-full header__menu__call" id="btn_call">
            @include('partials.icons.call')
            </span>
        </div>
        <div class="lg:w-1/2 text-right font-semibold relative hidden lg:block header__contacts" id="header_contacts">
            <span class="lg:hidden inline-block w-8 absolute right-0 top-0 p-2 mt-3 mr-3" id="btn_call_close">
            @include('partials.icons.close')
            </span>
            <div class="flex justify-end flex-col lg:flex-row header__contacts__phones">
                <a href="tel:{{ \Settings::getPhoneLink('phone_1') }}"
                   class="block text-xl header__contacts__phone lg:mr-5">
                    {{ \Settings::get('phone_1') }}
                </a>
                <a href="tel:{{ \Settings::getPhoneLink('phone_2') }}"
                   class="block text-sm header__contacts__phone lg:text-xl lg:mr-5">
                    {{ \Settings::get('phone_2') }}
                </a>
                <p class="text-sm text-grey-200 lg:hidden">
                    {{ \Settings::get('duty_lawyer') }}
                </p>
                <div class="flex justify-center header__contacts__messenger">
                    <a href="{{ \Settings::get('telegram') }}" target="_blank" class="block text-xl mr-3 lg:mr-5">
                        @include('partials.icons.telegram')
                    </a>
                    <a href="{{ \Settings::get('whatsapp') }}" target="_blank" class="block text-xl">
                        @include('partials.icons.whatsapp')
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<nav class="bg-grey-200 header__menu" id="header_menu">
    <div class="min-h-full container mx-auto flex flex-col lg:flex-row">
        <span class="lg:hidden absolute right-0 top-0 mr-4 mt-4 w-6 header__menu__close" id="btn_menu_close">
            @include('partials.icons.close')
        </span>
        <div class="mb-8 lg:hidden advantages__emblem emblem" lazy data-src="/img/emblem.png"></div>
        <ul class="w-full lg:w-2/3 flex flex-no-wrap whitespace-no-wrap justify-center uppercase font-semibold header__menu__items">
            @foreach($menu as $name => $url)
                <li>
                    <a href="{{ $url }}" class="inline-block px-2">
                        {{ $name }}
                    </a>
                </li>
            @endforeach
            @if(\Auth::id())
                <li>
                    <a href="{{ route('platform.services') }}" class="inline-block px-2"style="background: #026f4f;color: #fff;">
                        Админка
                    </a>
                </li>
            @endif
        </ul>
        <div class="hidden lg:flex w-1/3 uppercase text-white font-semibold header__partners">
            <a href="{{ route('page.index') }}#moment"
               class="inline-block bg-primary text-center w-1/2 text-xs transition-all duration-300 hover:opacity-75">
                <img lazy src="@include('partials.icons.null_image')" data-src="/img/headmomentistini-red.jpg"
                     alt="Момент истины">
            </a>
            <a href="{{ route('page.index') }}#callback"
               class="inline-block bg-primary text-center w-1/2 text-xs
                    transition-all duration-300 hover:opacity-75 modal-callback-open">
                @include('partials.icons.mail') {{ __('loops.menu.callback') }}
            </a>
        </div>
        <div class="lg:hidden text-center font-semibold relative mt-5">
            <a href="tel:{{ \Settings::getPhoneLink('phone_1') }}" class="block text-sm mt-0 text-2xl tracking-widest">
                {{ \Settings::get('phone_1') }}
            </a>
            <span
                class="flex flex-col-reverse justify-center text-sm text-grey-200 whitespace-no-wrap font-bold tracking-widest">
                <span class="lg:mr-1">дежурный адвокат 00:00-21:00</span>
                <a href="tel:{{ \Settings::getPhoneLink('phone_2') }}">
                    {{ \Settings::get('phone_2') }}
                </a>
            </span>
        </div>
    </div>
</nav>

{{--<div class="flex justify-center lg:hidden btn_float_wrap">--}}
{{--    <a href="{{ route('page.index') }}#callback"--}}
{{--       class="inline-block bg-primary text-center btn_float shadow-md--}}
{{--                    transition-all duration-300 hover:opacity-75 modal-callback-open">--}}
{{--        @include('partials.icons.mail') {{ __('loops.menu.callback') }}--}}
{{--    </a>--}}
{{--</div>--}}
