<footer class="bg-primary text-white font-medium text-center md:text-left footer">
    <div class="container flex flex-wrap flex-col items-center md:flex-row py-16">
        <div class="md:w-2/5 mb-5 md:mb-0">
            <p>© {{ now()->year }}</p>
            <p>«Соколов, Трусов и Партнеры»</p>
            <p>Адвокатское бюро города Москвы</p>
            <span class="w-2/5 inline-block border-b border-white mb-1 opacity-50"></span>
            <p class="mb-2 md:mb-0 px-3 md:px-0">
                {{ \Settings::get('address') }}
            </p>
            <p class="mb-2 md:mb-0">Контактный телефон:
                <a href="tel:{{ \Settings::getPhoneLink('phone_1') }}"
                   class="border-b border-white border-opacity-50 pb-1">
                    {{ \Settings::get('phone_1') }}
                </a>
            </p>
            <p>Email:
                <a href="mailto:{{ \Settings::get('email') }}"
                   class="border-b border-white border-opacity-50 pb-1">
                    {{ \Settings::get('email') }}
                </a>
            </p>
        </div>
        <div class="md:w-3/5 max-w-xs md:max-w-none">
            <div class="flex flex-wrap justify-center md:justify-end items-center">
                @foreach($menu as $name => $link)
                    <a href="{{ $link }}" class="border-b border-white border-opacity-50 leading-tight mx-3 my-1 pb-1">
                        {{ $name }}
                    </a>
                @endforeach
            </div>
            <div class="flex flex-wrap justify-center md:justify-end items-center">
                <a class="inline-block p-2 bg-white mr-3 mt-12 _big">
                    <img src="/img/palata.png" alt="pravo-300">
                </a>
                <a class="inline-block p-2 bg-white mr-3 mt-12 _big">
                    <img src="/img/guild.png" alt="pravo-300">
                </a>
                <a class="inline-block p-2 bg-white md:mr-3 mt-12">
                    <img src="/img/pravo-300.png" alt="pravo-300">
                </a>
            </div>
        </div>
    </div>
</footer>
