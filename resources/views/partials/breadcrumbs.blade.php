@unless ($breadcrumbs->isEmpty())
    <ol class="flex my-6 text-primary font-medium breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)

            @if (!is_null($breadcrumb->url) && !$loop->last)
                <li class="mr-3 border-b border-opacity-25 border-primary breadcrumb-item">
                    <a href="{{ $breadcrumb->url }}">
                        {{ $breadcrumb->title }}
                    </a>
                </li>
            @else
                <li class="breadcrumb-item active">
                    {{ $breadcrumb->title }}
                </li>
            @endif

        @endforeach
    </ol>
@endunless
