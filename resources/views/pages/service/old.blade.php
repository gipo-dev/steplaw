@extends('partials.body')

@section('content')
    <div class="container container_small">
        @include('partials.blocks.breadcrumbs.breadcrumbs')
    </div>
    <div class="page__inner page_service">
        @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
            <section class="container container_small bg-white mx-auto">
                <div class="flex justify-end">
                    <a href="{{ route('platform.services.edit', $service->id) }}" class="btn btn_admin btn_sm">Редактировать</a>
                </div>
            </section>
        @endif

        @if($service->getValue('old.service.text.0.title', true) ?? false)
            <section class="container container_small bg-white mx-auto">

                <section class="container bg-white mx-auto">
                    <div class="block__service-header">
                        <div class="block__service-header__body">
                            <h1 class="text-primary">{!! $service->getValue('old.service.text.0.title') !!}</h1>
                            <p class="block__service-header__description">
                                @php $headerComment = $service->page['body']['old']['header']['comment'] ?? ''; @endphp
                                @if(trim($headerComment) != '')
                                    {!! $headerComment !!}
                                @else
                                    {{ __('blocks.services.header.description') }}
                                @endif
                            </p>
                            <a href="#callback-block" class="btn_main block__service-header__btn">
                                {{ __('blocks.services.header.request') }}
                            </a>
                        </div>
                    </div>

                    <div class="container  mx-auto bg-white mt-5 pt-5">
                        {!! $service->getValue('old.service.text.0.text') !!}
                    </div>

            </section>
        @endif

        @if(count($childs) > 0)
            <section class="container container_small bg-white mx-auto mb-4 flex flex-wrap">
                @foreach($childs as $i => $child)
                    @include('partials.blocks.services.subcategory', [
                        'service' => $service,
                        'subcategory' => $child,
                        'ml' => ($i % 2) != 0,
                    ])
                @endforeach
            </section>
        @endif

        @if($service->getValue('old.service.text.1.text', true) ?? false)
            <section class="container container_small bg-white mx-auto">
                @include('partials.blocks.hr.hr_1')
                <h2 class="text-primary">{!! $service->getValue('old.service.text.1.title') !!}</h2>
                {!! $service->getValue('old.service.text.1.text') !!}
            </section>
        @endif

        @if(($service->getValue('old.person.id', true) ?? false) && ($service->getValue('old.person.text', true) ?? false))
            <section class="container container_small bg-white mx-auto" style="padding-top: 0;">
                @include('partials.blocks.quote.quote',
                        [
                            'person' => \App\Models\Person::find($service->getValue('old.person.id')),
                            'text' => $service->getValue('old.person.text'),
                        ])
            </section>
        @endif

        @foreach([1,2] as $i)
            @if($service->getValue('old.result.text_' . $i, true) ?? false)
                <section class="container container_small bg-primary mx-auto"
                         style="padding-top: 0;overflow:visible;">
                    @include('partials.blocks.result.result', ['text' => $service->getValue('old.result.text_' . $i) ?? ''])
                </section>
            @endif
        @endforeach

        @if($mentions->count() > 0)
            <section class="container container_small mx-auto bg-white" style="padding-top: 60px;">
                <h2 class="text-primary">{{ __('pages.person.mentions') }}</h2>
                <div class="mentioning mb-10">
                    @foreach($mentions as $mention)
                        @include('partials.blocks.mentioning.mention_item_big')
                    @endforeach
                </div>
            </section>
        @endif

        @if($cases->count() > 0)
            <section class="container container_small _fw mx-auto bg-primary pb-12 section_cases">
                @include('partials.blocks.hr.hr_1')
                <h2 class="text-white mb-10">{{ __('blocks.cases.title') }}</h2>
                @include('partials.blocks.cases.cases_2')
            </section>
        @endif

        <section class="container container_small mx-auto bg-white pb-20" id="callback-block">
            <div class="pt-5"></div>
            @include('partials.blocks.forms.horizontal_2', ['service_id' => $service->id])
        </section>
    </div>
@endsection
