@extends('partials.body')

@section('content')
    <div class="container ">
        @include('partials.blocks.breadcrumbs.breadcrumbs')
    </div>

    <div class="page__inner page_service">
        @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
            <section class="container  bg-white mx-auto">
                <div class="flex justify-end">
                    <a href="{{ route('platform.services.edit', $service->id) }}" class="btn btn_admin btn_sm">Редактировать</a>
                </div>
            </section>
        @endif

        @if($service->page['body'][0]['title'] ?? false)
            <section class="container bg-white mx-auto">
                <div class="block__service-header">
                    <div class="block__service-header__body">
                        <h1 class="text-primary">{!! $service->page['body'][0]['title'] ?? '' !!}</h1>
                        <p class="block__service-header__description">
                            @php $headerComment = $service->page['body'][101]['text'] ?? ''; @endphp
                            @if(trim($headerComment) != '')
                                {!! $headerComment !!}
                            @else
                                {{ __('blocks.services.header.description') }}
                            @endif
                        </p>
                        <a href="#callback-block" class="btn_main block__service-header__btn">
                            {{ __('blocks.services.header.request') }}
                        </a>
                    </div>
                </div>

                <div class="container  mx-auto bg-white mt-5 pt-5">
                    {!! $service->page['body'][0]['text'] ?? '' !!}
                </div>
            </section>
        @endif

        @if($service->page['body'][1]['text'] ?? false)
            <section class="container  mx-auto bg-white">
                <h2 class="text-primary">{!! $service->page['body'][1]['title'] ?? '' !!}</h2>
                <div class="ul-to-check">
                    {!! $service->page['body'][1]['text'] ?? '' !!}
                </div>
            </section>
        @endif

        @if(count($childs) > 0)
            <section class="container  bg-white mx-auto mb-4 flex flex-wrap">
                @foreach($childs as $i => $child)
                    @include('partials.blocks.services.subcategory', [
                        'service' => $service,
                        'subcategory' => $child,
                        'ml' => ($i % 2) != 0,
                    ])
                @endforeach
            </section>
        @endif

        @if($service->page['body'][2]['text'] ?? false)
            <section class="container  mx-auto bg-white">
                <h2 class="text-primary">{!! $service->page['body'][2]['title'] ?? '' !!}</h2>
                {!! $service->page['body'][2]['text'] ?? '' !!}
            </section>
        @endif

        <section class="container  p-0-i bg-white">
            @include('partials.blocks.advantages.advantages', ['title' => $service->page['body']['advantages']['title'] ?? null])
        </section>

        <section class="container  _fw mx-auto persons bg-white overflow-hidden">
            @include('partials.blocks.hr.hr_1')
            <h2 class="text-primary">{{ __('pages.index.persons.title') }}</h2>
            @include('partials.blocks.carousel.persons_small')
        </section>

        @if($service->page['body'][3]['text'] ?? false)
            <section class="container  mx-auto bg-white">
                {{--                @include('partials.blocks.hr.hr_2')--}}
                <h2 class="text-primary">{!! $service->page['body'][3]['title'] ?? '' !!}</h2>
                {!! $service->page['body'][3]['text'] ?? '' !!}
            </section>
        @endif

        @if($price->count() > 0)
            <section class="container  mx-auto price bg-white">
                {{--                @include('partials.blocks.hr.hr_2')--}}
                <h2 class="text-primary">{{ __('blocks.price.title') }}</h2>
                @include('partials.blocks.price.price')
                @if($service->page['body']['after_price']['text'] ?? false)
                    <div>{!! $service->page['body']['after_price']['text'] ?? '' !!}</div>
                @endif
            </section>
        @endif

        @if(!$service->parent)
            <section class="container  mx-auto price_reasons bg-white">
                <h2 class="text-primary">{{ __('blocks.price_reasons.title') }}</h2>
                <p>{{ __('blocks.price_reasons.description') }}</p>
                <ul class="mt-6">
                    @foreach(__('blocks.price_reasons.reasons') as $reason)
                        <li>{{ $reason }}</li>
                    @endforeach
                </ul>
            </section>
        @endif

        @if($service->page['body'][4]['text'] ?? false)
            <section class="container  mx-auto bg-white">
                <h2 class="text-primary">{!! $service->page['body'][4]['title'] ?? '' !!}</h2>
                <div class="">
                    <div>{!! $service->page['body'][4]['text'] ?? '' !!}</div>
                </div>
            </section>
        @endif

        @if($service->page['body']['qa']['text'] ?? false)
            <section class="container  mx-auto bg-white" style="padding-top: 0;">
                {!! $service->getQA() !!}
            </section>
        @endif

        @if(count($thanks) > 0)
            <section class="container  _fw mx-auto relative bg-grey-100 pb-12">
                @include('partials.blocks.hr.hr_2')
                <h2 class="text-primary mb-1">{{ __('pages.index.thanks.title') }}</h2>
                @include('partials.blocks.carousel.thanks', ['count' => 99, 'ajax' => false])
            </section>
        @endif

        @if($cases->count() > 0)
            <section class="container  _fw mx-auto bg-primary pb-12 section_cases">
                @include('partials.blocks.hr.hr_1')
                <h2 class="text-white mb-10">{{ __('blocks.cases.title') }}</h2>
                @include('partials.blocks.cases.cases_2')
            </section>
        @endif

        @if(count($reviews) > 0)
            <section class="container  _fw mx-auto bg-white block_reviews__wrap">
                @include('partials.blocks.hr.hr_2')
                <h2 class="text-primary mb-6">{{ __('blocks.reviews.title') }}</h2>
                @include('partials.blocks.reviews.reviews')
            </section>
        @endif

        <section class="container  mx-auto bg-white pb-20" id="callback-block">
            <h2 class="text-white mb-6">{{ __('blocks.cases.title') }}</h2>
            @include('partials.blocks.forms.horizontal_2', ['service_id' => $service->id])
        </section>

    </div>
@endsection
