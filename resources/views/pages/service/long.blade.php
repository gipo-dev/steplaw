@extends('partials.body')

@section('content')
    <section class="page_service_long">
        @php $person = \App\Models\Person::find($service->page['body']['long']['header']['person'] ?? ''); $personImage = $person ? $person->getFirstMediaUrl('face') : ''; @endphp
        <div class="page_service_long__header__wrap" style="--person-image: url('{{ $personImage }}')">
            <div class="container page_service_long__header {{ $personImage == '' ? '_no-photo' : '' }}">
                @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
                    <section class="container mx-auto">
                        <div class="flex justify-end">
                            <a href="{{ route('platform.services.edit', $service->id) }}" class="btn btn_admin btn_sm">Редактировать</a>
                        </div>
                    </section>
                @endif

                @include('partials.blocks.breadcrumbs.breadcrumbs')

                <div class="page_service_long__header__z1">
                    <h1 class="page_service_long__header__title">
                        {!! $service->page['body']['long']['header']['title'] ?? '' !!}
                    </h1>

                    <div class="page_service_long__header__comment">
                        @if($service->page['body']['long']['header']['text'] ?? false)
                            {!! $service->page['body']['long']['header']['text'] !!}
                        @else
                            <p>{!!  __('pages.service.long.header.description') !!}</p>
                        @endif
                    </div>

                    <div class="page_service_long__header__buttons">
                        <a href="#callback-block" class="form-control btn_main">
                            {{ __('pages.service.long.header.request') }}
                        </a>
                        <a href="#page-service-long-content-wrap" class="form-control btn_outline_main">
                            {{ __('pages.service.long.header.more') }}
                        </a>
                    </div>
                </div>

            </div>
        </div>

        <ul class="container py-10 page_service_long__features">
            @foreach(__('pages.service.long.features') as $feature)
                <li class="page_service_long__features__feature">
                    <span class="page_service_long__features__icon">
                        @include('partials.icons.' . ($feature['icon'] ?? ''))
                    </span>
                    <span class="page_service_long__features__title">
                        {{ $feature['title'] ?? '' }}
                    </span>
                    <span class="page_service_long__features__description">{{ $feature['description'] ?? '' }}</span>
                </li>
            @endforeach
            <li class="lg:hidden page_service_long__features__callback">
                <a href="#callback-block" class="form-control btn_main">
                    {{ __('pages.service.long.header.request') }}
                </a>
            </li>
        </ul>

        <div class="container py-5 page_service_long__about">

            <div class="p-10 page_service_long__about__main">
                <h2 class="page_service_long__about__title">
                    {{ $service->page['body']['long']['block2']['title'] ?? '' }}
                </h2>
                <div class="page_service_long__about__content">
                    {!! $service->page['body']['long']['block2']['text'] ?? '' !!}
                </div>
            </div>

            <div class="p-10 page_service_long__about__features">
                <p class="page_service_long__about__features__title">
                    {{ $service->page['body']['long']['spec']['title'] ?? '' }}
                </p>
                <ul class="page_service_long__about__features__items">
                    @foreach(explode(PHP_EOL, $service->page['body']['long']['spec']['items']) as $item)
                        <li>
                            {{ $item }}
                        </li>
                    @endforeach
                </ul>
            </div>

        </div>

        @if($service->childs->count())
            <div class="container py-5 page_service_long__services">
                <h3>
                    {{ __('pages.service.long.services.title') }}
                </h3>
                <div class="page_service_long__services__list">
                    @foreach($service->childs as $child)
                        <a href="{{ $child->getLink() }}" class="page_service_long__services__service">
                            <p class="page_service_long__services__service__title">
                                {{ $child->name }}
                            </p>
                            {{--                            <div>--}}
                            {{--                                <a href="{{ $child->getLink() }}" class="page_service_long__services__service__link">--}}
                            {{--                                    {{ __('pages.service.long.services.more') }}--}}
                            {{--                                </a>--}}
                            {{--                            </div>--}}
                        </a>
                    @endforeach
                </div>
            </div>
        @endif

        <div class="page_service_long__persons__wrap">
            <div
                class="container py-5 page_service_long__persons {{ ($service->page->body['options']['show_positions'] ?? false) ? 'page_service_long__persons_full' : '' }}">

                <h2>
                    {{ __('pages.service.long.persons.title') }}
                </h2>

                <ul class="page_service_long__persons__list">
                    @foreach(\App\Models\Person::get()->sortBy('order') as $i => $person)
                        <li class="splide__slide page_service_long__persons__person">
                            <a data-person="{{ route('blocks.services.person', ['person' => $person]) }}"
                               class="{{ $person->id == 24 ? '_current' : '' }}">
                                <div class="page_service_long__persons__person__img">
                                    <img src="{{ $person->getFirstMediaUrl('face') }}">
                                </div>
                                <p class="page_service_long__persons__person__name">
                                    {{ $person->name }}
                                </p>
                                @if($service->page->body['options']['show_positions'] ?? false)
                                    <p class="page_service_long__persons__person__position">
                                        {{ $person->data['post'] ?? '' }}
                                    </p>
                                @endif
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div id="service-person-block-current">
                    @include('partials.blocks.persons.person_show_big', [
                            'person' => \App\Models\Person::get()->sortBy('order')->first(),
                        ])
                </div>

                <h3 class="page_service_long__persons__features__title">
                    {{ $service->page['body']['long']['features']['title'] ?? __('pages.service.long.persons.features.title') }}
                </h3>

                <div class="splide page_service_long__persons__features__wrap">
                    <ul class="page_service_long__persons__features"
                        @if(\Agent::isMobile()) data-flickity='{ "draggable": true, "adaptiveHeight": true, "resize": false }' @endif>
                        @foreach(__('pages.service.long.persons.features.items') as $item)
                            <li class="splide__slide page_service_long__persons__feature">
                                <div class="page_service_long__persons__feature__wrap">
                                    <p class="page_service_long__persons__feature__header">
                                        {{ $item['header'] ?? '' }}
                                    </p>
                                    <p class="page_service_long__persons__feature__title">
                                        {{ $item['title'] ?? '' }}
                                    </p>
                                    <p class="page_service_long__persons__feature__text">
                                        {{ $item['text'] ?? '' }}
                                    </p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </div>

        @if($service->casepages->count() || ($service->parent && $service->parent->casepages->count()))
            <div class="container py-10 px-5 md:px-2">
                <div style="display: none">
                </div>

                <h2>
                    {{ __('pages.service.long.cases.title') }}
                </h2>

                @if($service->casepages->count())
                    <div class="page_service_long__cases">
                        <div class="page_service_long__cases__col1">
                            @include('partials.blocks.cases.case_big', ['case' => $service->casepages->first()])
                        </div>
                        <div class="page_service_long__cases__col2">
                            @foreach($service->casepages->skip(1)->take(2) as $case)
                                @include('partials.blocks.cases.case_small', ['case' => $case])
                            @endforeach
                        </div>
                        <div class="page_service_long__cases__col3">
                            @if ($service->casepages->count() > 3)
                                <div class="page_service_long__cases__list">
                                    <h4 class="page_service_long__cases__list__title">
                                        {{ __('pages.service.long.cases.list.title') }}
                                    </h4>
                                    <ul>
                                        @foreach($service->casepages->skip(3)->take(4) as $case)
                                            <li>
                                                <a href="{{ route('page.cases.case', $case) }}">
                                                    {{ $case->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="page_service_long__cases__more">
                                    <a href="{{ route('page.cases.all') }}">
                                <span>
                                    {{ __('pages.service.long.cases.list.link') }}
                                </span>
                                        @include('partials.icons.arrow_rigth')
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                @elseif($service->parent && $service->parent->casepages->count())
                    <div class="page_service_long__cases">
                        <div class="page_service_long__cases__col1">
                            @include('partials.blocks.cases.case_big', ['case' => $service->parent->casepages->first()])
                        </div>
                        <div class="page_service_long__cases__col2">
                            @foreach($service->parent->casepages->skip(1)->take(2) as $case)
                                @include('partials.blocks.cases.case_small', ['case' => $case])
                            @endforeach
                        </div>
                        <div class="page_service_long__cases__col3">
                            @if ($service->parent->casepages->count() > 3)
                                <div class="page_service_long__cases__list">
                                    <h4 class="page_service_long__cases__list__title">
                                        {{ __('pages.service.long.cases.list.title') }}
                                    </h4>
                                    <ul>
                                        @foreach($service->parent->casepages->skip(3)->take(4) as $case)
                                            <li>
                                                <a href="{{ route('page.cases.case', $case) }}">
                                                    {{ $case->name }}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="page_service_long__cases__more">
                                    <a href="{{ route('page.cases.all') }}">
                                <span>
                                    {{ __('pages.service.long.cases.list.link') }}
                                </span>
                                        @include('partials.icons.arrow_rigth')
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
                @endif

            </div>

            <div class="page_service_long__content__wrap" id="page-service-long-content-wrap">
                <div class="container page_service_long__content">
                    <h2>
                        {{ $service->page['body']['long']['block3_title'] ?? '' }}
                    </h2>

                    <ul class="page_service_long__content__links"
                        data-flickity='{ "cellAlign": "left", "freeScroll": true, "contain": true, "prevNextButtons": false, "pageDots": false }'>
                        @php $currentSet = false; @endphp
                        @foreach($service->page['body']['long']['block3'] ?? [] as $index => $item)
                            <li class="page_service_long__content__link
                        {{ $currentSet === false ? '_active' : '' }}" }}
                                data-tab-group=".page_service_long__content__block"
                                data-tab-btn="#loading-block-{{ $index }}">
                                @php $currentSet = true; @endphp
                                {{ $item['title'] }}
                            </li>
                        @endforeach
                    </ul>

                    <div class="page__inner page_service_long__content__block">

                        @foreach($service->page['body']['long']['block3'] as $i => $block)
                            <div class="_tab {{ $i == 0 ? '_active _loaded' : '' }}" id="loading-block-{{ $i }}"
                                 data-tab-url="{{ route('blocks.services.tab', [
                            'service' => $service,
                            'tab' => "long.block3.$i.content",
                        ]) }}">
                                <div>
                                    {!! $service->page['body']['long']['block3'][$i]['content'] ?? '' !!}
                                </div>
                                @if($i == 0)
                                    <div class="page_service_long__content__wrapped">
                                        @foreach(
                                            $service->page['body']['long']['qa'] ?? __('pages.service.long.content.qa')
                                        as $item)
                                            <div class="page_service_long__content__wrapped__item">
                                                <h4 class="page_service_long__content__wrapped__item__title"
                                                    data-collapse>
                                                    {!! $item['q'] ?? '' !!}
                                                    @include('partials.icons.arrow_down')
                                                </h4>
                                                <div class="page_service_long__content__wrapped__item__text pb-4">
                                                    <p>{!! $item['a'] ?? '' !!}</p>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        @endforeach

                        <div class="_tab" id="page-content-block-text-2"
                             data-tab-url="{{ route('blocks.services.tab', [
                            'service' => $service,
                            'tab' => 'default.features',
                        ]) }}"></div>
                        <div class="_tab" id="page-content-block-text-3"
                             data-tab-url="{{ route('blocks.services.tab', [
                            'service' => $service,
                            'tab' => 'default.statistics',
                        ]) }}"></div>

                    </div>
                </div>

                @if(count($service->photoReviews) || count($service->parent->photoReviews ?? []) > 0)
                    <div class="container py-10 page_service_long__reviews">

                        <h2 class="page_service_long__reviews__title">
                            {{ __('pages.service.long.reviews.title') }}
                        </h2>

                        <p class="page_service_long__reviews__subtitle">
                            {{ __('pages.service.long.reviews.subtitle') }}
                        </p>

                        <div class="splide splide_white">
                            <ul class="page_service_long__reviews__carousel"
                                id="image-reviews-carousel">
                                @foreach($service->photoReviews ?? $service->parent->photoReviews ?? [] as $photoReview)
                                    <li class="splide__slide">
                                        <a href="{{ $photoReview->getFirstMediaUrl('image') }}"
                                           data-jbox-image="photo-reviews-gallery">
                                            <img src="{{ $photoReview->getFirstMediaUrl('image') }}">
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="splide__arrows text-white mt-2">
                                <button class="splide__arrow splide__arrow--prev">
                                    @include('partials.icons.arrow_rigth')
                                </button>
                                <button class="splide__arrow splide__arrow--next">
                                    @include('partials.icons.arrow_rigth')
                                </button>
                            </div>
                        </div>

                    </div>
                @endif

                <div class="container page_service_long__reviews_yandex">

                    <div class="page_service_long__reviews_yandex__header">
                        <div class="page_service_long__reviews_yandex__header__rating">
                        <span class="page_service_long__reviews_yandex__header__rating__number">
                            4,4
                        </span>
                            <div class="page_service_long__reviews_yandex__header__rating__stars">
                                @for ($star = 0; $star < 5; $star++)
                                    @include('partials.icons.star')
                                @endfor
                            </div>
                            <a href="https://yandex.ru/maps/-/CCUVM2AStB" target="_blank"
                               class="page_service_long__reviews_yandex__header__rating__link">
                                Рейтинг организации в Яндексе
                                @include('partials.icons.link')
                            </a>
                        </div>
                        <a href="https://yandex.ru/maps/-/CCUVM2AStB" target="_blank"
                           class="page_service_long__reviews_yandex__header__btn">
                            <img src="/img/add.png">
                            Написать отзыв
                        </a>
                    </div>


                    @if(count($service->thanks))
                        <div class="splide splide_white">
                            <ul class="page_service_long__reviews__carousel _yandex  thanks__carousel flickity-enabled is-draggable
                                {{ ($service->page->body['options']['show_reviews_category'] ?? true) ? '' : '_hide_category' }}"
                                id="image-reviews-yandex-carousel">
                                @foreach($service->thanks as $index => $thank)
                                    <li class="splide__slide page_service_long__reviews__review"
                                        data-index="{{ $index }}">
                                        <div class="page_service_long__reviews__review__header">
                                        <span class="page_service_long__reviews__review__img">
                                            {{ mb_substr($thank->name ?? '', 0, 1) }}
                                        </span>
                                            <div class="page_service_long__reviews__review__header__wrap">
                                                <p class="page_service_long__reviews__review__name">
                                                    {{ $thank->name }}
                                                    <span>
                                                    {{ $thank->city ?? '' }}
                                                </span>
                                                </p>
                                                <div class="page_service_long__reviews__review__stars">
                                                    @for ($star = 0; $star < 5; $star++)
                                                        @include('partials.icons.star')
                                                    @endfor
                                                </div>
                                            </div>
                                        </div>
                                        @php $textLength = mb_strlen(mb_substr($thank->body, 270)); @endphp
                                        <div class="page_service_long__reviews__review__text"
                                             @if($textLength > 0) data-text="{{ $thank->body }}" @endif>
                                            {{ mb_substr($thank->body, 0, 270) . ($textLength > 0 ? '...' : '') }}
                                            @if($textLength > 0)
                                                <span class="page_service_long__reviews__review__text__more">
                                                    Читать&nbsp;далее
                                                </span>
                                            @endif
                                        </div>
                                        <div class="page_service_long__reviews__review__text _full">
                                            {{ $thank->body }}
                                        </div>
                                        <div class="page_service_long__reviews__review__footer">
                                            @if($thank->casePage)
                                                <a href="{{ route('page.cases.case', $thank->casePage) }}"
                                                   class="page_service_long__reviews__review__footer__case">
                                                    {{ $thank->casePage->name }}
                                                </a>
                                            @endif
                                            @if($thank->service)
                                                <div class="flex">
                                                <span>
                                                    Категория:
                                                </span>
                                                    <a href="{{ $thank->service->getLink() }}"
                                                       class="page_service_long__reviews__review__footer__service">
                                                        {{ $thank->service->name }}
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="splide__arrows text-white mt-2">
                                <button class="splide__arrow splide__arrow--prev">
                                    @include('partials.icons.arrow_rigth')
                                </button>
                                <button class="splide__arrow splide__arrow--next">
                                    @include('partials.icons.arrow_rigth')
                                </button>
                            </div>

                            <a class="page_service_long__reviews_yandex__link">
                                Смотреть все отзывы
                            </a>
                        </div>
                    @elseif($service->parent && $service->parent->thanks->count())
                        <div class="splide splide_white">
                            <ul class="page_service_long__reviews__carousel _yandex  thanks__carousel flickity-enabled is-draggable"
                                id="image-reviews-yandex-carousel">
                                @foreach($service->parent->thanks as $index => $thank)
                                    <li class="splide__slide page_service_long__reviews__review"
                                        data-index="{{ $index }}">
                                        <div class="page_service_long__reviews__review__header">
                                        <span class="page_service_long__reviews__review__img">
                                            {{ mb_substr($thank->name ?? '', 0, 1) }}
                                        </span>
                                            <div class="page_service_long__reviews__review__header__wrap">
                                                <p class="page_service_long__reviews__review__name">
                                                    {{ $thank->name }}
                                                    <span>
                                                    {{ $thank->city ?? '' }}
                                                </span>
                                                </p>
                                                <div class="page_service_long__reviews__review__stars">
                                                    @for ($star = 0; $star < 5; $star++)
                                                        @include('partials.icons.star')
                                                    @endfor
                                                </div>
                                            </div>
                                        </div>

                                        @php $textLength = mb_strlen(mb_substr($thank->body, 270)); @endphp
                                        <div class="page_service_long__reviews__review__text"
                                             @if($textLength > 0) data-text="{{ $thank->body }}" @endif>
                                            {{ mb_substr($thank->body, 0, 270) . ($textLength > 0 ? '...' : '') }}
                                            @if($textLength > 0)
                                                <span class="page_service_long__reviews__review__text__more">
                                                    Читать&nbsp;далее
                                                </span>
                                            @endif
                                        </div>
                                        <div class="page_service_long__reviews__review__text _full">
                                            {{ $thank->body }}
                                        </div>

                                        <div class="page_service_long__reviews__review__footer">
                                            @if($thank->casePage)
                                                <a href="{{ route('page.cases.case', $thank->casePage) }}"
                                                   class="page_service_long__reviews__review__footer__case">
                                                    {{ $thank->casePage->name }}
                                                </a>
                                            @endif
                                            @if($thank->service)
                                                <div class="flex">
                                                <span>
                                                    Категория:
                                                </span>
                                                    <a href="{{ $thank->service->getLink() }}"
                                                       class="page_service_long__reviews__review__footer__service">
                                                        {{ $thank->service->name }}
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <div class="splide__arrows text-white mt-2">
                                <button class="splide__arrow splide__arrow--prev">
                                    @include('partials.icons.arrow_rigth')
                                </button>
                                <button class="splide__arrow splide__arrow--next">
                                    @include('partials.icons.arrow_rigth')
                                </button>
                            </div>

                            <a class="page_service_long__reviews_yandex__link">
                                Смотреть все отзывы
                            </a>
                        </div>
                    @endif

                </div>

            </div>

            <div class="container py-10 page_service_long__recommendations">

                <h2>
                    {{ $service->page['body']['long']['recommendations']['title'] ?? __('pages.service.long.recommendations.title') }}
                </h2>

                <ul class="page_service_long__recommendations__list"
                    @if(\Agent::isMobile()) data-is-mobile="true" @endif>
                    @foreach($service->blocks()['recommendations']->body['block'] ?? [] as $i => $item)
                        <li class="page_service_long__recommendations__item">
                            <div class="page_service_long__recommendations__item__title__wrap">
                                <p class="page_service_long__recommendations__item__title">
                                    {{ $item['title'] ?? '' }}
                                </p>
                            </div>
                            <div class="page_service_long__recommendations__item__text__wrap">
                                <p class="page_service_long__recommendations__item__text">
                                    {{ $item['text'] ?? '' }}
                                </p>
                            </div>
                            <div class="page_service_long__recommendations__item__number__wrap">
                                <span class="page_service_long__recommendations__item__number">
                                    {{ $i }}
                                </span>
                            </div>
                        </li>
                    @endforeach
                    @if($attention = ($service->blocks()['recommendations']->body['block_attention'] ?? false))
                            <li class="page_service_long__recommendations__item _important">
                                <p class="page_service_long__recommendations__item__title">
                                    {{ $attention['title'] ?? '' }}
                                </p>
                                <div class="page_service_long__recommendations__item__text__wrap">
                                    <div class="page_service_long__recommendations__item__text">
                                        {!! $attention['text'] ?? '' !!}
                                    </div>
                                </div>
                                <div class="page_service_long__recommendations__item__number__wrap">
                            <span class="page_service_long__recommendations__item__number">
                                !
                            </span>
                                </div>
                            </li>
                    @elseif($service->page['body']['long']['recommendations']['text'] ?? false)
                        <li class="page_service_long__recommendations__item _important">
                            <p class="page_service_long__recommendations__item__title">
                                {{ __('pages.service.long.recommendations.important.title') }}
                            </p>
                            <div class="page_service_long__recommendations__item__text__wrap">
                                <div class="page_service_long__recommendations__item__text">
                                    {!! $service->page['body']['long']['recommendations']['text'] ?? '' !!}
                                </div>
                            </div>
                            <div class="page_service_long__recommendations__item__number__wrap">
                            <span class="page_service_long__recommendations__item__number">
                                !
                            </span>
                            </div>
                        </li>
                    @endif
                </ul>

            </div>

            <div class="overflow-hidden">
                <div class="container py-10 page_service_long__social">

                    <h2 id="block-social-title" data-url="{{ route('blocks.services.social') }}">
                        {{ __('pages.service.long.social.title') }}
                        @include('partials.icons.arrow_down')
                    </h2>
                    <div id="block-social-subtitle" class="page_service_long__social__subtitle"></div>

                    @include('partials.blocks.carousel.videos')

                </div>
            </div>

            @if(count($qa['items'] ?? []) > 0)
                <div class="py-10 page_service_long__faq">
                    <div class="container">

                        <h2>
                            {{ __('pages.service.long.faq.title') }}
                        </h2>

                        <ul class="page_service_long__faq__tags">
                            <li class="page_service_long__faq__tag _active"
                                data-faq="{{ route('blocks.services.faq', ['service' => $service, 'category' => -1]) }}">
                                {{ __('pages.service.long.faq.tags.all') }}
                            </li>
                            @foreach($qa['tags'] as $i => $tag)
                                <li class="page_service_long__faq__tag"
                                    data-faq="{{ route('blocks.services.faq', ['service' => $service, 'category' => $tag]) }}">
                                    {{ $tag->name }}
                                </li>
                            @endforeach
                        </ul>

                        <div class="page_service_long__faq__list_wrap">
                            <div id="faq-list" class="page_service_long__faq__list">
                                @include('partials.blocks.faq.faq_list', ['service' => $service, 'qa' => $qa])
                            </div>
                            <div class="page_service_long__faq__add_wrap hidden">
                                <div class="page_service_long__faq__add">
                                    <p class="page_service_long__faq__add__count">
                                        {{ $qa['total'] }} вопросов
                                    </p>
                                    <a class="btn btn_primary">
                                        Смотреть все
                                    </a>
                                    <a class="btn btn_primary _green">
                                        Задать вопрос
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            @endif

            @php $photos = $photoService->getPhotos(150) @endphp
            @if(count($photos) > 0)
                <div class="container py-10 page_service_long__photos">

                    <h2>
                        {{ __('pages.service.long.photos.title') }}
                    </h2>

                    <ul class="page_service_long__photos__list">
                        @foreach($photos as $i => $photo)
                            <li class="@if($i > (\Agent::isMobile() ? 7 : 15)) hidden @endif">
                                @if($i < (\Agent::isMobile() ? 7 : 15))
                                    <a href="{{ $photo['photo'] ?? '' }}" data-jbox-image="photos-gallery"
                                       data-jbox-title="{{ $photo['title'] ?? '' }}"
                                       class="page_service_long__photos__photo">
                                        <img src="{{ $photo['photo'] ?? '' }}">
                                    </a>
                                @elseif($i > (\Agent::isMobile() ? 7 : 15))
                                    <a href="{{ $photo['photo'] ?? '' }}" data-jbox-image="photos-gallery"
                                       data-jbox-title="{{ $photo['title'] ?? '' }}" class="hidden"></a>
                                @else
                                    <a href="{{ $photo['photo'] ?? '' }}" data-jbox-image="photos-gallery"
                                       data-jbox-title="{{ $photo['title'] ?? '' }}"
                                       class="page_service_long__photos__photo _link" data-text="Смотреть еще">
                                        <img src="{{ $photo['photo'] ?? '' }}">
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    </ul>

                    @if(count($photos) > (\Agent::isMobile() ? 7 : 15))
                        <a onclick="$('.page_service_long__photos__photo._link').click()"
                           class="page_service_long__reviews_yandex__link">
                            Смотреть еще
                        </a>
                    @endif

                </div>
            @endif

            <div class="page_service_long__prices">

                <div class="container py-10">

                    <h2>
                        {{ __('pages.service.long.prices.title') }}
                    </h2>

                    <ul class="page_service_long__content__links"
                        data-flickity='{ "cellAlign": "left", "freeScroll": true, "contain": true, "prevNextButtons": false, "pageDots": false }'>
                        @php $currentSet = false; @endphp
                        @foreach(__('pages.service.long.prices.links') as $link => $text)
                            <li class="page_service_long__content__link {{ $currentSet === false ? '_active' : '' }}"
                                @php $currentSet = true; @endphp
                                data-tab-group="#block-prices-content"
                                data-tab-btn="{{ $link }}">
                                {{ $text }}
                            </li>
                        @endforeach
                    </ul>

                    <div id="block-prices-content">

                        <div class="_tab _active" id="block-prices-content-table">
                            <table class="page_service_long__prices__list">
                                @foreach($service->prices as $price)
                                    <tr class="page_service_long__prices__item">
                                        <td class="page_service_long__prices__item__name">
                                            {!! $price->name !!}
                                            <span class="page_service_long__prices__item__price block lg:hidden">
                                            {{ __('pages.service.long.prices.item', [
                                            'price' => $price->price
                                        ]) }}
                                        </span>
                                        </td>
                                        <td class="text-center">
                                        <span class="page_service_long__prices__item__price hidden lg:inline">
                                            {{ __('pages.service.long.prices.item', [
                                            'price' => $price->price
                                        ]) }}
                                        </span>

                                            <a href="#callback-block"
                                               class="btn btn_outline_main _mobile">
                                                {{ __('blocks.price.button') }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#callback-block"
                                               class="btn btn_outline_main">
                                                {{ __('blocks.price.button') }}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>

                        <div id="block-prices-content-text-1" class="_tab"
                             data-tab-url="{{ route('blocks.services.tab', [
                                'service' => $service,
                                'tab' => 'default.prices',
                            ]) }}">
                        </div>

                    </div>

                </div>

            </div>

            <div class="container py-10 page_service_long__callback" id="callback-block">

                <h2 class="page_service_long__callback__title">
                    {{ __('pages.service.long.callback.title') }}
                </h2>

                <ul class="page_service_long__callback__list">
                    @foreach(__('pages.service.long.callback.steps') as $i => $step)
                        <li class="page_service_long__callback__step">
                        <span class="page_service_long__callback__step__number__wrap">
                            <span class="page_service_long__callback__step__number">
                                {{ $i + 1 }}
                            </span>
                        </span>
                            <p class="page_service_long__callback__step__title">
                                {{ $step['title'] ?? '' }}
                            </p>
                            <p class="page_service_long__callback__step__text">{{ $step['text'] ?? '' }}</p>
                        </li>
                    @endforeach
                </ul>

                <div class="callback-form-wrap">
                    <form action="{{ route('blocks.callback.store') }}" method="POST"
                          class="page_service_long__callback__form callback-form">
                        <div class="fields">
                            <input type="text" name="name"
                                   class="form-control"
                                   placeholder="{{ __('blocks.forms.inputs.name') }}">
                            <input type="text" name="phone"
                                   class="form-control" required
                                   placeholder="{{ __('blocks.forms.inputs.phone') }}">
                            @isset(request()->service)
                                <input type="hidden" name="service_id" value="{{ $service->id }}">
                            @endisset
                        </div>
                        <div class="page_service_long__callback__form__agreement">
                            <label class="checkbox__wrap">
                                <input type="checkbox" name="agreement" value="1" required>
                                <span class="checkbox">
                            {{ __('pages.service.long.callback.agreement.text') }}
                        </span>
                            </label>
                        </div>
                        <div class="flex justify-center">
                            <button class="form-control btn">
                                @include('partials.icons.shield')

                                {{ __('blocks.contacts.button') }}
                            </button>
                        </div>
                        @csrf
                        <div class="sent_wrap">
                            <span class="title">{{ __('blocks.forms.thanks.title') }}</span>
                            <p>{!! __('blocks.forms.thanks.text') !!}</p>
                        </div>
                    </form>
                </div>

            </div>

            @include('partials.blocks.services.services_related')

            @include('partials.blocks.maps.map_service')

    </section>
@endsection
