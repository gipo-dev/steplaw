@extends('partials.body')

@section('content')
    <div class="container container_small">
        @include('partials.blocks.breadcrumbs.breadcrumbs')
    </div>

    <div class="pb-8 page__inner page__post">
        <section class="container_small mx-auto pb-12 bg-white" style="padding-top: 0;">
            @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
                <div class="flex justify-end">
                    <a href="{{ route('platform.posts.edit', $post->id) }}"
                       class="btn btn_admin btn_sm">Редактировать</a>
                </div>
            @endif
            @include('partials.blocks.hr.hr_1')
            <h1 class="text-primary mb-6">{!! $post->name ?? '' !!}</h1>

            @include('partials.blocks.author.author')

            <div class="mb-5">
                <p>{{ $post->data['description'] ?? '' }}</p>
            </div>

            {!! \App\Facades\PageGenerator::renderMap($post->page->body ?? []) !!}

            {!! \App\Facades\PageGenerator::render($post->page->body ?? []) !!}

            <div class="mt-8">
                @include('partials.blocks.tags.tags')
            </div>
        </section>

        @if(count($related_posts) > 0)
            <section class="container_small mx-auto bg-white" style="margin-top: -55px;padding-top: 0;">
                <p class="text-xl font-bold px-1">{{ __('pages.posts.related.title') }}</p>
            </section>
            <section class="pt-3 container_small mx-auto bg-white page__post__related">
                <div class="flex flex-wrap">
                    @foreach($related_posts as $related_post)
                        <div class="md:px-3 md:w-1/2 lg:w-1/3 h-full">
                            @include('partials.blocks.post.post_item', ['post' => $related_post, 'small' => true])
                        </div>
                    @endforeach
                </div>
            </section>
        @endif

        <section class="container_small mx-auto bg-white page__post__comments">
            @include('partials.blocks.comments.comments')
        </section>

        <section class="pb-8 container_small mx-auto bg-white page__post__comments_add">
            @include('partials.blocks.comments.add', [
                'commentable_id' => $post->id,
                'commentable_type' => \App\Models\Post::class,
            ])
        </section>
    </div>
@endsection
