@extends('partials.body')

@section('content')
    <div class="section page__cases">
        <div class="container">
            <h1>
                {{ __('pages.case_page.list.title') }}
            </h1>

            <div class="case-categories__carousel page__cases__categories">
                @foreach($categories as $i => $category)
                    <a
                        {{--                        href="{{ route('page.cases.all', ['category' => $category]) }}" --}}
                        class="case">
                        {{ __('pages.case_page.categories.' . $category . '.small') }}
                    </a>
                @endforeach
            </div>

            <p class="page__cases__total">
                {{ __('pages.case_page.list.total', ['count' => $cases->total()]) }}
            </p>

            <div class="page__cases__list ">
                @foreach($cases as $i => $case)
                    <div class="page__cases__case">
                        <div class="page__cases__case__number">
                            {{ (((request()->page ?? 1) - 1) * 15) + $i + 1 }}
                        </div>
                        <div class="w-full">
                            <p class="page__cases__case__title
                                @if($case->files && count($case->files) > 0) _has-pdf @endif">
                                <a href="{{ route('page.cases.case', $case) }}">
                                    {{ $case->name }}
                                </a>
                            </p>
                            <table class="page__cases__case__properties">
                                @foreach(__('pages.case_page.properties') as $key => $name)
                                    <tr>
                                        <td class="page__cases__case__properties__name">
                                            {{ $name }}
                                        </td>
                                        <td class="page__cases__case__properties__value">
                                            @isset($case->properties[$key])
                                                {{ $case->properties[$key] }}
                                            @endisset
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="page__cases__case__properties__name">
                                        {{ __('pages.case_page.articles') }}
                                    </td>
                                    <td class="page__cases__case__properties__value">
                                        @if($case->articles)
                                            @foreach($case->articles as $index => $article)
                                                <a
                                                    {{--                                                href="{{ route('page.cases.all', ['art' => $article]) }}"--}}
                                                    class="text-primary">
                                                    {{ $case::getArticleName($article) }}</a>@if($index < count($case->articles) - 1)
                                                    ,@endif
                                            @endforeach
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="page__cases__case__person">
                            @if($case->person)
                                @include('partials.blocks.author.author', ['author' => $case->person, 'post' => $case])
                            @else
                                <div class="flex mb-5 block__author"></div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
            {!! $cases->links() !!}
        </div>

        {{--        <section class="container mx-auto pb-12 flex flex-wrap page__posts__wrap">--}}
        {{--            @if($posts->count() > 0)--}}
        {{--                @foreach($posts as $post)--}}
        {{--                    <div class="md:px-3 mb-6 w-full md:w-1/2 lg:w-1/3 h-full">--}}
        {{--                        @include('partials.blocks.post.post_item')--}}
        {{--                    </div>--}}
        {{--                @endforeach--}}
        {{--            @else--}}
        {{--                <h3 class="text-center font-bold text-xl block w-full">{{ __('pages.posts.no_posts') }}</h3>--}}
        {{--            @endif--}}
        {{--        </section>--}}
    </div>
@endsection
