@extends('partials.body')

@section('content')
    <div class="section page__pay">
        <div class="container">
            <h1>{{ __('pages.payment.pay.title') }}</h1>

            <div class="flex flex-col items-center">
                <p class="text-grey-500 text-xl text-center">
                    {!! __('pages.payment.pay.subtitle') !!}
                </p>
                <img src="/spb_qr.svg?v=2.1" style="max-width: 400px;">
            </div>

        </div>
    </div>
@endsection
