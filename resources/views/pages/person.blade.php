@extends('partials.body')

@section('content')
    <div class="page__inner page_person">

        <section class="container container_small mx-auto bg-white page_person__face"
                 style="background-image:url('{{ $person->getFirstMediaUrl('face') }}');">

            @include('partials.blocks.breadcrumbs.breadcrumbs')

            @include('partials.blocks.hr.hr_1')
            <h1 class="text-primary">{{ $person->name }}</h1>
            <div class="mt-6 block__text">
                <p class="font-semibold page_person__position">{{ $person->position ?? '' }}</p>
                <p class="whitespace-pre-line page_person__quote">{!! $person->page->field('about') !!}</p>
            </div>
            <blockquote class="mt-5 mb-16">«{!! $person->page->field('quote') !!}»</blockquote>
        </section>

        <section class="container container_small mx-auto bg-grey-400 text-white">
            <h2>{{ __('pages.person.profile') }}</h2>
            {!! $person->page->field('profile') !!}
        </section>

        @if(count($person->data['certificates'] ?? []) > 0)
            <section class="container container_small mx-auto bg-white">
                <h2 class="text-primary">{{ __('pages.person.certificates') }}</h2>
                <div class="mb-10">
                    <ul class="mb-6 list_unstyled history__files-carousel">
                        @php $certificates = $person->getAttachmentsByArray($person->data['certificates']) @endphp
                        @foreach($certificates as $file)
                            <li class="w-full md:w-1/2 xl:w-1/3 px-5 page_person__cert">
                                <a href="{{ $file->url }}" title="{{ $file->title }}" data-jbox-image="thanks">
                                    <img lazy data-src="{{ $file->url }}" alt="{{ $file->title }}">
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <div class="splide__arrows text-primary" style="margin-top: -24px;">
                        <button class="splide__arrow splide__arrow--prev">
                            @include('partials.icons.arrow_rigth')
                        </button>
                        <button class="splide__arrow splide__arrow--next">
                            @include('partials.icons.arrow_rigth')
                        </button>
                    </div>
                </div>
            </section>
        @endif

        <section class="container container_small mx-auto bg-white">
            <h2 class="text-primary">{{ __('pages.person.practice') }}</h2>
            <div class="block__text">
                {!! $person->page->field('practice') !!}
            </div>
        </section>

        <section class="container container_small mx-auto bg-white">
            <h2 class="text-primary">{{ __('pages.person.education') }}</h2>
            <div class="block__text">
                {!! $person->page->field('education') !!}
            </div>
        </section>

        @if(count($person->data['skills'] ?? []) > 0)
            <section class="container container_small mx-auto bg-white">
                <h2 class="text-primary">{{ __('pages.person.skills') }}</h2>
                <div class="flex flex-wrap page_person__skills">
                    @foreach($person->data['skills'] as $skill)
                        <div class="w-full md:w-1/3 mb-4 page_person__skills__skill">
                            <div>
                                @isset($skill['date'])
                                    <span class="text-grey-500 page_person__skills__skill__date">
                                        {{ \Carbon\Carbon::parse($skill['date'])->format('d.m.y') }}
                                    </span>
                                @endisset
                                <p class="page_person__skills__skill__title">
                                    {{ $skill['name'] ?? '' }}
                                </p>
                                @isset($skill['logo'])
                                    <img lazy data-src="{{ $skill['logo'] }}" alt="{{ $skill['logo'] }}">
                                @endisset
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        @endif

        @if($person->mentions->count() > 0)
            <section class="container container_small mx-auto bg-white">
                <h2 class="text-primary">{{ __('pages.person.mentions') }}</h2>
                <div class="flex flex-wrap mb-10 mentioning mentioning_big">
                    @foreach($person->mentions as $mention)
                        <div class="w-full md:w-1/2">
                            @include('partials.blocks.mentioning.mention_item_big')
                        </div>
                    @endforeach
                </div>
            </section>
        @endif

        @php $thanks = \App\Models\Thank::find($person->data['reviews'] ?? []) @endphp
        @if(count($thanks) > 0)
            <section class="container container_small mx-auto relative xs-px-0 bg-white thanks">
                @include('partials.blocks.hr.hr_1')
                <h2 class="text-primary mb-6">{{ __('pages.person.reviews', ['name' => $person->data['name']['gen'] ?? '']) }}</h2>
                <div class=" page__history__thanks">
                    <ul class="list_unstyled mb-2 thanks__carousel">
                        @foreach($thanks as $thank)
                            @include('partials.blocks.thanks.thanks_big')
                        @endforeach
                    </ul>
                    <div class="splide__arrows text-primary mt-2">
                        <button class="splide__arrow splide__arrow--prev">
                            @include('partials.icons.arrow_rigth')
                        </button>
                        <button class="splide__arrow splide__arrow--next">
                            @include('partials.icons.arrow_rigth')
                        </button>
                    </div>
                </div>
            </section>
        @endif

        @if($person->cases->count() > 0)
            <div class="cases">
                <div class="container container_small mx-auto bg-primary relative md:px-16 pt-10 pb-12">
                    @include('partials.blocks.hr.hr_1')
                    <h2 class="text-white mb-6 h-default">{{ __('pages.index.cases.title') }}</h2>
                    @include('partials.blocks.cases.cases_2', ['cases' => $person->cases])
                </div>
            </div>
        @endif

        @if($posts->count() > 0)
            <section class="container container_small mx-auto pb-12 page_person__news">
                <h2 class="text-primary">Новости</h2>
                @if($posts->count() > 0)
                    <div class="flex flex-wrap page__posts__wrap">
                        @foreach($posts as $post)
                            <div class="md:px-3 mb-3 md:mb-6 w-full md:w-1/2 lg:w-1/3 h-full">
                                @include('partials.blocks.post.post_item')
                            </div>
                        @endforeach
                    </div>
                    {{ $posts->links() }}
                @else
                    <h3 class="text-center font-bold text-xl block w-full">{{ __('pages.posts.no_posts') }}</h3>
                @endif
            </section>
        @endif

        <section class="container container_small mx-auto bg-white pb-20">
            <h2 class="text-white mb-6">{{ __('blocks.cases.title') }}</h2>
            @include('partials.blocks.forms.horizontal_2')
        </section>

    </div>
@endsection
