@extends('partials.body')

@section('content')
    <section class="bg-grey-100 overflow-hidden advantages">
        <div class="container flex mx-auto relative">
            <div class="absolute left-0 bottom-0 w-full flex z-10">
                <div class="advantages__img advantages__img_1 flex justify-end md:pr-16">
                    <img lazy src="@include('partials.icons.null_image')" data-src="/img/Sokolov_Sait.png"
                         alt="{{ __('pages.index.advantages.persons.sokolov') }}">
                </div>
                <div class="advantages__img advantages__img_2 md:pl-16">
                    <img lazy src="@include('partials.icons.null_image')" data-src="/img/Trusov_Sait.png"
                         alt="{{ __('pages.index.advantages.persons.trusov') }}">
                    <img src="/img/trusov_m_bg_kiborg_real.png"
                         alt="{{ __('pages.index.advantages.persons.trusov') }}"
                         class="absolute top-0 kiborg transition ease-in-out duration-300 opacity-0">
                </div>
            </div>
            <div class="z-20 flex flex-col align-middle text-center relative font-semibold w-full advantages__block">
                <div class="mb-6 advantages__emblem" lazy data-src="/img/emblem.png"></div>
                <p class="text-primary text-3xl md:whitespace-pre-line advantages__title">{{ __('pages.index.advantages.title') }}</p>
                <div class="uppercase mt-6 advantages__items">
                    <span>>5 200</span>
                    <p>{!! __('pages.index.advantages.cases') !!}</p>
                    <span>{{ now()->year - 2005 }}</span>
                    <p>{!! __('pages.index.advantages.work') !!}</p>
                    <span>24/7</span>
                    <p>{!! __('pages.index.advantages.24/7') !!}</p>
                </div>
                <div class="text-xl text-primary advantages__links">
                    <a href="{{ route('page.persons.person', ['person' => 'sokolov-andrey-nikolaevich.html']) }}"
                       class="border-b-2 border-primary leading-1 advantages__links_1">
                        {{ __('pages.index.advantages.persons.sokolov') }}
                    </a>
                    <a href="{{ route('page.persons.person', ['person' => 'trusov-fedor-nikolaevich.html']) }}"
                       class="border-b-2 border-primary leading-1 advantages__links_2">
                        {{ __('pages.index.advantages.persons.trusov') }}
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="bg-primary text-white py-6 section services__wrap">
        <div class="container">
            @include('partials.blocks.hr.hr_1')
            <h2>{{ __('pages.index.services.title') }}</h2>
            @include('partials.blocks.services.services')
        </div>
    </section>

    <section class="bg-grey-100 section persons">
        <div class="container mx-auto relative">
            @include('partials.blocks.hr.hr_1')
            <h2 class="text-primary">{{ __('pages.index.persons.title') }}</h2>
            @include('partials.blocks.carousel.persons')
        </div>
    </section>

    @if($cases->count() > 0)
        <section class="section cases">
            <div class="container mx-auto relative xs-px-0">
                @include('partials.blocks.hr.hr_1')
                <h2 class="text-primary mb-6">{{ __('pages.index.cases.title') }}</h2>
                @include('partials.blocks.cases.cases')
            </div>
        </section>
    @endif

    <section class="bg-grey-100 section thanks">
        <div class="container mx-auto relative xs-px-0">
            @include('partials.blocks.hr.hr_1')
            <h2 class="text-primary mb-6">{{ __('pages.index.thanks.title') }}</h2>
            @include('partials.blocks.carousel.thanks', ['count' => 2])
        </div>
    </section>

    <section class="section partner" id="moment">
        <div class="container mx-auto relative text-white md:px-6">
            @include('partials.blocks.hr.hr_1')
            <h2 class="mb-5">{{ __('pages.index.partners.title') }}</h2>
            <p class="font-semibold mb-12">{{ __('pages.index.partners.description') }}</p>
            @include('partials.blocks.carousel.partner')
        </div>
    </section>

    <section class="bg-grey-100 section mentioning" id="smi">
        <div class="container mx-auto relative">
            @include('partials.blocks.hr.hr_1')
            <h2 class="text-primary mb-4">{{ __('pages.index.mentioning.title') }}</h2>
            @include('partials.blocks.mentioning.mentioning')
        </div>
    </section>

    <section class="section">
        <div class="container mx-auto relative">
            @include('partials.blocks.hr.hr_1')
            <h1 class="text-primary mb-6">{{ $page->field('title') }}</h1>
            <div class="page__inner block__text">
                @php $cols = explode('<p><!-- pagebreak --></p>',$page->field('content')); @endphp
                @include('partials.blocks.texts.2', [
                    'col_1' => $cols[0] ?? '',
                    'col_2' => $cols[1] ?? '',
                ])
            </div>
        </div>
    </section>

    <section class="bg-grey-100 section" id="callback">
        <div class="container mx-auto">
            @include('partials.blocks.forms.horizontal_1')
        </div>
    </section>
@endsection
