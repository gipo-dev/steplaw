@extends('partials.body')

@section('content')
    <section class="page__inner page_contacts">
        <div class="flex flex-wrap container">
            <div class="w-full md:w-2/5 px-6 bg-white overflow-hidden section contacts">
                <h1 class="text-primary text-left">
                    {{ $page->field('title', true) ?? __('pages.contacts.title') }}
                </h1>

                <p class="contacts__name">Адвокатское бюро г. Москвы «Соколов, Трусов и Партнёры»</p>

                <div class="flex flex-wrap contacts__menu">
                    <div class="w-full md:w-1/2 px-1">
                        <p class="text-xl">
                            <strong>{{ __('pages.contacts.phone') }}</strong>
                        </p>
                        <span class="block font-semibold">
                            <a href="tel:{{ \Settings::getPhoneLink('phone_1') }}">
                                {{ \Settings::get('phone_1') }}
                            </a>
                        </span>
                        <span class="block font-semibold mt-1">
                            <a href="tel:{{ \Settings::getPhoneLink('phone_2') }}">
                                {{ \Settings::get('phone_2') }}
                            </a>
                        </span>
                        <p class="text-sm">{{ \Settings::get('duty_lawyer') }}</p>
                    </div>
                    <div class="w-full md:w-1/2 px-1">
                        <p class="text-xl">
                            <strong>{{ __('pages.contacts.email') }}</strong>
                        </p>
                        <p class="font-semibold">
                            <a href="mailto:{{ \Settings::get('email') }}">
                                {{ \Settings::get('email') }}
                            </a>
                        </p>
                    </div>
                    <div class="w-full mt-3 px-1">
                        <p class="text-xl">
                            <strong>{{ __('pages.contacts.address') }}</strong>
                        </p>
                        <p>{{ \Settings::get('address') }}</p>
                    </div>
                    <div class="w-full mt-3 px-1">
                        <p class="text-xl">
                            <strong>{{ __('pages.contacts.work_time') }}</strong>
                        </p>
                        <p>{{ \Settings::get('work_time') }}</p>
                    </div>
                    <div class="w-full mt-3 px-1">
                        <p class="text-xl">
                            <strong>{{ __('pages.contacts.requisites') }}</strong>
                        </p>
                        <p style="white-space: pre-line; margin-bottom: 0;">{{ \Settings::get('requisites') }}</p>
                    </div>
                    <hr>
                    <div class="mt-3 w-full">
                        @include('partials.blocks.forms.form_contacts')
                    </div>
                </div>
            </div>
            <div class="w-full md:w-3/5 contacts__map" id="full-map">
                <iframe
                    src="https://yandex.ru/map-widget/v1/?um=constructor%3Af32e28392b057a9c82b15098cc357fb56815875347728fd2a0ea7727a34315f6&amp;source=constructor"
                    width="100%" height="100%" frameborder="0"></iframe>
            </div>
        </div>
    </section>
@endsection
