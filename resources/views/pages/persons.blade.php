@extends('partials.body')

@section('content')

    <div class="container container_small">
        @include('partials.blocks.breadcrumbs.breadcrumbs')
    </div>
    
    <section class="container section page_persons">
        @include('partials.blocks.hr.hr_1')
        <h1 class="text-primary">
            {{ $page->field('title', true) ?? __('pages.index.persons.title') }}
        </h1>
        <div class="{{ !\Agent::isMobile() ? 'flex' : '' }}">
            @if(!\Agent::isMobile())
                <div class="w-1/6"></div>
            @endif
            <div class="{{ \Agent::isMobile() ? '' : 'w-4/6' }} mb-10 persons__carousel disable_carousel flex flex-wrap">
                @foreach($persons as $i => $person)
                    <div class="w-full md:w-1/3 py-3 md:py-5">
                        @include('partials.blocks.persons.person_item')
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="bg-grey-100 section">
        <div class="container mx-auto">
            @include('partials.blocks.forms.horizontal_1')
        </div>
    </section>
@endsection
