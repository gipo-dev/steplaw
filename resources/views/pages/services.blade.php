@extends('partials.body')

@section('content')

    <div class="bg-grey-300">
        <div class="container container_small">
            @include('partials.blocks.breadcrumbs.breadcrumbs')
        </div>
    </div>

    <section class="page_service">
        @foreach($categories as $i => $category)
            <div class="section lg:pt-1 px-4 @if($i == count($categories) - 1) @elseif($i % 2) bg-grey-500 splide_white @else bg-grey-300 @endif">
                <div class="container mx-auto {{ $i == (count($categories) - 1) ? 'services_white' : '' }}">
                    @include('partials.blocks.hr.hr_1')
                    <h2 class="text-{{ $i % 2 ? 'white' : 'primary' }}">
                        {{ $category->name }}
                    </h2>
                    @include('partials.blocks.services.services_wrap', ['services' => $category->services, 'id' => $i, 'primary' => true])
{{--                    @if(\Agent::isMobile())--}}
{{--                        <div class="splide__arrows  @if($i % 2) text-white @else text-primary @endif mt-2 lg:hidden lg:mt-10">--}}
{{--                            <button class="splide__arrow splide__arrow--prev">--}}
{{--                                @include('partials.icons.arrow_rigth')--}}
{{--                            </button>--}}
{{--                            <button class="splide__arrow splide__arrow--next">--}}
{{--                                @include('partials.icons.arrow_rigth')--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    @endif--}}
                </div>
            </div>
        @endforeach
    </section>
    <section class="bg-grey-100 section">
        <div class="container mx-auto">
            @include('partials.blocks.forms.horizontal_1')
        </div>
    </section>
@endsection
