<div class="page__cases__list">
    @foreach($items as $i => $qa)

        <div class="page__cases__case">
            <div class="page__cases__case__number">
                {{ (((request()->page ?? 1) - 1) * 15) + $i + 1 }}
            </div>
            <div class="w-full">
                <p class="page__cases__case__title">
                    <a href="{{ route('page.qa.item', $qa) }}">
                        {{ $qa->name }}
                    </a>
                </p>
                <table class="page__cases__case__properties">
                    @foreach(__('pages.questions.properties') as $key => $name)
                        <tr>
                            <td class="page__cases__case__properties__name">
                                {{ $name }}
                            </td>
                            <td class="page__cases__case__properties__value">
                                @isset($qa->properties[$key])
                                    {{ $qa->properties[$key] }}
                                @endisset
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="page__cases__case__person">
                @if($qa->persons->count() > 0)
                    @foreach($qa->persons as $person)
                        @include('partials.blocks.author.author', ['author' => $person])
                    @endforeach
                @else
                    <div class="flex mb-5 block__author"></div>
                @endif
            </div>
        </div>

    @endforeach
</div>
