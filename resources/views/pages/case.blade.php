@extends('partials.body')

@section('content')
    <div class="container container_small">
        @include('partials.blocks.breadcrumbs.breadcrumbs')
    </div>

    <div class="container container_small">
        <div class="page__inner page_case">
            @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
                <section class="container container_small bg-white mx-auto">
                    <div class="flex justify-end">
                        <a href="{{ route('platform.casepages.edit', $case->id) }}" class="btn btn_admin btn_sm">Редактировать</a>
                    </div>
                </section>
            @endif

            <h1 class="text-primary">
                {{ $case->name }}
            </h1>
            @include('partials.blocks.author.author', ['author' => $case->person, 'post' => $case])

            <table class="page__cases__case__properties page_case__properties">
                @foreach(__('pages.case_page.properties') as $key => $name)
                    <tr>
                        <td class="page__cases__case__properties__name">
                            {{ $name }}
                        </td>
                        <td class="page__cases__case__properties__value">
                            @isset($case->properties[$key])
                                {{ $case->properties[$key] }}
                            @endisset
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td class="page__cases__case__properties__name">
                        {{ __('pages.case_page.articles') }}
                    </td>
                    <td class="page__cases__case__properties__value">
                        @if($case->articles)
                            @foreach($case->articles as $index => $article)
                                <a
                                    {{--                                href="{{ route('page.cases.all', ['art' => $article]) }}"--}}
                                    class="text-primary">
                                    {{ $case::getArticleName($article) }}</a>@if($index < count($case->articles) - 1)
                                    ,@endif
                            @endforeach
                        @endif
                    </td>
                </tr>
            </table>

            @if($case->page->body['description'] ?? false)
                <div class="page_case__description">
                    <h2 class="text-primary">
                        {{ __('pages.case_page.index.description') }}
                    </h2>
                    <div>
                        {!! $case->page->body['description'] ?? '' !!}
                    </div>
                </div>
            @endif

            @if($case->page->body['threatening'] ?? false)
                <div class="bg-grey-100 mt-12 page_case__threatening">
                    <h2>
                        {{ __('pages.case_page.index.threatening') }}
                    </h2>
                    <div>
                        {!! $case->page->body['threatening'] ?? '' !!}
                    </div>
                </div>
            @endif

            @if($case->page->body['result'] ?? false)
                <div class="mt-12 page_case__result">
                    <h2 class="text-primary">
                        {{ __('pages.case_page.index.result') }}
                    </h2>
                    <div>
                        {!! $case->page->body['result'] ?? '' !!}
                    </div>
                </div>
            @endif

            @if(count($case->getAttachments()) > 0)
                <div class="mt-12 page_case__documents">
                    <h2 class="text-primary">
                        {{ __('pages.case_page.index.documents') }}
                    </h2>
                    <ul class="page_case__documents__carousel">
                        @foreach($case->getAttachments() as $attachment)
                            <li class="flex" data-jbox-image="gallery1">
                                <iframe
                                    @if(app()->environment() == 'local')
                                    src="https://docs.google.com/gview?url=https://steplaw.ru/storage/2022/02/06/e6b6c378969b5a053f27c7360a2ce886d3c64bb4.pdf&embedded=true"
                                    @else
                                    src="https://docs.google.com/gview?url=https://steplaw.ru{{ $attachment['path'] ?? '' }}&embedded=true"
                                    @endif
                                    frameborder="0" class="page_case__documents__preview"></iframe>
                                {{--                                <div class="flex items-center ml-4 page_case__documents__preview__name">--}}
                                {{--                                    {{ $attachment['name'] ?? 'Прикрепленный файл' }}--}}
                                {{--                                </div>--}}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if($case->page->body['review'] ?? false)
                <div class="bg-grey-100 mt-12 page_case__review">
                    @include('partials.blocks.hr.hr_2')
                    <h2 class="text-primary page_case__review__title">
                        {{ __('pages.case_page.index.review') }}
                    </h2>
                    <div class="bg-white px-8 py-10">
                        <h2 class="text-primary page_case__review__name">
                            {{ $case->page->body['review_name'] ?? '' }}
                        </h2>
                        @if($case->page->body['review_date'] ?? false)
                            <p class="text-sm text-grey-400">
                                {{ \Carbon\Carbon::parse($case->page->body['review_date'])->format('d M Y') }}
                            </p>
                        @endif
                        <div>
                            {!! $case->page->body['review'] ?? '' !!}
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <section class="container container_small mx-auto bg-white pb-20" id="callback-block">
        <h2 class="text-white mb-6">{{ __('blocks.cases.title') }}</h2>
        @include('partials.blocks.forms.horizontal_2')
    </section>
@endsection
