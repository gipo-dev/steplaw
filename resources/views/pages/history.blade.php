@extends('partials.body')

@section('content')
    <div class="page__inner page__history">
        <section class="section container bg-white mx-auto">
            @include('partials.blocks.hr.hr_1')
            <div class="flex justify-center">
                <h1 class="text-primary">{{ $page->field('title') }}</h1>
            </div>
            <div class="block__text">
                {!! $page->field('content') !!}
            </div>

            @if(isset($page->body['steps']) && count($page->body['steps']) > 0)
                <div class="page__history__steps">
                    <ol>
                        @foreach($page->body['steps'] as $step)
                            <li>
                                <div>
                                    <h4>
                                        {{ $step['title'] ?? '' }}
                                    </h4>
                                    {!! $step['content'] ?? '' !!}
                                </div>
                            </li>
                        @endforeach
                    </ol>
                </div>
            @endif
        </section>

        <section class="section container bg-primary mx-auto">
            @include('partials.blocks.hr.hr_1')
            <div class="flex justify-center">
                <p class="text-center text-white font-bold page__history__now">
                    {{ $page->body['comment'] }}
                </p>
            </div>
        </section>

        <section class="section container bg-white mx-auto hidden">
            <div class="flex justify-around page__history__features">
                @foreach($page->body['features'] ?? [] as $feature => $content)
                    <div class="page__history__features__feature">
                        <img class="page__history__features__img"
                             src="/img/features/{{ $feature }}.png" alt="{{ $feature }}">
                        <p class="page__history__features__title">
                            {{ $content['title'] ?? '' }}
                        </p>
                        <div class="page__history__features__content">
                            {!! $content['content'] ?? '' !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </section>

        <section class="container _fw mx-auto persons bg-white overflow-hidden">
            @include('partials.blocks.hr.hr_1')
            <h2 class="text-primary">{{ __('pages.index.persons.title') }}</h2>
            @include('partials.blocks.carousel.persons_small')
        </section>

        @php $certificates = $page->getAttachmentsByArray($page->body['certificates']['files'] ?? []) @endphp
        @if($certificates->count() > 0)
            <section class="container mx-auto bg-white overflow-hidden ds page__history__certificates">
                @include('partials.blocks.hr.hr_1')
                <h2 class="text-primary">{{ $page->body['certificates']['title'] ?? '' }}</h2>
                <div class="mb-10">
                    {!! $page->body['certificates']['text'] ?? '' !!}
                </div>

                <ul class="mb-6 list_unstyled history__files-carousel">
                    @foreach($certificates as $file)
                        <li class="w-full md:w-1/2 xl:w-1/3 px-5 history__files-carousel__item">
                            <a href="{{ $file->url }}" title="{{ $file->title }}" data-jbox-image="certificates">
                                <img src="{{ $file->url }}" alt="{{ $file->title }}">
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="splide__arrows text-primary mt-2">
                    <button class="splide__arrow splide__arrow--prev">
                        @include('partials.icons.arrow_rigth')
                    </button>
                    <button class="splide__arrow splide__arrow--next">
                        @include('partials.icons.arrow_rigth')
                    </button>
                </div>
            </section>
        @endif

        <section class="container mx-auto persons bg-white overflow-hidden ds">
            @include('partials.blocks.hr.hr_1')
            <h2 class="text-primary">{{ $page->body['clients']['title'] ?? '' }}</h2>
            <div>
                {!! $page->body['clients']['text'] ?? '' !!}
            </div>
        </section>

        @php $thanks = $page->getAttachmentsByArray($page->body['thanks']['files'] ?? []) @endphp
        @if($thanks)
            <section class="container mx-auto persons bg-white overflow-hidden ds page__history__thanks">
{{--                @include('partials.blocks.hr.hr_1')--}}
{{--                <h2 class="text-primary">{{ __('pages.history.letters.title') }}</h2>--}}

                <ul class="mb-6 list_unstyled history__files-carousel">
                    @foreach($thanks as $file)
                        <li class="w-full md:w-1/2 xl:w-1/3 px-5 history__files-carousel__item">
                            <a href="{{ $file->url }}" title="{{ $file->title }}" data-jbox-image="thanks">
                                <img src="{{ $file->url }}" alt="{{ $file->title }}">
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="splide__arrows text-primary mt-2">
                    <button class="splide__arrow splide__arrow--prev">
                        @include('partials.icons.arrow_rigth')
                    </button>
                    <button class="splide__arrow splide__arrow--next">
                        @include('partials.icons.arrow_rigth')
                    </button>
                </div>
            </section>
        @endif

        @php $thanks = \App\Models\Thank::find($page->body['cases'] ?? []) @endphp
        <section class="container mx-auto relative xs-px-0
            bg-white thanks page__history__thanks">
            @include('partials.blocks.hr.hr_1')
            <h2 class="text-primary mb-6">{{ __('pages.history.thanks.title') }}</h2>
            <div>
                <ul class="list_unstyled mb-2 thanks__carousel">
                    @foreach($thanks as $thank)
                        @include('partials.blocks.thanks.thanks_big')
                    @endforeach
                </ul>
                <div class="splide__arrows text-primary mt-2">
                    <button class="splide__arrow splide__arrow--prev">
                        @include('partials.icons.arrow_rigth')
                    </button>
                    <button class="splide__arrow splide__arrow--next">
                        @include('partials.icons.arrow_rigth')
                    </button>
                </div>
            </div>
        </section>

        <section class="container mx-auto bg-white pb-20" id="callback-block">
            <h2 class="text-white mb-6">{{ __('blocks.cases.title') }}</h2>
            @include('partials.blocks.forms.horizontal_2')
        </section>

    </div>
@endsection
