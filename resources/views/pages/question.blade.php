@extends('partials.body')

@section('content')
    <div class="container container_small">
        @include('partials.blocks.breadcrumbs.breadcrumbs')
    </div>

    <div class="container container_small">
        <div class="page__inner page_case page__question">
            @if(\Auth::user() && \Auth::user()->hasAccess('platform.index'))
                <section class="container container_small bg-white mx-auto">
                    <div class="flex justify-end">
                        <a href="{{ route('platform.qas.edit', $qa->id) }}"
                           class="btn btn_admin btn_sm">Редактировать</a>
                    </div>
                </section>
            @endif

            <h1 class="text-primary">
                {{ $qa->page->body['question_description'] ?? $qa->page->body['question'] ?? 'Без заголовка' }}
            </h1>

            <table class="page__cases__case__properties page_case__properties">
                @foreach(__('pages.questions.properties') as $key => $name)
                    <tr>
                        <td class="page__cases__case__properties__name">
                            {{ $name }}
                        </td>
                        <td class="page__cases__case__properties__value">
                            @isset($qa->properties[$key])
                                {{ $qa->properties[$key] }}
                            @endisset
                        </td>
                    </tr>
                @endforeach
            </table>

            <div class="py-5">
                @foreach($qa->persons as $person)
                    @include('partials.blocks.author.author', ['author' => $person])
                @endforeach
            </div>

            {{--            @if($qa->page->body['question'] ?? false)--}}
            {{--                <div class="page_case__description">--}}
            {{--                    <h2 class="text-primary">--}}
            {{--                        {{ __('pages.questions.index.question') }}--}}
            {{--                    </h2>--}}
            {{--                    <div>--}}
            {{--                        {!! $qa->page->body['question'] ?? '' !!}--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endif--}}

            {{--            @if($qa->page->body['question_description'] ?? false)--}}
            {{--                <div class="page_case__description">--}}
            {{--                    <h2 class="text-primary">--}}
            {{--                        {{ __('pages.questions.index.question_description') }}--}}
            {{--                    </h2>--}}
            {{--                    <div>--}}
            {{--                        {!! $qa->page->body['question_description'] ?? '' !!}--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            @endif--}}

            @if($qa->page->body['answer'] ?? false)
                <div class="page_case__description">
{{--                    <h2 class="text-primary">--}}
{{--                        {{ __('pages.questions.index.answer') }}--}}
{{--                    </h2>--}}
                    <div>
                        {!! $qa->page->body['answer'] ?? '' !!}
                    </div>
                </div>
            @endif

        </div>
    </div>

    <section class="page__inner mt-4 pt-10" style="background:transparent;">

        <section class="container_small mx-auto bg-white page__post__comments">
            @include('partials.blocks.comments.comments')
        </section>

        <section class="pb-8 container_small mx-auto bg-white page__post__comments_add">
            @include('partials.blocks.comments.add', [
                'comment' => __('pages.questions.index.comments.add'),
                'commentable_id' => $qa->id,
                'commentable_type' => \App\Models\QA::class,
            ])
        </section>

    </section>

    <section class="container container_small mx-auto bg-white pb-20" id="callback-block">
        @include('partials.blocks.forms.horizontal_2')
    </section>
@endsection
