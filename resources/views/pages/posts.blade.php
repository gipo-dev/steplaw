@extends('partials.body')

@section('content')
    <div class="bg-grey-300">
        <div class="container container_small">
            @include('partials.blocks.breadcrumbs.breadcrumbs')
        </div>
    </div>
    <div class="section lg:pt-1 lg:pb-4 bg-grey-300 page__posts">
        @include('partials.blocks.hr.hr_1')
        <h2 class="text-primary">
            {{ $category->name ?? __('pages.posts.title') }}
        </h2>

        <section class="container mx-auto pb-12">
            @if($posts->count() > 0)
                <div class="flex flex-wrap page__posts__wrap">
                    @foreach($posts as $post)
                        <div class="md:px-3 mb-3 md:mb-6 w-full md:w-1/2 lg:w-1/3 h-full">
                            @include('partials.blocks.post.post_item')
                        </div>
                    @endforeach
                </div>
                {{ $posts->links() }}
            @else
                <h3 class="text-center font-bold text-xl block w-full">{{ __('pages.posts.no_posts') }}</h3>
            @endif
        </section>
    </div>
@endsection
