@extends('partials.body')

@section('content')
    <div class="section page__cases page__questions">
        <div class="container">
            <h1>
                {{ __('pages.questions.list.title') }}
            </h1>

            <div class="case-categories__carousel page__cases__categories">
                @foreach($services as $i => $service)
{{--                    <a href="{{ route('page.qa.all', ['service' => $service->id]) }}" class="case">--}}
                    <a class="case" data-service-id="{{ $service->id }}">
                        {{ $service->name }}
                    </a>
                @endforeach
            </div>

            <p class="page__cases__total">
                {{ __('pages.case_page.list.total', ['count' => $items->total()]) }}
            </p>

            @include('pages.partials.faqs', ['items' => $items])

            {!! $items->links() !!}
        </div>
    </div>
@endsection
