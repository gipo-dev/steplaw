@extends('partials.body')

@section('content')
    <div class="page__inner page_default">
        <section class="section container container_small bg-white mx-auto">
            @include('partials.blocks.hr.hr_1')
            <h1 class="text-primary">{{ $page->field('title') }}</h1>
            <div class="block__text">
                {!! $page->field('content') !!}
            </div>
        </section>
    </div>
@endsection
