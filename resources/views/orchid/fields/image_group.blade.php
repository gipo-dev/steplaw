<a href="{{ $link }}">
    @foreach($images as $url => $name)
        <img src="{{ $url }}" style="max-width:100px;max-height: 100px;margin-right: 5px;">
    @endforeach
</a>
