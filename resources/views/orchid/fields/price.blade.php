<div class="field-prices" data-name="{{ $name }}" data-controller="fields--price">
    <div class="wrap">
        @foreach($value as $i => $price)
            @php /** @var App\Models\Price $price **/ @endphp
            <div class="row align-items-baseline" data-price-id="{{ $i }}">
                <div class="col  pr-0 ">
                    <div class="form-group">
                        <label for="field-price-name-{{ $i }}" class="form-label">Название</label>
                        <div>
                            <input class="form-control" name="{{ $name }}[{{ $i }}][name]" title="Название"
                                   required value="{{ $price->name ?? '' }}" id="field-name-price-{{ $i }}">
                        </div>
                    </div>
                </div>
                <div class="col  pr-0 ">
                    <div class="form-group">
                        <label for="field-price-value-{{ $i }}" class="form-label">Цена</label>
                        <div>
                            <input type="number" class="form-control" name="{{ $name }}[{{ $i }}][price]"
                                   title="Цена" required value="{{ $price->price ?? '' }}"
                                   id="field-price-value-{{ $i }}">
                        </div>
                    </div>
                </div>
                <div class="col  pr-0 ">
                    <div class="form-group">
                        <label for="field-price-category-{{ $i }}" class="form-label">Категория</label>
                        <div>
                            <select class="form-control" name="{{ $name }}[{{ $i }}][category_id]" title="Цена"
                                    required id="field-price-category-{{ $i }}">
                                <option value="0" {{ ($price->category_id ?? 0) == 0 ? 'selected' : '' }}>
                                    Для юридических лиц
                                </option>
                                <option value="1" {{ ($price->category_id ?? 0) == 1 ? 'selected' : '' }}>
                                    Для физических лиц
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col  pr-0 ">
                    <label style="opacity: 0" class="form-label">1</label>
                    <button class="btn btn-danger" data-delete>Удалить</button>
                </div>
            </div>
        @endforeach
    </div>

    <div class="mt-2">
        <span class="btn btn-primary d-inline-block" data-add>Добавить</span>
    </div>
</div>
