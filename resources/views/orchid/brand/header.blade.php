@push('head')
    <link
        href="/favicon.ico"
        id="favicon"
        rel="icon"
    >
@endpush

<p class="h2 n-m font-thin v-center">
    <x-orchid-icon path="fingerprint"/>
    <span class="m-l ml-2 d-none d-sm-block">
        Steplaw
        <small class="v-top opacity">Admin</small>
    </span>
</p>
