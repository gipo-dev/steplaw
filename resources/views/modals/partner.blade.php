<div class="flex flex-wrap max-w-5xl w-full modal__partner">
    <div class="w-full xl:w-2/3 py-1 xl:pr-5 mb-3 xl:mb-0">
        <iframe src="{{ $video->src }}"
                title="YouTube video player" frameborder="0"
                allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>
    <div class="w-full xl:w-1/3 page__inner" style="background:#fff;">
        <p></p>
        {!! $video->text ?? '' !!}
    </div>
</div>
