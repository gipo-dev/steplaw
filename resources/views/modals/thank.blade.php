<div class="max-w-2xl">
    <h5 class="mt-2 font-semibold text-primary text-2xl" data-id="{{ $thank->id }}"
        data-url="{{ route('blocks.thank.modal', ['id' => $thank->id ]) }}">
        {{ $thank->name ?? '' }}
    </h5>
    @if($thank->service)
        <p class="mb-2">
        <span class="text-sm text-grey-400">{{ __('blocks.thanks.service') }}
            <a href="{{ $thank->service->getLink() }}"
               class="text-primary border-b border-primary">
                {{ $thank->service->name ?? '' }}
            </a>
        </span>
        </p>
    @endif
    <p class="font-medium mb-2" data-id="{{ $thank->id }}"
       data-url="{{ route('blocks.thank.modal', ['id' => $thank->id ]) }}">
        {!! $thank->body !!}
    </p>
</div>
