<div class="max-w-2xl cases__carousel">
    <div class="case" data-id="{{ $case->id }}"
         data-url="{{ route('blocks.case.modal', ['id' => $case->id ]) }}">
        <div class="p-2">
            <h4 class="mb-1 font-bold text-grey-400 lg:text-xl uppercase">
                {{ __('blocks.cases.summary') }}
            </h4>
            <div class="font-medium case__text">{!! $case->body ?? '' !!}</div>
            <h4 class="mt-4 mb-1 font-bold text-grey-400 lg:text-xl uppercase">
                {{ __('blocks.cases.result') }}
            </h4>
            <div class="font-medium case__text">{!! $case->result !!}</div>
        </div>
    </div>
</div>
