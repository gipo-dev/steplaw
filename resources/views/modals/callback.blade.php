<section class="modal__callback">
    <span class="modal-callback-close tr">@include('partials.icons.close')</span>
    <div class="px-4 modal__callback__container callback-form-wrap">
        <p class="modal__callback__title">{{ __('blocks.forms.modal.title') }}</p>
        <p class="text-md md:text-xl whitespace-pre-line mt-2 md:mt-0 mb-5">{{ __('blocks.forms.modal.text') }}</p>
        <div class="flex justify-center mb-5 modal__callback__messengers">
            <a href="{{ \Settings::get('telegram') }}" target="_blank" class="block text-xl mr-3 lg:mr-5">
                @include('partials.icons.telegram')
            </a>
            <a href="{{ \Settings::get('whatsapp') }}" target="_blank" class="block text-xl">
                @include('partials.icons.whatsapp')
            </a>
        </div>
        <form action="{{ route('blocks.callback.store') }}" method="POST" class="mb-10 contacts__form callback-form">
            <div class="fields">
                <input type="text" name="name"
                       class="form-control"
                       placeholder="{{ __('blocks.forms.inputs.name') }}">
                <input type="text" name="phone"
                       class="form-control" required
                       placeholder="{{ __('blocks.forms.inputs.phone') }}*">
                @isset(request()->service)
                    <input type="hidden" name="service_id" value="{{ $service->id }}">
                @endisset
                {{--                <textarea name="message" rows="5" class="form-control" required--}}
                {{--                          placeholder="{{ __('blocks.forms.inputs.message') }}"></textarea>--}}
                <div class="sent_wrap">
                    <span>{{ __('blocks.forms.thanks.title') }}</span>
                    <p>{!! __('blocks.forms.thanks.text') !!}</p>
                </div>
            </div>
            <div class="flex justify-center">
                <button class="mt-3 form-control btn_main">
                    {{ __('blocks.contacts.button') }}
                </button>
            </div>
            @csrf
            <div class="sent_wrap">
                <span class="title">{{ __('blocks.forms.thanks.title') }}</span>
                <p>{!! __('blocks.forms.thanks.text') !!}</p>
                <div class="flex justify-center mt-8">
                    <span class="modal-callback-close btn btn_primary">{{ __('blocks.forms.modal.close') }}</span>
                </div>
            </div>
        </form>
    </div>
</section>
