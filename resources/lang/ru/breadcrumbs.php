<?php

return [
    'index' => 'Главная',
    'services' => 'Услуги',
    'cases' => 'Дела',
    'journal' => 'Журнал',
    'persons' => 'Команда',
    'qa' => [
        'all' => 'Вопросы и ответы',
        'item' => 'Вопрос',
    ],
];
