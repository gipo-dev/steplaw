<?php

return [
    'menu' => [
        'index' => 'Главная',
        'persons' => 'Команда',
        'services' => 'Услуги',
        'pro-bono' => 'Pro bono',
        'mentioning' => 'Мы в СМИ',
        'contacts' => 'Контакты',
        'posts' => 'Публикации',
        'about' => 'О нас',
        'callback' => 'Оставить заявку',
        'journal' => 'Журнал',
        'payment' => 'Оплата',
    ],

    'page' => [
        'type' => [
            \App\Models\InnerPage::class => 'Внутренняя страница',
            \App\Models\PageBlock::class => 'Блок страницы',
        ],

        'name' => [
            'page' => [
                'index' => 'Главная',
                'contacts' => ['index' => 'Контакты'],
                'persons' => ['all' => 'Адвокаты'],
                'services' => ['all' => 'Услуги'],
                'about' => 'О нас',
                'posts' => ['all' => 'Публикации'],
                'cases' => ['all' => 'Наша практика'],
                'officeHistory' => 'История бюро',
                'payment' => ['pay' => 'Оплата услуг'],
                'qa' => ['all' => 'FAQ'],
                'blocks' => [
                    'recommendations' => 'Как выбрать хорошего адвоката',
                ],
            ],
        ],
    ],

    'post' => [
        'status' => [
            \App\Models\Post::STATUS_PUBLISHED => 'Опубликован',
            \App\Models\Post::STATUS_DRAFT => 'Черновик',
            \App\Models\Post::STATUS_DISABLED => 'Отключен',
        ],
    ],

    'callback' => [
        'status' => [
            \App\Models\Callback::STATUS_NEW => 'Новая заявка',
            \App\Models\Callback::STATUS_PROCESS => 'Заявка обрабатывается',
            \App\Models\Callback::STATUS_PROCESSED => 'Заявка обработана',
            \App\Models\Callback::STATUS_REJECT => 'Заявка отменена',
            \App\Models\Callback::STATUS_SPAM => 'Спам',
        ],
    ],

    'casepage' => [
        'article' => [
            'number' => 'ст. :number',
            'part' => ' ч. :number',
        ],
    ],
];
