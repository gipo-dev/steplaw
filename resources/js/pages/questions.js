$(function () {

    if ($('.page__questions').length === 0)
        return

    $('.page__questions [data-service-id]')
      .click(function () {
          let list = $('.page__cases__list')
          let serviceId = $(this).data('service-id')

          list.addClass('_loading')

          $.ajax({
              url: '/questions/ajax',
              data: {
                  service: serviceId,
              },
              success: function (resp) {
                  list.html(resp)
              },
              complete: function (resp) {
                  list.removeClass('_loading')
                  window.loadImages()
              },
          })
      })

})
