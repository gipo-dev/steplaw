export default class extends window.Controller {

    connect() {
        let element = this.element;

        $(element).on('click', '[data-delete]', function () {
            $(this).parents('[data-price-id]').remove();
        });

        $(element).on('click', '[data-add]', function () {
            let field = $(element);

            let name = field.data('name');
            let id = field.find('.wrap > div').length + 1;

            field.find('.wrap').append(`<div class="row align-items-baseline" data-price-id="` + id + `">
                <div class="col  pr-0 ">
                    <div class="form-group">
                        <label for="field-price-name-` + id + `" class="form-label">Название</label>
                        <div>
                            <input class="form-control" name="` + name + `[` + id + `][name]" title="Название"
                                required value="" id="field-name-price-` + id + `">
                        </div>
                    </div>
                </div>
                <div class="col  pr-0 ">
                    <div class="form-group">
                        <label for="field-price-value-` + id + `" class="form-label">Цена</label>
                        <div>
                                <input type="number" class="form-control" name="` + name + `[` + id + `][price]"
                                   title="Цена" required value="" id="field-price-value-` + id + `">
                        </div>
                    </div>
                </div>
                <div class="col  pr-0 ">
                    <div class="form-group">
                        <label for="field-price-category-` + id + `" class="form-label">Категория</label>
                        <div>
                            <select class="form-control" required name="` + field.data('name') + `[` + id + `][category_id]"
                                    title="Цена" id="field-price-category-` + id + `">
                                <option value="0">
                                    Для юридических лиц
                                </option>
                                <option value="1">
                                    Для физических лиц
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col  pr-0 ">
                    <label style="opacity: 0" class="form-label">1</label>
                    <button class="btn btn-danger" data-delete>Удалить</button>
                </div>
            </div>`);
        });
    }


    disconnect() {
        this.element.unbind();
    }

}
