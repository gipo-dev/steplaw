export default class extends window.Controller {

    changer = null;

    blocks = {};

    connect() {
        let that = this;
        this.changer = $('[name="page[body][template][type]"]');

        this.setupBlocks();
        this.changer.on('change', function () {
            that.changeType();
        });
        this.changeType();
    }


    disconnect() {
        this.changer.unbind();
    }

    changeType() {
        let type = this.changer.val();
        this.hideBlocks(type);
    }

    hideBlocks(type) {
        for (const [key, blocks] of Object.entries(this.blocks)) {
            blocks.forEach(function (block) {
                if (key === type)
                    block.show();
                else
                    block.hide();
            })
        }
    }

    setupBlocks() {
        let that = this;
        this.blocks = {
            new: [
                $('[name="prices[phys]"]').parents('fieldset'),
            ],
            old: [
                $('[name="page[body][old][service][text][0][title]"]').parents('fieldset'),
                $('[name="page[body][old][service][text][1][title]"]').parents('fieldset'),
                $('[name="page[body][old][person][text]"]').parents('fieldset'),
                $('[name="page[body][old][result][text_1]"]').parents('fieldset'),
            ],
            long: [
                $('[name="page[body][long][header][title]"]').parents('fieldset'),
                $('[name="page[body][long][block2][title]"]').parents('fieldset'),
                $('[name="page[body][long][spec][title]"]').parents('fieldset'),
                $('[name="page[body][long][block3][title]"]').parents('fieldset'),
                $('[name="page[body][long][reviews][title]"]').parents('fieldset'),
                $('[name="page[body][long][recommendations][title]"]').parents('fieldset'),
                $('[name="prices[phys]"]').parents('fieldset'),
            ],
        };

        [0, 1, 2, 3, 4].forEach(function (el) {
            that.blocks.new.push(
                $('[name="page[body][' + el + '][title]"]').parents('fieldset')
            );
        });
    }
}

