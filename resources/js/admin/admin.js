require('./bootstrap');
require('./editorjs/fields');
require('./pages/service');
require('./pages/casepage');

import PriceField from './fields/price';
import ServiceUpdaterField from './fields/service_updater_field';
import CasePage from './pages/casepage';

if (typeof window.application !== 'undefined') {
    window.application.register('fields--price', PriceField);
    window.application.register('fields--service_updater', ServiceUpdaterField);

    window.application.register('fields--casepage', CasePage);
}
