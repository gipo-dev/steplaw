$(function () {
    let forms = $('.callback-form');

    forms.each(function (i, form) {

        $(form).submit(function (e) {
            e.preventDefault();

            let url = $(this).attr('action');
            let data = $(this).serialize();
            $(form).parents('.callback-form-wrap').addClass('sent');

            $.ajax({
                url: url,
                method: 'POST',
                data: data,

                success: function (resp) {
                    // console.log(resp)
                },

                error: function (resp) {
                    alert('Упс! Кажется что-то пошло не так...');
                }
            });
        });

    });

    let callbackForm = $('.modal__callback');

    $('body').on('click', '.modal-callback-open', function (e) {
        e.preventDefault();
        callbackForm.addClass('_open');
    });

    $('.modal-callback-close').click(function (e) {
        e.preventDefault();
        callbackForm.removeClass('_open');
    });
});
