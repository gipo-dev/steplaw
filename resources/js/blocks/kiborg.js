$(function () {

    let enabled = false;
    let $block = $('.advantages');
    let kiborgImage = $block.find('.kiborg');
    let halfScreen = $(document).width() / 2 + 100;
    if ($(document).width() > 1024)
        halfScreen += 100;

    $('.advantages').on('mousemove touchstart', function (e) {
        if (halfScreen - e.pageX < 0) {
            kiborgImage.css({opacity: 1});
            if (kiborgImage.data('src')) {
                kiborgImage.attr('src', kiborgImage.data('src'));
                kiborgImage.data('src', false);
            }
        } else
            kiborgImage.css({opacity: 0});
    }).mouseleave(function () {
        kiborgImage.css({opacity: 0});
    });
});
