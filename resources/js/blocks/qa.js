$(function () {
    let qa = $('.ul-to-qa');

    if (qa.length < 1)
        return;

    qa.find('li').wrapInner('<p></p>');
    qa.find('li p').each(function (i, el) {
        let question = $(el).find('strong');
        $(el).parent('li').prepend(question);
    });

    qa.find('li strong').click(function (e) {
        $(this).parent('li').toggleClass('_open').find('p').slideToggle();
    });

    qa.each(function (i, el) {
        $($(el).find('li strong')[0]).trigger('click');
    });
});
