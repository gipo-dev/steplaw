$(function () {
    let $carousel = $('.page_case__documents__carousel');

    if ($carousel.length < 1)
        return;

    let flickity = new Flickity($carousel[0], {
        cellAlign: 'left',
        prevNextButtons: false,
        groupCells: true,
        draggable: true,
        contain: true,
        pageDots: false,
    });
    fixScrolling($carousel, flickity);

    $carousel.on('staticClick.flickity', function (event, pointer, cellElement, cellIndex) {
        console.log(123)
    });

});
