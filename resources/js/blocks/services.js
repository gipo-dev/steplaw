import jBox from "jbox";

$(function () {
    if ($('.services__carousel').length > 0) {
        $('.services__carousel').each(function (i, el) {
            initCarousel(el);
        });
    }

    function initCarousel($carousel) {
        let flickity = new Flickity($carousel, {
            cellAlign: 'left',
            contain: true,
            prevNextButtons: false,
            wrapAround: true,
            adaptiveHeight: true,
            groupCells: true,
        });
        fixScrolling($carousel, flickity);
    }

    $('.services__categories [data-id]').click(function () {
        let url = $('.services__categories').data('url');
        let category_id = $(this).data('id');

        $.ajax({
            url: url + '/' + category_id,
            success: function (resp) {
                $('.services__items')[0].outerHTML = resp;

                if ($('.services__carousel').length > 0) {
                    $('.services__carousel').each(function (i, el) {
                        initCarousel(el);
                    });
                }
            }
        });
    });

    if ($('.page_service figure.image').length > 0) {
        $('.page_service figure.image').wrapAll('<div style="overflow: hidden"></div>');
    }

    new jBox('Modal', {
        attach: '[data-modal-help]',
        getId: 'data-modal-help',
        isolateScroll: true,
        ajax: {
            getURL: 'data-url',
            reload: 'strict',
            setContent: false,
            success: function (response) {
                this.setContent(response);
            },
            error: function () {
                this.setContent('<b style="color: #d33">Произошла ошибка...</b>');
            },
        },
        onClose: function () {
            $('.jBox-content').empty();
        },
    });

    $('[data-collapse]').click(function () {
        $(this).parent().toggleClass('_open');
    });


    if ($('.partner__carousel._custom').length > 0) {
        let $carousel = $('.partner__carousel._custom');

        if ($carousel.length > 0) {
            let flickity = new Flickity($carousel[0], {
                cellAlign: 'left',
                prevNextButtons: false,
            });
            fixScrolling($carousel, flickity);
        }
    }

    if ($('.page_service_long__recommendations__list').length > 0) {
        let $carousel = $('.page_service_long__recommendations__list');
        let isMobile = $carousel.data('is-mobile') || false;

        if (isMobile && $carousel.length > 0) {
            let baseHeight = $carousel.find('.page_service_long__recommendations__item')[0].offsetHeight;
            $carousel.find('.page_service_long__recommendations__item').each(function (i, item) {
                if (i == 0)
                    return;
                let titleHeight = $(item).find('.page_service_long__recommendations__item__title')[0].offsetHeight;
                let text = $(item).find('.page_service_long__recommendations__item__text');
                let maxHeight = baseHeight - 22 - 50 - titleHeight;
                let originalText = text.text();
                $(item).css({height: baseHeight + 'px'});

                while (text.outerHeight() > maxHeight) {
                    text.text(function (index, _text) {
                        let lastIndex = _text.lastIndexOf(" ");
                        return _text.substring(0, lastIndex) + '...';
                    });
                }
                let _text = text.text();
                text.text(_text.substring(0, _text.length - 3));
                text.append(`<a class="_more">...</a>`);
                text.append(`<span class="_text-more">` + originalText.substring(_text.length - 3) + `</span>`);

                $(item).one('click', function () {
                    $(this).find('._more').hide();
                    $(this).find('._text-more').show();
                    $(item).css({height: 'auto'});
                    flickity.reloadCells();
                });
            });

            let flickity = new Flickity($carousel[0], {
                draggable: true,
                cellAlign: "left",
                prevNextButtons: false,
                adaptiveHeight: true,
            });
            fixScrolling($carousel, flickity);
        }
    }


    if ($('.page_service_long__persons__features').length > 0) {
        let $carousel = $('.page_service_long__persons__features');

        if ($carousel.length > 0) {
            fixScrolling($carousel, null);
        }
    }

    if ($('.page_service_long__related').length > 0) {
        let $resultField = $('#related-services-items');
        let isMobile = $resultField.data('is-mobile') || false;

        let resFlick = null
        if (isMobile) {
            resFlick = new Flickity($resultField[0], {
                cellAlign: 'left',
                prevNextButtons: false,
                pageDots: false,
            });
            fixScrolling($resultField, resFlick);
        }

        $('.page_service_long__related').on('click', '[data-category]', function () {
            let url = $(this).data('category');
            $(this).parent().find('._active').removeClass('_active');
            $(this).addClass('_active');

            $.ajax({
                url: url,
                success: function (resp) {
                    if (isMobile)
                        resFlick.destroy();

                    $resultField.html(resp);

                    if (isMobile) {
                        resFlick = new Flickity($resultField[0], {
                            cellAlign: 'left',
                            prevNextButtons: false,
                            pageDots: false,
                        });
                        fixScrolling($resultField, resFlick);
                    }
                }
            });
        });
    }

    $('[data-tab-btn]').click(function () {
        let tabGroup = $($(this).data('tab-group'));
        tabGroup.find('._tab').removeClass('_active');
        let selectedTab = tabGroup.find($(this).data('tab-btn'));

        if (selectedTab.hasClass('_loading'))
            return;

        if (selectedTab.data('tab-url') && !selectedTab.hasClass('_loaded')) {
            selectedTab.addClass('_loading');
            $.ajax({
                url: selectedTab.data('tab-url'),
                success: function (resp) {
                    selectedTab.html(resp);
                    selectedTab.removeClass('_loading');
                    selectedTab.addClass('_loaded');
                }
            });
        }

        selectedTab.addClass('_active');

        $('[data-tab-btn][data-tab-group="' + $(this).data('tab-group') + '"]').removeClass('_active');
        $(this).addClass('_active');
    });

    $('body').on('click', '[data-faq]', function () {
        let url = $(this).data('faq');

        $('.page_service_long__faq__tag._active').removeClass('_active');
        $(this).addClass('_active');

        $.ajax({
            url: url,
            success: function (resp) {
                $('#faq-list').html(resp);
            }
        });
    });

    $('.page_service_long__persons__list').on('click', '[data-person]', function () {
        let url = $(this).data('person');
        $.ajax({
            url: url,
            success: function (resp) {
                $('#service-person-block-current').html(resp);
            }
        });
        $('.page_service_long__persons__list [data-person]._current').removeClass('_current');
        $(this).addClass('_current');
    });

    $('#block-social-title').click(function () {
        let url = $(this).data('url');

        $.ajax({
            url: url,
            success: function (resp) {
                $('#block-social-subtitle').html(resp);
            }
        });
    });

    let currentModalIndex = 0;

    let modalReview = new jBox('Modal', {
        attach: '.cases__carousel_small .case',
        maxWidth: 500,
        getId: 'data-id',
        isolateScroll: true,
        addClass: 'modal-review',
        onOpen() {
            $(this.wrapper).addClass('_open')

            let totalSlides = $('.page_service_long__reviews__review').length
            let nextSlide = currentModalIndex >= totalSlides ? 0 : currentModalIndex
            let prevSlide = currentModalIndex > 1 ? currentModalIndex - 2 : totalSlides - 1

            $(this.wrapper).find('.jBox-image-pointer-next, .jBox-image-pointer-prev').remove()

            let nextButton = jQuery('<div class="jBox-image-pointer-next"/>')
              .on('click tap', function () {
                  openReview($('.page_service_long__reviews__review[data-index=' + nextSlide + ']'))
              }.bind(this))
            let prevButton = jQuery('<div class="jBox-image-pointer-prev"/>')
              .on('click tap', function () {
                  openReview($('.page_service_long__reviews__review[data-index=' + prevSlide + ']'))
              }.bind(this))

            $(this.wrapper)
              .append(nextButton)
              .prepend(prevButton)
        },
        onCloseComplete() {
            $(this.wrapper).removeClass('_open')
        },
    });

    $('body').on('click', '.page_service_long__reviews__review__text .page_service_long__reviews__review__text__more', function () {
        openReview($(this).parents('.page_service_long__reviews__review'))
    })

    function openReview(review) {
        currentModalIndex = review.data('index') + 1
        modalReview.setContent(review.html())
        modalReview.open()
    }

    function nextReview() {
        console.log()
    }

    // if(navigator.userAgentData.mobile) {
    //
    //     let $carouselFeatures = $('#service-long-features');
    //     $carouselFeatures.find('.page_service_long__persons__feature');//.addClass('splide__slide');
    //     // $carouselFeatures.addClass('splide');
    //
    //     let flickityFeatures = new Flickity($carouselFeatures, {
    //         cellAlign: 'left',
    //         prevNextButtons: false,
    //         wrapAround: true,
    //     });
    //     fixScrolling($carouselFeatures, flickityFeatures);
    //
    // }
});


