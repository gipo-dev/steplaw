// import Splide from "@splidejs/splide";

$(function () {
    if ($('.persons__carousel').length < 1)
        return;

    initCarousel();

    function initCarousel() {
        let $carousel = $('.persons__carousel:not(.persons__carousel_small):not(.disable_carousel)');

        if ($carousel.length > 0) {
            let flickity = new Flickity($carousel[0], {
                cellAlign: 'left',
                prevNextButtons: false,
                adaptiveHeight: true,
                wrapAround: true,
                groupCells: true,
            });
            fixScrolling($carousel, flickity);
        }

        let $carousel_small = $('.persons__carousel_small');
        if ($carousel_small.length > 0) {
            let flickity = new Flickity($carousel_small[0], {
                cellAlign: 'left',
                prevNextButtons: false,
                adaptiveHeight: true,
                wrapAround: true,
                groupCells: true,
            });
            fixScrolling($carousel_small, flickity);
        }
    }
});

$(function () {
    if ($('.page_service_long__persons__list').length < 1)
        return;

    initCarousel();

    function initCarousel() {
        let $carousel = $('.page_service_long__persons__list');

        if ($carousel.length > 0) {
            let flickity = new Flickity($carousel[0], {
                freeScroll: true,
                contain: true,
                prevNextButtons: false,
                pageDots: false,
                cellAlign: "left",
            });
            fixScrolling($carousel, flickity);
        }
    }
});


