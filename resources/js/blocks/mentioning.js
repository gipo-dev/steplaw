import Splide from "@splidejs/splide";

$(function () {

    initCarousel();

    function initCarousel() {
        if ($('.mentioning__carousel').length < 1 || $(document).width() > 1280)
            return;

        let $carousel = $('.mentioning__carousel');
        let flickity = new Flickity($carousel[0], {
            cellAlign: 'left',
            prevNextButtons: false,
            // adaptiveHeight: true,
        });
        fixScrolling($carousel, flickity);
    }

    $('.mentioning__categories [data-year]').click(function () {
        let url = $('.mentioning__categories').data('url');
        let year = $(this).data('year');
        $('.mentioning__categories [data-year]').removeClass('_active');
        $(this).addClass('_active');

        $.ajax({
            url: url + '/' + year,
            success: function (resp) {
                $('.mentioning__wrap')[0].outerHTML = resp;
                initCarousel();
            }
        });
    });
});


