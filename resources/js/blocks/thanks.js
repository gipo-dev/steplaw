import jBox from 'jbox';

$(function () {
    let $carousel = $('.thanks__carousel');

    if ($carousel.length < 1)
        return;

    initCarousel();

    function initCarousel() {
        if ($carousel.data('url')) {
            $.ajax({
                url: $carousel.data('url'),
                success: function (resp) {
                    $carousel.html(resp);

                    let flickity = new Flickity('.thanks__carousel', {
                        cellAlign: 'left',
                        prevNextButtons: false,
                        adaptiveHeight: true,
                        wrapAround: true,
                        groupCells: true,
                    });
                    fixScrolling($('.thanks__carousel'), flickity);
                }
            });
        } else {
            let flickity = new Flickity('.thanks__carousel', {
                cellAlign: 'left',
                prevNextButtons: false,
                adaptiveHeight: true,
                wrapAround: true,
                groupCells: true,
            });
            fixScrolling($('.thanks__carousel'), flickity);
        }
    }

    if ($carousel.data('url')) {
        new jBox('Modal', {
            attach: '.thanks__carousel .font-medium, .thanks__carousel h5',
            getId: 'data-id',
            isolateScroll: true,
            ajax: {
                getURL: 'data-url',
                reload: 'strict',
                setContent: false,
                success: function (response) {
                    this.setContent(response);
                },
                error: function () {
                    this.setContent('<b style="color: #d33">Произошла ошибка...</b>');
                }
            }
        });
    }
});


