import jBox from 'jbox';

$(function () {
    if ($('.reviews__carousel').length < 1)
        return;

    initCarousel();

    function initCarousel() {

        let $carousel = $('.reviews__carousel');

        if ($carousel.length > 0) {
            let flickity = new Flickity($carousel[0], {
                cellAlign: 'left',
                prevNextButtons: false,
                wrapAround: true,
                groupCells: true,
                adaptiveHeight: true,
            });
            fixScrolling($carousel, flickity);
        }
    }
});

let gallery;
new jBox('Image', {
    onOpen: function () {
        gallery = this;
        initIframe()
    },
});

$('body').on('click', '.jBox-image-pointer-next', function () {
    initIframe();
});

function initIframe() {
    if ($('.page_case__documents__carousel').length > 0) {
        let src = $(gallery.source[0]).find('iframe').attr('src')
        gallery.setContent(`<iframe src="` + src + `"></iframe>`);
    }
}


$(function () {
    let $imageReviewsCarousel = $('#image-reviews-carousel');

    if ($imageReviewsCarousel.length < 1)
        return;

    let imageReviewsCarouselFlickity = new Flickity($imageReviewsCarousel[0], {
        cellAlign: 'left',
        prevNextButtons: false,
        // wrapAround: true,
        // groupCells: true,
        adaptiveHeight: true,
        pagination: false,
    });
    fixScrolling($imageReviewsCarousel, imageReviewsCarouselFlickity);


    let $yandexReviewsCarousel = $('#image-reviews-yandex-carousel');

    if ($yandexReviewsCarousel.length < 1)
        return;

    let yandexReviewsCarouselFlickity = new Flickity($yandexReviewsCarousel[0], {
        cellAlign: 'left',
        prevNextButtons: false,
        // wrapAround: true,
        // groupCells: true,
        adaptiveHeight: true,
        pagination: false,
    });
    fixScrolling($yandexReviewsCarousel, yandexReviewsCarouselFlickity);
})
