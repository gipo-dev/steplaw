var tweetIframes = document.querySelectorAll("[id^='tweet_']");
tweetIframes.forEach(element => {
    element.onload = function () {
        this.contentWindow.postMessage({element: this.id, query: "height"},
            "https://twitframe.com");
    };
});

/* listen for the return message once the tweet has been loaded */
window.onmessage = (oe) => {
    if (oe.origin != "https://twitframe.com")
        return;
    if (oe.data.height && oe.data.element.match(/^tweet_/))
        document.getElementById(oe.data.element).style.height = (parseInt(oe.data.height) + 10 ) + "px";
}
