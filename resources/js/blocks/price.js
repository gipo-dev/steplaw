$(function () {
    let price = $('.block__price');

    if (price.length < 1)
        return;

    price.find('.btn-price').click(function (e) {
        e.preventDefault();

        let category = $(this).data('category');
        $('.price-category').hide();
        $('#price-category-' + category).show();
        $('.btn-price').removeClass('active');
        $(this).addClass('active');
    });
});
