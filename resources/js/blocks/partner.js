import jBox from 'jbox';

let partnerCarousel = $('.partner__carousel:not(._custom)');

$(function () {
    if (partnerCarousel.length > 0) {

        setTimeout(function () {
            $('.partner__carousel .partner__item iframe').each(function (i, el) {
                $(el).attr('src', $(el).data('src'));
            });
        }, 1000);

        initCarousel();

        function initCarousel() {
            let $carousel = $('.partner__carousel');

            if ($carousel.length > 0) {
                let flickity = new Flickity($carousel[0], {
                    cellAlign: 'left',
                    prevNextButtons: false,
                    wrapAround: true,
                    // adaptiveHeight: true,
                });
                fixScrolling($carousel, flickity);
            }
        }
    }

    if($('[data-partner]').length > 0) {
        new jBox('Modal', {
            attach: '[data-partner]',
            getId: 'data-id',
            isolateScroll: true,
            ajax: {
                getURL: 'data-url',
                reload: 'strict',
                setContent: false,
                success: function (response) {
                    this.setContent(response);
                },
                error: function () {
                    this.setContent('<b style="color: #d33">Произошла ошибка...</b>');
                },
            },
            onClose: function () {
                $('.jBox-content').empty();
            },
        });
    }

});
