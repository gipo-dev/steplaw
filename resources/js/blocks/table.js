$(function () {
    $tables = $('table');

    $tables.each(function (i, el) {
        $(el).wrap(`<div class="table__wrap"></div>`);
    });
});
