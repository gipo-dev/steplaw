$(function () {
    let $form = $('.block__comments__form');
    let $reply_to = $form.find('input[name="comment[reply_to]"]');

    let $success = $form.find('.sent_success');

    if (!$form)
        return;

    $form.submit(function (e) {
        e.preventDefault();
        let that = $(this);

        $success.slideDown();
        that.addClass('_loading');

        $.ajax({
            url: that.attr('action'),
            method: that.attr('method'),
            data: that.serialize(),
            success: function (resp) {
                that.trigger("reset");
                that.removeClass('_loading');
            },
        });
    });

    $form.find('input, textarea').focus(function () {
        $success.slideUp();
    });

    $('.comment .comment_reply').click(function (e) {
        e.preventDefault();
        let commentId = $(this).parents('.comment').data('id');
        $reply_to.val(commentId);
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".page__post__comments_add").offset().top
        }, 500);
    });
});
