import jBox from "jbox";

$(function () {
    if ($('.cases__carousel').length < 1)
        return;

    initCarousel();

    function initCarousel() {
        let $carousel_big = $('.cases__carousel:not(.cases__carousel_small)');
        if ($carousel_big.length > 0) {
            let flickity = new Flickity($carousel_big[0], {
                cellAlign: 'left',
                prevNextButtons: false,
                adaptiveHeight: true,
                wrapAround: true,
                groupCells: true,
            });
            fixScrolling($carousel_big, flickity);
        }

        let $carousel_small = $('.cases__carousel_small');
        if ($carousel_small.length > 0) {
            let flickity = new Flickity($carousel_small[0], {
                cellAlign: 'left',
                prevNextButtons: false,
                adaptiveHeight: true,
                wrapAround: true,
                groupCells: true,
            });
            fixScrolling($carousel_small, flickity);
        }
    }

    new jBox('Modal', {
        attach: '.cases__carousel_small .case',
        getId: 'data-id',
        isolateScroll: true,
        ajax: {
            getURL: 'data-url',
            reload: 'strict',
            setContent: false,
            success: function (response) {
                this.setContent(response);
            },
            error: function () {
                this.setContent('<b style="color: #d33">Произошла ошибка...</b>');
            }
        }
    });
});


$(function () {
    let $carousel = $('.case-categories__carousel');

    if ($carousel.length < 1)
        return;

    let flickity = new Flickity($carousel[0], {
        cellAlign: 'left',
        prevNextButtons: false,
        groupCells: true,
        draggable: true,
        contain: true,
        pageDots: false,
    });
    fixScrolling($carousel, flickity);
});
