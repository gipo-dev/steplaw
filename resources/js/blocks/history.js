$(function () {
  let $carouseles = $('.history__files-carousel');

  if ($carouseles.length < 1)
    return;

  $carouseles.each(function (index, $carousel) {

    let flickity = new Flickity($carousel, {
      cellAlign: 'left',
      prevNextButtons: false,
      groupCells: true,
      draggable: true,
      contain: true,
      pageDots: true,
    });

    setTimeout(function () {
      flickity.resize();
    }, 1500);

    fixScrolling($carousel, flickity);

  });
});
