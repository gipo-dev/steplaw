require('jquery.maskedinput/src/jquery.maskedinput')

$(function () {
    let fields = $('input[name=phone]');

    fields.mask('+7 (999) 999-99-99?99', {
        placeholder: '+7 (___) ___-__-__  ',
    });
});
