export default class PersonTool {

    constructor({data, api, config}) {
        this.api = api;
        this.config = config || {};
        this.data = data;
        this.wrapper = undefined;
        this.settingsWrapper = undefined;
        this.settings = [];
        this._getSettings();
    }

    static get toolbox() {
        return {
            title: 'Цитата',
            icon: '<svg style="height: 17px;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><g xmlns="http://www.w3.org/2000/svg"><path d="M433 512c-11.046 0-20-8.954-20-20 0-78.299-63.701-142-142-142h-30c-78.299 0-142 63.701-142 142 0 11.046-8.954 20-20 20s-20-8.954-20-20c0-100.355 81.645-182 182-182h30c100.355 0 182 81.645 182 182 0 11.046-8.954 20-20 20zM254 270c-74.439 0-135-60.561-135-135S179.561 0 254 0s135 60.561 135 135-60.561 135-135 135zm0-230c-52.383 0-95 42.617-95 95s42.617 95 95 95 95-42.617 95-95-42.617-95-95-95z"/></g></svg>'
        };
    }

    render() {
        this.wrapper = document.createElement('div');
        return this.wrapper;
    }

    save(blockContent) {
        return {
            person_id: this.data.person_id,
            text: $(blockContent).find('[editable]').text() || '',
        }
    }

    validate(savedData) {
        return true;
    }

    rendered() {
        this._getBlocks();
    }

    renderSettings() {
        let that = this;
        this.settingsWrapper = document.createElement('div');
        this.settingsWrapper.classList.add('whitespace-nowrap');

        this.settings.forEach(person => {
            let button = document.createElement('div');

            button.innerHTML = person.name;
            this.settingsWrapper.appendChild(button);

            button.addEventListener('click', () => {
                that.data.person_id = person.id;
                this._setView(person.id);
                $('.' + this.api.styles.settingsButtonActive).removeClass(this.api.styles.settingsButtonActive);
                button.classList.toggle(this.api.styles.settingsButtonActive);
            });

        });


        return this.settingsWrapper;
    }

    _getBlocks() {
        let that = this;

        axios.get('/editor/block/render', {
            params: {
                block: 'person',
                view: 'quote',
                options: {
                    person_id: that.data.person_id,
                    text: that.data.text,
                },
            },
        }).then(function (result) {
            that._renderBlock(result.data.view);
        });
    }

    _renderBlock(view) {
        let t = new DOMParser().parseFromString(view, "text/html").firstChild;
        t.querySelectorAll('[editable]').forEach(function (el) {
            el.contentEditable = true;
        });

        this.wrapper.innerHTML = '';
        this.wrapper.appendChild(t);
    }

    _getSettings() {
        let that = this;
        axios.get('/editor/block/options', {
            params: {
                block: 'person',
            },
        }).then(function (result) {
            that.settings = result.data.persons;
        });
    }

    _setView(personId) {
        console.log(personId)
        let that = this;
        axios.get('/editor/block/render', {
            params: {
                block: 'person',
                view: 'quote',
                options: this.save(this.wrapper),
            },
        }).then(function (result) {
            that._renderBlock(result.data.view);
        });
    }

}

window.editorJSTools = window.editorJSTools || [];
window.editorJSTools['PersonTool'] = PersonTool;
