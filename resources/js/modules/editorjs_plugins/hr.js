export default class HrTool {

    constructor({data, api, config}) {
        this.api = api;
        this.config = config || {};
        this.data = data;
        this.wrapper = undefined;
        this.settingsWrapper = undefined;
        this.settings = [];
        this._getSettings();
    }

    static get toolbox() {
        return {
            title: 'Разделитель',
            icon: '<svg width="19" height="4" viewBox="0 0 19 4" xmlns="http://www.w3.org/2000/svg"><path d="M1.25 0H7a1.25 1.25 0 1 1 0 2.5H1.25a1.25 1.25 0 1 1 0-2.5zM11 0h5.75a1.25 1.25 0 0 1 0 2.5H11A1.25 1.25 0 0 1 11 0z"></path></svg>',
        };
    }

    render() {
        this.wrapper = document.createElement('div');
        return this.wrapper;
    }

    save(blockContent) {
        return {
            view: this.data.view || 'hr_1',
        }
    }

    validate(savedData) {
        // if (!savedData.url.trim()) {
        //     return false;
        // }

        return true;
    }

    rendered() {
        this._getBlocks();
    }

    renderSettings() {
        let that = this;
        this.settingsWrapper = document.createElement('div');
        this.settingsWrapper.classList.add('whitespace-nowrap');

        this.settings.forEach(block => {
            let button = document.createElement('div');

            // button.classList.add(this.api.styles.settingsButton);
            button.innerHTML = block.name;
            this.settingsWrapper.appendChild(button);

            button.addEventListener('click', () => {
                this.data.view = block.view;
                this._setView(block.view);
                $('.' +this.api.styles.settingsButtonActive).removeClass(this.api.styles.settingsButtonActive);
                button.classList.toggle(this.api.styles.settingsButtonActive);
            });

        });


        return this.settingsWrapper;
    }

    _getBlocks() {
        let that = this;

        axios.get('/editor/block/render', {
            params: {
                block: 'hr',
                view: this.data.view || 'hr_1',
            },
        }).then(function (result) {
            that._renderBlock(result.data.view);
        });
    }

    _renderBlock(view) {
        var t = new DOMParser().parseFromString(view, "text/html").firstChild;
        // t.contentEditable = true;

        this.wrapper.innerHTML = '';
        this.wrapper.appendChild(t);
    }

    _getSettings() {
        let that = this;
        axios.get('/editor/block/options', {
            params: {
                block: 'hr',
            },
        }).then(function (result) {
            that.settings = result.data.settings;
        });
    }

    _setView(view) {
        let that = this;
        axios.get('/editor/block/render', {
            params: {
                block: 'hr',
                view: view,
            },
        }).then(function (result) {
            that._renderBlock(result.data.view);
        });
    }

}

window.editorJSTools = window.editorJSTools || [];
window.editorJSTools['HrTool'] = HrTool;
