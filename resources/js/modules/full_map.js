$(function () {

    let map = $('#full-map iframe');
    let originalWidth = map.width();

    if (map.length < 1 || $(document).width() < 768)
        return;

    resizeMap();

    $(window).resize(function () {
        resizeMap()
    });

    function resizeMap() {
        let rect = map[0].getBoundingClientRect();
        let margin = ($(document).width() - $('.container').width()) / 2;

        map.width(originalWidth + margin);
    }
});
