$(function () {
    if ($(document).width() > 1300)
        return;

    let blocks = $('[data-show-more]');

    blocks.each(function (i, el) {
        let block = $(el);

        block.append('<span class="btn btn_primary btn_sm">' + $(el).data('show-more') + '</span>');

        block.find('.btn').click(function () {
            block.addClass('_open');
        });
    });
});
