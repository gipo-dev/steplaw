import LazyLoad from "lazyload";

jQuery('img[lazy]').on('load', function () {
    this.style.opacity = 1;
});

let images = $("[lazy]");

new LazyLoad(images);

window.loadImages = function () {
    let images = $("[lazy]");

    new LazyLoad(images);

    jQuery('img[lazy]').on('load', function () {
        this.style.opacity = 1;
    });
}
