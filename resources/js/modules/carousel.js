global.fixScrolling = function (carousel, flickity) {

    let $carousel = $(carousel);

    $carousel.on('dragStart.flickity', function (event, pointer) {
        document.ontouchmove = function (e) {
            e.preventDefault();
        }
    });

    $carousel.on('dragEnd.flickity', function (event, pointer) {
        document.ontouchmove = function (e) {
            return true;
        }
    });

    if(flickity) {
        $carousel.next().find('.splide__arrow--prev').on('click', function () {
            flickity.previous();
        });

        $carousel.next().find('.splide__arrow--next').on('click', function () {
            flickity.next();
        });
    }
}
