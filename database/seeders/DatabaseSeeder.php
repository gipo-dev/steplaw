<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Person;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\WorkCase;
use Database\Factories\PersonFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PersonSeeder::class,
            ServiceSeeder::class,
            CaseSeeder::class,
            MentionSeeder::class,
            SettingSeeder::class,
            InnerPageSeeder::class,
        ]);
    }
}
