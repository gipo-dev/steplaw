<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    private $settings = [
        'phone_1' => '+7 (495) 786-87-56',
        'phone_2' => '+7 (925) 565-63-23',
        'email' => 'info@steplaw.ru',
        'address' => '109544, г Москва, ул Большая Андроньевская, д 13, стр 2',
        'subway' => 'Станция метро',
        'duty_lawyer' => 'дежурный адвокат 00:00-21:00',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->settings as $name => $value) {
            Setting::updateOrCreate([
                'name' => $name,
            ], [
                'value' => $value,
            ]);
        }
    }
}
