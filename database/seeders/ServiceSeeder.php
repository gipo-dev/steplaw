<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Price;
use App\Models\Service;
use App\Models\ServiceCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ServiceSeeder extends Seeder
{
    /**
     * Категории услуг при генерации
     * @var string[]
     */
    private static $service_categories = ['Физическим лицам', 'Юридическим лицам', 'Дела о клевете, споры со СМИ', 'Коллективные иски', 'Банкротство'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (static::$service_categories as $category) {
            ServiceCategory::factory()
                ->has(
                    Service::factory()
                        ->has(Page::factory(), 'page')
                        ->has(Price::factory(10), 'prices')
                        ->count(10)
                    , 'services')
                ->has(Page::factory(), 'page')
                ->create([
                    'name' => $category,
                    'slug' => Str::slug($category),
                ]);
        }
    }
}
