<?php

namespace Database\Seeders;

use App\Models\Mention;
use App\Models\MentionSource;
use Illuminate\Database\Seeder;

class MentionSeeder extends Seeder
{
    private static $sources = [
        'Новые известия' => 'https://steplaw.ru/img/site_logo_main.svg',
        'Коммерсантъ' => 'https://upload.wikimedia.org/wikipedia/commons/9/9f/Logo_Kommersant.jpg',
        'Независимая' => 'https://www.ng.ru/bitrix/templates/ng//i/logo.png',
        'bfm.ru' => 'https://s.bfm.ru/images/main_logo.png',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded
     */
    public function run()
    {
        foreach (static::$sources as $name => $img) {
            $source = MentionSource::factory()
                ->has(Mention::factory(8), 'mentions')
                ->create([
                    'name' => $name,
                ]);
            $source->addMediaFromUrl($img)->toMediaCollection('image');
        }
    }
}
