<?php

namespace Database\Seeders;

use App\Models\Page;
use App\Models\Person;
use Illuminate\Database\Seeder;

class PersonSeeder extends Seeder
{
    private static $persons = [
        'Соколов Андрей Николаевич' => [
            'full' => 'https://steplaw.ru/netcat_files/67/239/h_0f9c61554e77ecfc0fd0cc609d971e3c',
            'face' => 'https://steplaw.ru/netcat_files/67/239/Sokolov_Sait.png',
        ],
        'Фабричная Анна Игоревна' => [
            'full' => 'https://steplaw.ru/netcat_files/67/239/h_53cf01e4473a676d3b374ed752b23065',
            'face' => 'https://steplaw.ru/netcat_files/67/239/fabrichnaya_aa.png',
        ],
        'Петровская Оксана Юрьевна' => [
            'full' => 'https://steplaw.ru/netcat_files/67/239/h_962aea85e6b95b9b5697a6ca6e079936',
            'face' => 'https://steplaw.ru/netcat_files/67/239/IMG_0a9ec0a8d9e04bb9e8a746e38f2184fb_AA.png',
        ],
        'Трусов Фёдор Николаевич' => [
            'full' => 'https://steplaw.ru/netcat_files/67/239/h_305bb35e7637d16ebc80d634eac08b60',
            'face' => 'https://steplaw.ru/netcat_files/67/239/Trusov_Sait.png',
        ],
        'Иванова Галина Петровна' => [
            'full' => 'https://steplaw.ru/netcat_files/67/239/h_4c84477d4869efc6f4b56d3cb6a62ff3',
            'face' => 'https://steplaw.ru/netcat_files/67/239/ivanova_gp.png',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (static::$persons as $name => $images) {
            $person = Person::factory()
                ->has(Page::factory(), 'page')
                ->create([
                    'name' => $name,
                ]);
            $person
                ->addMediaFromUrl($images['full'])
                ->toMediaCollection('image');
            $person
                ->addMediaFromUrl($images['face'])
                ->toMediaCollection('face');
        }
    }
}
