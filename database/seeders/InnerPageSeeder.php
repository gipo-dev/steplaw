<?php

namespace Database\Seeders;

use App\Models\InnerPage;
use Illuminate\Database\Seeder;

class InnerPageSeeder extends Seeder
{
    private static $pages = [
        'index' => 'Главная',
        'services.all' => 'Услуги',
        'persons.all' => 'Адвокаты',
        'contacts' => 'Контакты',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (static::$pages as $slug => $name) {
            InnerPage::firstOrCreate([
                'slug' => 'page.' . $slug,
            ], [
                'body' => [],
                'meta_title' => $name,
                'meta_description' => $name,
            ]);
        }
    }
}
