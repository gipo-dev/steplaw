<?php

namespace Database\Seeders;

use App\Models\WorkCase;
use Illuminate\Database\Seeder;

class CaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WorkCase::factory(5)
            ->create();
    }
}
