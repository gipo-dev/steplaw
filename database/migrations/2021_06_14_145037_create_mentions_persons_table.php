<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMentionsPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mention_person', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('person_id')->unsigned();
            $table->bigInteger('mention_id')->unsigned();

            $table->index(['person_id', 'mention_id']);

            $table->foreign('person_id')
                ->references('id')
                ->on('people')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('mention_id')
                ->references('id')
                ->on('mentions')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mentions_persons');
    }
}
