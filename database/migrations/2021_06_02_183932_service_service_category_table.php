<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServiceServiceCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('service_service_category', function (Blueprint $blueprint) {
//
//            $blueprint->bigInteger('service_id')->unsigned();
//            $blueprint->bigInteger('service_category_id')->unsigned();
//
//            $blueprint->primary(['service_id', 'service_category_id']);
//
//            $blueprint->foreign('service_id')
//                ->references('id')
//                ->on('services')
//                ->cascadeOnUpdate()
//                ->cascadeOnDelete();
//
//            $blueprint->foreign('service_category_id')
//                ->references('id')
//                ->on('service_categories')
//                ->cascadeOnUpdate()
//                ->cascadeOnDelete();
//        });
        Schema::dropIfExists('service_service_category');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_service_category');
    }
}
