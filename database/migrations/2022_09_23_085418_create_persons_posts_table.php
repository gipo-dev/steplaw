<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonsPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons_posts', function (Blueprint $table) {
            $table->id();

            $table->bigInteger('person_id')->unsigned();
            $table->bigInteger('post_id')->unsigned();

            $table->timestamps();

            $table->index(['person_id', 'post_id']);

            $table->foreign('person_id')
                ->references('id')
                ->on('people')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persons_posts');
    }
}
