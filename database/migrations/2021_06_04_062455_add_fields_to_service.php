<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->after('name', function (Blueprint $table) {
                $table->text('description');
                $table->bigInteger('service_category_id')->unsigned()->nullable();

                $table->index('service_category_id');

                $table->foreign('service_category_id')
                    ->references('id')
                    ->on('service_categories')
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service', function (Blueprint $table) {
            //
        });
    }
}
