<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPersonIdToReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews', function (Blueprint $table) {
            $table->after('service_id', function (Blueprint $table) {
                $table->bigInteger('person_id')->unsigned()->nullable();

                $table->index('person_id');

                $table->foreign('person_id')
                    ->references('id')
                    ->on('people')
                    ->cascadeOnUpdate()
                    ->nullOnDelete();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews', function (Blueprint $table) {
            //
        });
    }
}
