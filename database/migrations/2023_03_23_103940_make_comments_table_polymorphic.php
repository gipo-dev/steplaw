<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeCommentsTablePolymorphic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('comments', function (Blueprint $table) {

                    $table->dropForeign('comments_post_id_index');
                    $table->dropColumn('post_id');

                    $table->bigInteger('commentable_id')->after('id');
                    $table->string('commentable_type')->after('commentable_id');

                    $table->index(['commentable_id', 'commentable_type'], 'commentable');
            });
        } catch (\Exception $exception) {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
