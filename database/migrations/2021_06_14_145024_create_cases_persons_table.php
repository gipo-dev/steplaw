<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasesPersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_work_case', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('person_id')->unsigned();
            $table->bigInteger('work_case_id')->unsigned();

            $table->index(['person_id', 'work_case_id']);

            $table->foreign('person_id')
                ->references('id')
                ->on('people')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('work_case_id')
                ->references('id')
                ->on('work_cases')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases_persons');
    }
}
