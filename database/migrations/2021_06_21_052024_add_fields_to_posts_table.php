<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->after('slug', function (Blueprint $table) {
                $table->bigInteger('author_id')->unsigned()->nullable()->index();
                $table->tinyInteger('status_id')->unsigned()->index();
                $table->json('data')->nullable();
            });

            $table->foreign('author_id')
                ->references('id')
                ->on('people')
                ->cascadeOnUpdate()
                ->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            //
        });
    }
}
