<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotoGroupServiceCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_group_service_category', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('photo_group_id')->unsigned();
            $table->bigInteger('service_category_id')->unsigned();

            $table->index(['photo_group_id', 'service_category_id'], 'pgi_sci');

            $table->foreign('photo_group_id', 'pgi')
                ->references('id')
                ->on('photo_groups')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreign('service_category_id', 'sci')
                ->references('id')
                ->on('service_categories')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_group_service_category');
    }
}
