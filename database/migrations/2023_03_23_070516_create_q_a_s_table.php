<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQASTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qas', function (Blueprint $table) {
            $table->id();
            $table->json('properties')->nullable();
            $table->timestamps();
        });

        Schema::create('qa_service', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('qa_id')->index();
            $table->bigInteger('service_id')->index();
            $table->timestamps();
        });

        Schema::create('qa_person', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('qa_id')->index();
            $table->bigInteger('person_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qas');
        Schema::dropIfExists('qa_person');
        Schema::dropIfExists('qa_service');
    }
}
