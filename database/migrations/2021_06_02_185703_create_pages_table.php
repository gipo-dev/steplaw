<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('pageable_type');
            $table->bigInteger('pageable_id')->unsigned();
            $table->string('slug')->nullable();
            $table->json('body');
            $table->string('meta_title');
            $table->tinyText('meta_description');
            $table->timestamps();

            $table->unique(['pageable_type', 'slug']);
            $table->index(['pageable_type', 'pageable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
