<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_pages', function (Blueprint $table) {
            $table->id();
            $table->string('question');
            $table->smallInteger('category_id');
            $table->bigInteger('person_id')->nullable();
            $table->json('articles')->nullable();
            $table->json('properties')->nullable();
            $table->json('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_pages');
    }
}
