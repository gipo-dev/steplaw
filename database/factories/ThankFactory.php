<?php

namespace Database\Factories;

use App\Models\Thank;
use Illuminate\Database\Eloquent\Factories\Factory;

class ThankFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Thank::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
