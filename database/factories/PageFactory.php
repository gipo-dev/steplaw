<?php

namespace Database\Factories;

use App\Models\Page;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Page::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'slug' => Str::slug($this->faker->sentence(4)),
            'body' => $this->generateBody(),
            'meta_title' => $this->faker->sentence(),
            'meta_description' => $this->faker->sentence(15),
        ];
    }

    private function generateBody()
    {
        $sentences = $this->faker->sentences($this->faker->numberBetween(10, 20));
        $body = [
            'time' => now()->timestamp,
            'blocks' => [],
            'version' => '2.8.1',
        ];
        foreach ($sentences as $sentence) {
            $body['blocks'][] = [
                'type' => 'paragraph',
                'data' => [
                    'text' => $sentence,
                ],
            ];
        }
        return $body;
    }
}
