<?php

namespace Database\Factories;

use App\Models\MentionSource;
use Illuminate\Database\Eloquent\Factories\Factory;

class MentionSourceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MentionSource::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word(),
        ];
    }
}
