<?php

namespace Database\Factories;

use App\Models\CasePage;
use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class CasePageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CasePage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(100),
            'category_id' => $this->faker->numberBetween(1, 14),
            'person_id' => 7,
            'articles' => [
                "ст. 158 УК РФ",
                "ст. 213 НК РФ"
            ],
            'properties' => [
                "date" => "24.06.2021",
                "court" => "Шарьинский районный суд (Костромская область)",
                "case_number" => "№ 2-540/2021"
            ],
        ];
    }
}
