<?php

namespace Database\Factories;

use App\Models\WorkCase;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkCaseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WorkCase::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->title(),
            'body' => $this->faker->title(),
            'result' => $this->faker->title(),
            'type' => $this->faker->randomElement([1, 2]),
        ];
    }

    private function generateBody()
    {
        $sentences = $this->faker->sentences($this->faker->numberBetween(3, 5));
        $body = [
            'time' => now()->timestamp,
            'blocks' => [],
            'version' => '2.8.1',
        ];
        foreach ($sentences as $sentence) {
            $body['blocks'][] = [
                'type' => 'paragraph',
                'data' => [
                    'text' => $sentence,
                ],
            ];
        }
        return $body;
    }
}
