User-agent: *
Allow: /
Disallow: /?
Disallow: /?p=*
Disallow: /history/
Disallow: /uslugi/fizicheskim-litsam/advokat-po-semeinym-delam-i-sporam/
Host: https://steplaw.ru/
Sitemap: https://steplaw.ru/sitemap.xml
