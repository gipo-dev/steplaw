const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/admin/admin.js', 'public/admin/js')
    .sass('resources/css/admin.scss', 'public/css', {}, [
        require("tailwindcss"),
    ])
    .sass('resources/css/app.scss', 'public/css', {}, [
        require("tailwindcss"),
    ]).version();

mix.browserSync('127.0.0.1:8000');
