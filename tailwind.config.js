module.exports = {
    purge: [
        './resources/**/*.blade.php',
        './resources/**/*.js',
        './resources/**/*.vue',
    ],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            colors: {
                primary: '#026f4f',
                grey: {
                    10: '#efefef',
                    50: '#ededed',
                    100: '#dedede',
                    200: '#a0a0a0',
                    300: '#dddbdb',
                    400: '#888888',
                    500: '#696969',
                },
            },
            spacing: {
                '400': '40px',
            },
            fontSize: {
                '2xl': '23px',
                '15': '15px',
                '17': '17px',
            },
            container: {
                center: true,
                screens: {
                    lg: "1180px",
                },
                padding: {
                    default: '8px',
                },
            },
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        require('autoprefixer'),
    ],
}
