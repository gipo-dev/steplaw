## Steplaw
**[steplaw.ru](https://steplaw.ru)**

### Стэк технологий

- **[Laravel](https://laravel.com/)** - основной фремворк
- **[Orchid](https://orchid.software/ru)** - админ панель
- **[Tailwind](https://v1.tailwindcss.com)** - css фреймворк
- **[Editorjs](https://editorjs.io)** - блочный редактор страниц
- **[Splidejs](https://splidejs.com)** - карусель
- **[jBox](https://stephanwagner.me/jBox)** - модальные окна

### По вопросам разработки

Telegram @nsokolovv
