<?php

namespace App\Facades;

use App\Models\Service;
use Illuminate\Support\Facades\Facade;

/**
 * Class LinkManager
 * @package App\Facades
 *
 * @method static string generateServiceUrl(Service $service)
 */
class LinkManager extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'link_manager';
    }
}
