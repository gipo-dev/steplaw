<?php


namespace App\Facades;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;

/**
 * Class PageHandler
 * @package App\Facades
 *
 * @method static Collection spliceCollection(Collection $collection, int $parts = 2)
 */
class PageHandler extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'page_handler';
    }
}
