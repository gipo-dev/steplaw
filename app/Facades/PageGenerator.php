<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class PageGenerator
 * @package App\Facades
 *
 * @method static string render(string[] $data)
 * @method static string renderMap(string[] $data)
 */
class PageGenerator extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'page_generator';
    }
}
