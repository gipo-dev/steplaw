<?php


namespace App\Composers;


use Illuminate\View\View;

class FooterComposer
{
    public function compose(View $view)
    {
        $view->with('menu', [
            __('loops.menu.index') => route('page.index'),
            __('loops.menu.about') => route('page.about'),
            __('loops.menu.persons') => route('page.persons.all'),
            __('loops.menu.services') => route('page.services.all'),
            __('loops.menu.payment') => route('page.payment.pay'),
            __('loops.menu.contacts') => route('page.contacts.index'),
        ]);
    }
}
