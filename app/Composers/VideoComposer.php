<?php

namespace App\Composers;

use App\Models\Video;
use Illuminate\View\View;

class VideoComposer
{

    public function compose(View $view)
    {
        $view->with('videos', Video::latest()->get());
    }

}
