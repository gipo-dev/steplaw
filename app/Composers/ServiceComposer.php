<?php

namespace App\Composers;

use App\Models\ServiceCategory;
use Illuminate\View\View;

class ServiceComposer
{

    public function compose(View $view)
    {
        $categories = ServiceCategory::where('name', 'like', 'физическим%')
            ->orWhere('name', 'like', 'юридическим%')
            ->orWhere('name', 'like', 'коллективные%')
            ->get();
        $view->with('categories', $categories);
        $view->with('services', $categories->first()->services);
    }

}
