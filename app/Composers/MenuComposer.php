<?php

namespace App\Composers;

use Illuminate\View\View;

class MenuComposer
{
    public function compose(View $view)
    {
        $view->with('menu', [
            __('loops.menu.index') => route('page.index'),
            __('loops.menu.services') => route('page.services.all'),
            __('loops.menu.persons') => route('page.persons.all'),
            __('loops.menu.journal') => route('page.posts.all'),
            __('loops.menu.mentioning') => route('page.index') . '#smi',
//            __('loops.menu.posts') => route('page.posts.all'),
            __('loops.menu.contacts') => route('page.contacts.index'),
        ]);
    }
}
