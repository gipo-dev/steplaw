<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

class CasePage extends Model
{
    use AsSource;
    use Filterable;
    use Attachable;
    use HasFactory;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'articles' => 'array',
        'properties' => 'array',
        'files' => 'array',
    ];

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function page(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Page::class, 'pageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    public function getShortNameAttribute()
    {
        dd($this);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAttachments()
    {
        return collect(\Orchid\Attachment\Models\Attachment::find($this->files ?? []))
            ->map(function ($file) {
                return [
                    'name' => $file->original_name,
                    'path' => '/storage/' . $file->path . $file->name . '.' . $file->extension,
                ];
            });
    }

    public static function getArticleName($data)
    {
        if (!isset($data['number']))
            return '';

        $text = __('loops.casepage.article.number', ['number' => $data['number'] ?? '']);

        if ($data['part'] ?? false)
            $text .= __('loops.casepage.article.part', ['number' => $data['part'] ?? '']);

        $text .= ' ' . __('pages.case_page.categories.' . str_replace('_', '', $data['category'] ?? '') . '.small');
        return $text;
    }
}
