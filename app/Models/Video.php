<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class Video
 * @package App\Models
 *
 * @property int $id
 * @property string $text
 * @property string $src
 * @property \Carbon\Carbon $createdAt
 * @property \Carbon\Carbon $updatedAt
 */
class Video extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'text',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'text',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    public function getPreviewAttribute() {
        $link = Arr::last(explode('/', $this->src));
        return "https://i3.ytimg.com/vi/$link/hqdefault.jpg";
    }
}
