<?php

namespace App\Models;

use App\Facades\PageGenerator;
use App\Services\InnerPage\ServiceRecommendationsTemplate;
use App\Services\Templatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class Page
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property array $body
 * @property string $meta_title
 * @property string $meta_description
 * @property string $pageable_type
 * @property int $pageable_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property string $inner_name
 * @property Model $pageable
 */
class Page extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Templatable;


    /**
     * @var string
     */
    protected $table = 'pages';

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'meta_title',
        'slug',
        'pageable_type',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'meta_title',
        'slug',
        'pageable_type',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'body' => 'array',
    ];

    /**
     * @var string[]
     */
    protected $attributes = [
        'body' => '{}',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function pageable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return PageGenerator::render($this->body);
    }

    /**
     * @return bool
     */
    public function isInner(): bool
    {
        return $this->pageable_type == InnerPage::class;
    }

    /**
     * @return bool
     */
    public function isDeletable(): bool
    {
        if($this->template == ServiceRecommendationsTemplate::class && $this->slug != 'page.blocks.recommendations') {
            return true;
        }

        return !in_array($this->pageable_type, [InnerPage::class, PageBlock::class]);
    }

    /**
     * @return string
     */
    public function getInnerNameAttribute(): string
    {
        if($this->template == ServiceRecommendationsTemplate::class && $this->slug != 'page.blocks.recommendations') {
            $service = Service::find($this->pageable_id);
            return "Блок для страницы '$service->name'";
        }

        return __('loops.page.type.' . $this->pageable_type) . ': ' . __('loops.page.name.' . $this->slug);
    }

    /**
     * @param $name
     * @param bool $nullable
     * @return mixed|string
     */
    public function field(string $name, bool $nullable = false)
    {
        if ($nullable)
            return $this->body[$name] ?? null;
        return $this->body[$name] ?? '';
    }
}
