<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class Mention
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $link
 * @property int $sourceId
 * @property \Carbon\Carbon $createdAt
 * @property \Carbon\Carbon $updatedAt
 *
 * @property \App\Models\MentionSource $source
 *
 * @method static self year(int $year)
 */
class Mention extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'text',
        'name',
        'year',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'year',
        'text',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $with = ['source'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function source(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(MentionSource::class, 'source_id');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param int $year
     */
    public function scopeYear(Builder $builder, int $year)
    {
        $builder->where('year', $year);
    }

    /**
     * @return string
     */
    public function getImageAttribute(): string
    {
        return $this->source->getFirstMediaUrl('image') ?? '';
    }
}
