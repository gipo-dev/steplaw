<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Metric
 * @package App\Models
 *
 * @property int $id
 * @property string $metricable_type
 * @property int $metricable_id
 * @property int $type
 * @property int $val
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Model $metricable
 */
class Metric extends Model
{
    use HasFactory;

    public const TYPE_VIEW = 1;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'val' => 0,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function metricable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }
}
