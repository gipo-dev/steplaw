<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Attachment\Attachable;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class WorkCase
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $body
 * @property string $result
 * @property int $sort
 * @property int $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @method static $this sort()
 * @method static $this index()
 * @method static $this service()
 */
class WorkCase extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
//    use Attachable;


    public const TYPE_INDEX = 1,
        TYPE_SERVICE = 2;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function scopeSort(Builder $builder)
    {
        $builder->orderBy('sort');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function scopeIndex(Builder $builder)
    {
        $builder->where('type', self::TYPE_INDEX);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function scopeService(Builder $builder)
    {
        $builder->where('type', self::TYPE_SERVICE);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $builder
     * @param null $except
     * @return void
     */
    public function scopeUnbinded(Builder $builder, $except = null)
    {
        $builder->where(function () use (&$builder, &$except) {
            $builder->whereDoesntHave('page');
            if ($except)
                $builder->orWhere('id', $except);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function page()
    {
        return $this->hasOne(CasePage::class, 'work_case_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Service::class);
    }
}
