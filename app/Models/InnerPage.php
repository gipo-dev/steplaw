<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class InnerPage extends Page
{
    /**
     * @var string
     */
    protected $table = 'pages';

    /**
     * @var array
     */
    protected $attributes = [
        'pageable_type' => self::class,
        'pageable_id' => 1,
    ];

    protected static function booted()
    {
        static::addGlobalScope('type', function (Builder $builder) {
            $builder->where('pageable_type', self::class);
        });
    }

    /**
     * @param string $slug
     * @return static|null
     */
    public static function search(string $slug): ?InnerPage
    {
        return self::where('slug', $slug)->first();
    }
}
