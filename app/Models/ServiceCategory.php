<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class ServiceCategory
 * @package App\Models
 *
 *
 * @property \App\Models\Page $page
 */
class ServiceCategory extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'slug',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'slug',
        'parent_id',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function services(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Service::class)->whereNull('parent_id')
            ->where(function ($q) {
                $q->whereNull('status_id')->orWhere('status_id', 1);
            })->latest('order');
    }

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        if (request()->routeIs('platform.*'))
            return 'id';
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function page(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Page::class, 'pageable');
    }
}
