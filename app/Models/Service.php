<?php

namespace App\Models;

use App\Facades\LinkManager;
use App\Services\InnerPage\ServiceRecommendationsTemplate;
use App\Services\Pageable;
use App\Services\PageGenerator\ServicePageGenerator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class Service
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $tags
 * @property int $victories
 * @property int $service_category_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Page $page
 * @property \App\Models\ServiceCategory $category
 * @property \App\Models\Price[]|\Illuminate\Support\Collection $prices
 * @property \App\Models\Mention[]|\Illuminate\Support\Collection $mentions
 */
class Service extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use Pageable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'user_id',
        'name',
        'slug',
        'description',
        'service_category_id',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'user_id',
        'name',
        'slug',
        'description',
        'serviceCategoryId',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $with = ['page', 'category', 'parent'];

    protected $blocks;

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        if (request()->routeIs('platform.*'))
            return 'id';
        return 'slug';
    }

    /**
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope('active', function (Builder $builder) {
            if (request()->routeIs('platform.*') || (Auth::user() && Auth::user()->hasAccess('platform.index')))
                return;

            $builder->where(function ($q) {
                $q->whereNull('status_id')->orWhere('status_id', 1);
            });

//            if(!request()->routeIs('platform.*')) {
//                $builder->latest('order');
//            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function page(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Page::class, 'pageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ServiceCategory::class, 'service_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Price::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviews(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Review::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photoReviews(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PhotoReview::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cases(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(WorkCase::class)->service();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function casePages()
    {
        return $this->belongsToMany(CasePage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function thanks(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Thank::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function mentions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Mention::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Service::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function faqs(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Faq::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function qas(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(QA::class, 'qa_service', null, 'qa_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Service::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function blocks()
    {
        if(!isset($this->blocks)) {
            $this->blocks = PageBlock::where('pageable_type', PageBlock::class)->get();
        }

        /** @var Collection $blocks */
        $blocks = $this->blocks->keyBy(function($block) {
            return str_replace('page.blocks.', '', $block->slug);
        });

        if($block = $blocks->where('template', ServiceRecommendationsTemplate::class)->whereNull('slug')->first()) {
            $blocks['recommendations'] = $block;
        }

        return $blocks;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeParentsOnly(Builder $query): Builder
    {
        return $query->whereNull('parent_id');
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return LinkManager::generateServiceUrl($this);
    }

    /**
     * @return array
     */
    protected function defaultPageTemplate(): array
    {
        return [
            'Название услуги',
            'Какие уголовные дела ведет бюро?',
            'Что мы будем делать?',
            'Типичные ошибки клиентов',
            'Вопросы адвокату по уголовным делам',
        ];
    }

    /**
     * @return string
     */
    public function getQA(): string
    {
        preg_match_all('/<p>Q:\s*(.*)<\/p>/', $this->page['body']['qa']['text'] ?? '', $questions);
        preg_match_all('/<p>A:\s*(.*)<\/p>/', $this->page['body']['qa']['text'] ?? '', $answers);
        $text = trim(str_replace(array_merge($questions[0], $answers[0]), '', $this->page['body']['qa']['text'] ?? ''));
        $text .= "<div class='ul-to-qa'><ul>";
        foreach ($questions[1] as $i => $question)
            $text .= PHP_EOL . "<li><strong>$question</strong> " . ($answers[1][$i] ?? '') . " </li>";
        $text .= "</ul></div>";
        return $text;
    }

    public function getPricesTable($category_id)
    {
        $table = "<table>";
        foreach ($this->prices()->where('category_id', $category_id)->get() as $item) {
            $table .= "<tr><td>{$item->name}</td><td>{$item->price}</td></tr>";
        }
        $table .= "</table>";
        return $table;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return 'pages.service.' . ($this->page->body['template']['type'] ?? 'new');
    }

    /**
     * @param $path
     * @param false $nullable
     * @return string|null
     */
    public function getValue($path, $nullable = false): ?string
    {
        $dot = Arr::dot($this->page->body ?? []);
        return ServicePageGenerator::generate($dot[$path] ?? ($nullable ? null : ''));
    }
}
