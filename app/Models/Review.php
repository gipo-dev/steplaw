<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Class Review
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property int $service_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Review extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];
}
