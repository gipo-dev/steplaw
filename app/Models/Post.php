<?php

namespace App\Models;

use App\Services\HasMetricsInterface;
use App\Services\Metricable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Class Post
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $author_id
 * @property int $status_id
 * @property array $data
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Page $page
 * @property \App\Models\PostCategory[]|\Illuminate\Support\Collection $categories
 * @property \App\Models\Comment[]|\Illuminate\Support\Collection $comments
 * @property \App\Models\Tag[]|\Illuminate\Support\Collection $tags
 * @property \App\Models\PostCategory $category
 * @property \App\Models\Person $author
 */
class Post extends Model implements HasMedia, HasMetricsInterface
{
    use HasFactory;
    use InteractsWithMedia;
    use AsSource;
    use Filterable;
    use Metricable;

    const STATUS_PUBLISHED = 1,
        STATUS_DRAFT = 2,
        STATUS_DISABLED = 3;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'slug',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'slug',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'data' => 'array',
    ];

    protected $with = [
        'author',
        'tags',
        'comments',
    ];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        if (request()->routeIs('platform.*'))
            return 'id';
        return 'slug';
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('active', function (Builder $builder) {
            if (!request()->routeIs('platform.*'))
                $builder->where('status_id', self::STATUS_PUBLISHED);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function page(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Page::class, 'pageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Person::class, 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(PostCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * @return \App\Models\PostCategory|null
     */
    public function getCategoryAttribute(): ?PostCategory
    {
        return $this->categories()->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function persons(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(Person::class, 'persons_posts');
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return route('page.posts.post', [
            'category' => $this->category->slug ?? '',
            'post' => $this->slug ?? '',
        ]);
    }

    /**
     * @return string
     */
    public function image(): string
    {
        $image = $this->getFirstMediaUrl('image');
        if ($image)
            return $image;
        if ($this->category)
            return $this->category->getFirstMediaUrl('image');
        return '';
    }
}
