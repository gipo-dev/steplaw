<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageBlock extends Model
{
    protected $table = 'pages';

    protected $attributes = [
        'pageable_type' => self::class,
    ];

    protected $casts = [
        'body' => 'array',
    ];


}
