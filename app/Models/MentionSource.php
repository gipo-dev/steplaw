<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * Class MentionSource
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $createdAt
 * @property \Carbon\Carbon $updatedAt
 */
class MentionSource extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mentions(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Mention::class, 'source_id');
    }
}
