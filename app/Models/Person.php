<?php

namespace App\Models;

use App\Services\Templatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * Class Person
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $position
 * @property string $about
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Page $page
 * @property \App\Models\Mention[]|\Ramsey\Collection\Collection $mentions
 * @property \App\Models\WorkCase[]|\Ramsey\Collection\Collection $cases
 */
class Person extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use AsSource;
    use Filterable;
    use Templatable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'slug',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'slug',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'data' => 'array',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        if (request()->routeIs('platform.*'))
            return 'id';
        return 'slug';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function page(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Page::class, 'pageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cases(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(WorkCase::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function mentions(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Mention::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts(): \Illuminate\Database\Eloquent\Relations\BelongsToMany {
        return $this->belongsToMany(Post::class, 'persons_posts')
            ->where('show_on_person', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function qas(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(QA::class, 'qa_person', null, 'qa_id');
    }
}
