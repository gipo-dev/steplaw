<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class Callback
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property int $status_id
 * @property int $service_id
 * @property string $admin_comment
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property string $status_name
 * @property \App\Models\Service $service
 */
class Callback extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;
    use SoftDeletes;

    const STATUS_NEW = 1,
        STATUS_PROCESS = 2,
        STATUS_PROCESSED = 3,
        STATUS_REJECT = 4,
        STATUS_SPAM = 5;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'phone',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'phone',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'name' => '',
        'status_id' => self::STATUS_NEW,
    ];

    /**
     * @var string[]
     */
    protected $with = ['service'];

    /**
     * @return string
     */
    public function getStatusNameAttribute(): string
    {
        return __('loops.callback.status.' . $this->status_id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Service::class);
    }
}
