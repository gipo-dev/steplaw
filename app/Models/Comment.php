<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;

/**
 * Class Comment
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property int $post_id
 * @property int $reply_to
 * @property int $likes
 * @property int $dislikes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Comment $parent
 * @property \App\Models\Post $post
 * @property \App\Models\Person $person
 * @property \App\Models\Comment[]|\Illuminate\Support\Collection $replies
 */
class Comment extends Model
{
    use HasFactory;
    use AsSource;
    use Filterable;

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'name',
        'slug',
        'post_id',
        'reply_to',
        'created_at',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'name',
        'slug',
        'post_id',
        'reply_to',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var int[]
     */
    protected $attributes = [
        'likes' => 0,
        'dislikes' => 0,
    ];

    protected $with = ['parent', 'person'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Comment::class, 'reply_to');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @return bool
     */
    public function isChild(): bool
    {
        return !is_null($this->parent);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comment::class, 'reply_to')->orderByDesc('person_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function person(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Person::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable() {
        return $this->morphTo();
    }
}
