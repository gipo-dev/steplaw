<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Orchid\Attachment\Attachable;
use Orchid\Attachment\Models\Attachment;
use Orchid\Filters\Filterable;
use Orchid\Screen\AsSource;
use Spatie\MediaLibrary\InteractsWithMedia;

class PhotoGroup extends Model
{
    use AsSource;
    use Filterable;
    use Attachable;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected $casts = [
        'images' => 'array',
    ];

    /**
     * @var string[]
     */
    protected $allowedFilters = [
        'id',
        'title',
        'description',
    ];

    /**
     * @var string[]
     */
    protected $allowedSorts = [
        'id',
        'title',
        'description',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function serviceCategories()
    {
        return $this->belongsToMany(ServiceCategory::class);
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return Attachment::find($this->images ?? [])->pluck('name', 'url');
    }
}
