<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ServiceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $currentService = Arr::last($request->route()->parameters());
        if ($request->url() . '/' != $currentService->getLink())
            return redirect($currentService->getLink());

        return $next($request);
    }
}
