<?php

namespace App\Http\Controllers;

use App\Events\CallbackLeft;
use App\Http\Requests\CallbackRequest;
use App\Models\Callback;

class CallbackController extends Controller
{
    /**
     * @param \App\Http\Requests\CallbackRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CallbackRequest $request)
    {
        $callback = Callback::create($request->validated());
        CallbackLeft::dispatch($callback);
        return response()->json([
            'status' => 'ok',
            'data' => $callback->id,
        ]);
    }
}
