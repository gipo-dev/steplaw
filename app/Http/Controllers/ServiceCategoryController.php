<?php

namespace App\Http\Controllers;

use App\Models\ServiceCategory;
use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;

class ServiceCategoryController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceCategoryController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    public function index(Request $request, ServiceCategory $category)
    {
        /** @var \App\Models\Service $service */
        $service = $category->services()->first();
        if (!$service)
            abort(404);
        return redirect($service->getLink());
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ServiceCategory|null $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list(Request $request, ServiceCategory $category = null)
    {
        if (!$category)
            abort(404);
        return view('partials.blocks.services.services_wrap', [
            'services' => $category->services,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ServiceCategory $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function related(Request $request, ServiceCategory $category)
    {
        return view('partials.blocks.services.services_related_items', [
            'services' => $category->services,
        ]);
    }
}
