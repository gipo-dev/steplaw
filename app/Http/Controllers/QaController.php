<?php

namespace App\Http\Controllers;

use App\Models\Person;
use App\Models\QA;
use App\Models\Service;
use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class QaController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function all(Request $request)
    {
        $this->metaGenerator->setMetas();
        \Meta::set('canonical', $request->url() . '/');

        $items = QA::with(['persons'])->where('id', '>', 0);

        if (isset($request->service)) {
            $items = $items->whereHas('services', function (Builder $query) use ($request) {
                $query->where('services.id', $request->service);
            });
        }

        if (isset($request->lawyer)) {
            $items = $items->whereHas('persons', function (Builder $query) use ($request) {
                $query->where('qa_person.id', $request->lawyer);
            });
        }

        return view('pages.questions', [
            'items' => $items->paginate(15),
            'services' => Service::whereHas('qas')->get(),
//            'persons' => Person::whereHas('qas')->get(),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function ajax(Request $request)
    {
        return view('pages.partials.faqs', [
            'items' => QA::with(['persons'])
                ->whereHas('services', function (Builder $query) use ($request) {
                    $query->where('services.id', $request->service);
                })->get(),
        ]);
    }


    public function index(Request $request, QA $qa)
    {
        \Meta::title($qa->page->meta_title ?? '');
        \Meta::set('description', $qa->page->meta_description ?? '');
        \Meta::set('canonical', $request->url() . '/');

        return view('pages.question', [
            'breadcrumbs' => [
                [
                    'title' => __('breadcrumbs.index'),
                    'url' => route('page.index'),
                ],
                [
                    'title' => __('breadcrumbs.qa.all'),
                    'url' => route('page.qa.all'),
                ],
                [
                    'title' => __('breadcrumbs.qa.item'),
                ],
            ],
            'qa' => $qa,
            'comments' => $qa->comments()->whereNull('reply_to')->orderByDesc('person_id')->get(),
        ]);
    }
}
