<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Services\QaService;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function list(Request $request, Service $service, $tag = -1)
    {
        return view('partials.blocks.faq.faq_list', [
            'service' => $service,
            'qa' => [
                'items' => (new QaService())->getItems($service, $tag, $request->page ?? 1),
                'total' => (new QaService())->getTotal($service),
                'hasMore' => (new QaService())->hasMore($service, $tag, ($request->page ?? 1) + 1),
            ],
        ]);
    }
}
