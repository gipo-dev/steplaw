<?php

namespace App\Http\Controllers;

use App\Models\Person;
use App\Models\Post;
use App\Models\WorkCase;
use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PersonController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Person $person
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, Person $person)
    {
        \Meta::title($person->page->meta_title ?? '');
        \Meta::set('description', $person->page->meta_description ?? '');
        if (Str::endsWith($request->url(), '.html'))
            \Meta::set('canonical', $request->url());
        else
            \Meta::set('canonical', $request->url() . '/');

        return view('pages.person', [
            'person' => $person,
            'posts' => $person->posts()->paginate(6),
            'breadcrumbs' => [
                [
                    'title' => __('breadcrumbs.index'),
                    'url' => route('page.index'),
                ],
                [
                    'title' => __('breadcrumbs.persons'),
                    'url' => route('page.persons.all'),
                ],
            ],
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function all(Request $request)
    {
        $this->metaGenerator->setMetas();
        \Meta::set('canonical', $request->url() . '/');

        return view('pages.persons', [
            'page' => $this->metaGenerator->getPage(),
            'persons' => Person::get()->sortBy('order'),
            'breadcrumbs' => [
                [
                    'title' => __('breadcrumbs.index'),
                    'url' => route('page.index'),
                ],
                [
                    'title' => __('breadcrumbs.persons'),
                ],
            ],
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Person $person
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function big(Request $request, Person $person) {
        return view('partials.blocks.persons.person_show_big', [
            'person' => $person,
        ]);
    }
}
