<?php

namespace App\Http\Controllers;

use App\Models\Mention;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MentioningController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param int|null $year
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list(Request $request, int $year = null): View
    {
        return view('partials.blocks.mentioning.mentioning_wrap', [
            'mentions' => Mention::where('year', $year ?? now()->year)->get(),
        ]);
    }
}
