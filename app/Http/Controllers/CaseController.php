<?php

namespace App\Http\Controllers;

use App\Models\CasePage;
use App\Models\WorkCase;
use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;

class CaseController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function all(Request $request)
    {
        $this->metaGenerator->setMetas();
        \Meta::set('canonical', $request->url() . '/');

        $cases = CasePage::where('id', '>', 0);

        if(isset($request->category)) {
            $cases = $cases->where('category_id', $request->category);
        }

        if(isset($request->art)) {
            $cases = $cases->whereJsonContains('articles', $request->art);
        }

        return view('pages.cases', [
            'cases' => $cases->paginate(15),
            'categories' => CasePage::get(['category_id'])->unique('category_id')->pluck('category_id'),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\CasePage $case
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, CasePage $case)
    {
        \Meta::title($case->page->meta_title ?? '');
        \Meta::set('description', $case->page->meta_description ?? '');
        \Meta::set('canonical', $request->url() . '/');

        if ($case->category_id) {
            $breadcrumbs = [
                [
                    'title' => __('breadcrumbs.index'),
                    'url' => route('page.index'),
                ],
                [
                    'title' => __('breadcrumbs.cases'),
                    'url' => route('page.cases.all'),
                ],
                [
                    'title' => __('pages.case_page.categories.' . $case->category_id . '.full'),
                ],
            ];
        } else
            $breadcrumbs = [];

        return view('pages.case', [
            'breadcrumbs' => $breadcrumbs,
            'case' => $case,
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modal(Request $request)
    {
        return view('modals.case', [
            'case' => WorkCase::find($request->id),
        ]);
    }
}
