<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function get(Request $request, Post $post)
    {

    }

    /**
     * @param \App\Http\Requests\CommentRequest $request
     */
    public function store(CommentRequest $request)
    {
        Comment::create($request->comment);
    }
}
