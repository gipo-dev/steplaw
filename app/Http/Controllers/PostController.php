<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostCategory;
use App\Services\MeticService;
use App\Services\MetricService;
use App\Services\PageGenerator\PageMetaGenerator;
use App\Services\PostRelationService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * @var \App\Services\MetricService
     */
    private MetricService $metricService;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     * @param \App\Services\MetricService $metricService
     */
    public function __construct(PageMetaGenerator $metaGenerator, MetricService $metricService)
    {
        $this->metaGenerator = $metaGenerator;
        $this->metricService = $metricService;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PostCategory $category
     * @param \App\Models\Post $post
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, PostCategory $category, Post $post)
    {
        \Meta::title($post->page->meta_title ?? '');
        \Meta::set('description', $post->page->meta_description ?? '');
        $this->metricService->addView($post);
        return view('pages.post', [
            'category' => $category,
            'post' => $post,
            'author' => $post->author,
            'tags' => $post->tags,
            'comments' => $post->comments()->whereNull('reply_to')->orderByDesc('person_id')->get(),
            'related_posts' => PostRelationService::getRelatedPosts($post),
            'breadcrumbs' => $this->getBreadcrumbs($post, $category),
        ]);
    }

    /**
     * @param $post
     * @param $category
     * @return array[]
     */
    private function getBreadcrumbs($post, $category)
    {
        $breadcrumbs = [
            [
                'title' => __('breadcrumbs.index'),
                'url' => route('page.index'),
            ],
            [
                'title' => __('breadcrumbs.journal'),
                'url' => route('page.posts.all'),
            ],
        ];
        if ($category) {
            $breadcrumbs[] = [
                'title' => $category->name,
                'url' => route('page.posts.category', $post->category),
            ];
        }
        return $breadcrumbs;
    }
}
