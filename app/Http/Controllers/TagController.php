<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index(Request $request, Tag $tag)
    {
        \Meta::title(__('pages.posts.tags.title', ['tag' => $tag->name ?? '']));
        \Meta::set('canonical', $request->url() . '/');
        return view('pages.posts', [
            'category' => $tag,
            'posts' => $tag->posts()->paginate(20),
        ]);
    }
}
