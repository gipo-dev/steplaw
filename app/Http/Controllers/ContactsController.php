<?php

namespace App\Http\Controllers;

use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $this->metaGenerator->setMetas();
        \Meta::set('canonical', $request->url() . '/');

        return view('pages.contacts', [
            'page' => $this->metaGenerator->getPage(),
        ]);
    }
}
