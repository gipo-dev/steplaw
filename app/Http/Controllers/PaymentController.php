<?php

namespace App\Http\Controllers;

use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function pay()
    {
        $this->metaGenerator->setMetas();
        return view('pages.pay');
    }
}
