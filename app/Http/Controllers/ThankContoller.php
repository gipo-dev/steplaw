<?php

namespace App\Http\Controllers;

use App\Models\Thank;
use Illuminate\Http\Request;

class ThankContoller extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function modal(Request $request)
    {
        return view('modals.thank', [
            'thank' => Thank::find($request->id),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function carousel(Request $request)
    {
        return view('partials.blocks.carousel.thanks_inner', [
            'thanks' => Thank::latest()->limit(10)->get(),
        ])->render();
    }
}
