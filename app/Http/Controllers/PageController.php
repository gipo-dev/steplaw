<?php

namespace App\Http\Controllers;

use App\Models\InnerPage;
use App\Models\Page;
use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\InnerPage $page
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function default(Request $request, Page $page)
    {
        if (!$page->exists)
            $page = InnerPage::search($request->route()->getName());

        if (!$page)
            abort(404);

        \Meta::title($page->meta_title ?? '');
        \Meta::set('description', $page->meta_description ?? '');
        \Meta::set('canonical', $request->url() . '/');

//        dd($page->body);
        return view($page->getPageTemplate()->getView($page), [
            'page' => $page,
        ]);
    }
}
