<?php

namespace App\Http\Controllers;

use App\Models\InnerPage;
use App\Models\Mention;
use App\Models\Person;
use App\Models\ServiceCategory;
use App\Models\Thank;
use App\Models\Video;
use App\Models\WorkCase;
use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * IndexController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $this->metaGenerator->setMetas();
        \Meta::set('short_logo', true);
        \Meta::set('canonical', $request->url());
        return view('pages.index', [
            'page' => $this->metaGenerator->getPage(),
            'services' => ServiceCategory::first()->services()->limit(6)->get(),
            'persons' => Person::get()->sortBy('order'),
            'cases' => WorkCase::index()->sort()->latest()->limit(10)->get(),
            'thanks' => Thank::latest()->limit(10)->get(),
            'mentions' => Mention::year(Mention::max('year'))->get(),
            'videos' => Video::latest()->get(),
        ]);
    }
}
