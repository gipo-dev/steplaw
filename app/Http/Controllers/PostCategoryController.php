<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostCategory;
use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Http\Request;

class PostCategoryController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * PostCategoryController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\PostCategory $category
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request, PostCategory $category)
    {
        \Meta::title($category->page->meta_title ?? '');
        \Meta::set('description', $category->page->meta_description ?? '');
        \Meta::set('canonical', $request->url() . '/');
        return view('pages.posts', [
            'category' => $category,
            'posts' => $category->posts()->where('show_on_person', 0)
                ->latest()->paginate(20),
            'breadcrumbs' => [
                [
                    'title' => __('breadcrumbs.index'),
                    'url' => route('page.index'),
                ],
                [
                    'title' => __('breadcrumbs.journal'),
                    'url' => route('page.posts.all'),
                ],
                [
                    'title' => '',
                ],
            ],
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function all(Request $request)
    {
        $this->metaGenerator->setMetas();
        \Meta::set('canonical', $request->url() . '/');
        return view('pages.posts', [
            'posts' => Post::where('show_on_person', 0)->latest()->paginate(20),
            'breadcrumbs' => [
                [
                    'title' => __('breadcrumbs.index'),
                    'url' => route('page.index'),
                ],
                [
                    'title' => __('breadcrumbs.journal'),
                ],
            ],
        ]);
    }
}
