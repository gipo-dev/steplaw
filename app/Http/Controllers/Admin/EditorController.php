<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EditorController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function options(Request $request): \Illuminate\Http\JsonResponse
    {
        $block = $this->getBlock($request->block);

        return response()->json($block->options());
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function render(Request $request): \Illuminate\Http\JsonResponse
    {
        $block = $this->getBlock($request->block);

        return response()->json([
            'view' => $block->render($request->view, json_decode($request->options, true) ?? []),
        ]);
    }

    /**
     * @param string $block
     * @return \App\Services\EditorJs\BlockInterface
     * @throws \Exception
     */
    private function getBlock(string $block): \App\Services\EditorJs\BlockInterface
    {
        $block = "\\App\\Services\\EditorJs\\" . Str::ucfirst($block) . "Block";
        if (!class_exists($block))
            throw new \Exception("\"$block\" class does not exists");
        return new $block;
    }
}
