<?php

namespace App\Http\Controllers;

use App\Models\Video;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    public function modal(Request $request)
    {
        return view('modals.partner', [
            'video' => Video::find($request->id),
        ]);
    }
}
