<?php

namespace App\Http\Controllers;

use App\Models\Review;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\Thank;
use App\Models\WorkCase;
use App\Services\PageGenerator\PageMetaGenerator;
use App\Services\PhotoService;
use App\Services\QaService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ServiceController extends Controller
{
    /**
     * @var \App\Services\PageGenerator\PageMetaGenerator
     */
    private PageMetaGenerator $metaGenerator;

    /**
     * ServiceController constructor.
     * @param \App\Services\PageGenerator\PageMetaGenerator $metaGenerator
     */
    public function __construct(PageMetaGenerator $metaGenerator)
    {
        $this->metaGenerator = $metaGenerator;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ServiceCategory $category
     * @param \App\Models\Service $service
     * @param \App\Models\Service|null $subcategory1
     * @param \App\Models\Service|null $subcategory2
     * @param \App\Models\Service|null $subcategory3
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(
        Request  $request, ServiceCategory $category,
        Service  $service, ?Service $subcategory1 = null,
        ?Service $subcategory2 = null, ?Service $subcategory3 = null
    )
    {
        if ($subcategory1) {
            $service = Arr::last($request->route()->parameters());
        }

        \Meta::title($service->page->meta_title ?? '');
        \Meta::set('description', $service->page->meta_description ?? '');
        \Meta::set('canonical', $request->url() . '/');

        if ($service->reviews->count() > 0)
            $reviews = $service->reviews()->latest()->limit(10)->get();
        else
            $reviews = Review::latest()->limit(10)->get();

        return view($service->getTemplate(), [
            'breadcrumbs' => $this->getBreadcrumbs($service, $category),
            'category' => $category,
            'childs' => $service->childs,
            'service' => $service,
            'thanks' => $service->thanks,
            'reviews' => $reviews,
            'mentions' => $service->mentions,
            'cases' => $service->cases,
            'price' => $service->prices,
            'photoService' => app()->make(PhotoService::class),
            'qa' => [
                'tags' => (new QaService())->getTags($service),
                'items' => (new QaService())->getItems($service),
                'total' => (new QaService())->getTotal($service),
                'hasMore' => (new QaService())->hasMore($service),
            ],
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function all(Request $request)
    {
        $this->metaGenerator->setMetas();
        \Meta::set('canonical', $request->url() . '/');
        return view('pages.services', [
            'categories' => ServiceCategory::get(),
            'breadcrumbs' => [
                [
                    'title' => __('breadcrumbs.index'),
                    'url' => route('page.index'),
                ],
                [
                    'title' => __('breadcrumbs.services'),
                ],
            ],
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    public function help(Request $request)
    {
        return 'Здесь будет текст';
    }

    /**
     * @param $service
     * @param $category
     * @return array[]
     */
    private function getBreadcrumbs($service, $category)
    {
        $breadcrumbs = [
            [
                'title' => __('breadcrumbs.index'),
                'url' => route('page.index'),
            ],
            [
                'title' => __('breadcrumbs.services'),
                'url' => route('page.services.all'),
            ],

        ];
        if ($service->parent) {
            $breadcrumbs[] =
                [
                    'title' => $service->parent->name ?? '',
                    'url' => $service->parent->getLink(),
                ];
        }
        if ($category) {
            $breadcrumbs[] =
                [
                    'title' => $category->name ?? '',
                ];
        }
        return $breadcrumbs;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    public function social(Request $request)
    {
        return 'Адвокатское бюро города Москвы «Соколов, Трусов и Партнёры» является официальным многолетним партнёром медиапортала «Момент Истины». Вместе с Редакцией Издательского дома «Момент Истины» нам доверяют самые сложные дела и расследования, конфликты со СМИ, судебные дела по защите чести и достоинства в сети Интернет.';
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Service $service
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public function tab(Request $request, Service $service)
    {
        $tab = $request->get('tab');
        if (Str::startsWith($tab, 'default.')) {
            $content = __('pages.service.long.content.' . $tab);
        } else {
            $content = Arr::dot($service->page->body)[$tab] ?? '';
        }
        return $content;
    }
}
