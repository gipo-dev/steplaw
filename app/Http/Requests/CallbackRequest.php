<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (is_null($this->request->get('name')))
            $this->request->set('name', '');
        if ($this->server('HTTP_REFERER'))
            $this->request->set('url', $this->server('HTTP_REFERER'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'phone' => 'required|max:255',
            'message' => '',
            'service_id' => 'integer',
            'url' => '',
        ];
    }
}
