<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ServiceCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category.name' => 'required|max:255',
            'category.slug' => [
                'required',
                'max:255',
                Rule::unique('service_categories', 'slug')
                    ->ignore($this->request->get('category')['id'] ?? null),
            ],

            'page.meta_title' => 'required|max:255',
            'page.meta_description' => 'required|max:255',
        ];
    }
}
