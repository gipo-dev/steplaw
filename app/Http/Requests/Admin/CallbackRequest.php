<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CallbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'callback.name' => 'required|max:255',
            'callback.phone' => 'required|max:255',
            'callback.service_id' => 'integer',
            'callback.status_id' => 'required|integer',
        ];
    }
}
