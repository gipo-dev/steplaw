<?php

namespace App\Http\Requests\Admin;

use App\Models\PostCategory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class PostCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $category = $this->request->get('category');

        if (!$category['slug']) {
            $category['slug'] = Str::slug($category['name']);
            if (PostCategory::where('slug', $category['slug'])->where('id', '!=', $category['id'] ?? -1)->count() > 0)
                $category['slug'] .= '_' . Str::random(4);
        }

        $this->request->set('category', $category);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category.name' => 'required|max:255',
            'category.slug' => [
                'required',
                'max:255',
                Rule::unique('post_categories', 'slug')
                    ->ignore($this->request->get('category')['id'] ?? null),
            ],
        ];
    }
}
