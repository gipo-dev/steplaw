<?php

namespace App\Http\Requests\Admin;

use App\Models\Service;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $service = $this->request->get('service');
        $page = $this->request->get('page');
        if (!$service['description']) {
            $description = trim(strip_tags($this->request->get('page')['body'][0]['text'] ?? ''));
            if (mb_strlen($description) > 100)
                $description = mb_substr($description, 0, 100) . '...';
            $service['description'] = $description ?? '';
        }
        if (!$service['slug']) {
            $service['slug'] = Str::slug($service['name']);
            if (Service::where('slug', $service['slug'])->where('id', '!=', $service['id'] ?? -1)->count() > 0)
                $service['slug'] .= '_' . Str::random(4);
        }
        if (trim($page['body'][0]['title'] ?? '') == '')
            $page['body'][0]['title'] = $service['name'];
        if (!isset($service['parent_id']))
            $service['parent_id'] = null;

        $this->request->set('service', $service);
        $this->request->set('page', $page);
        $this->preparePrices();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service.name' => 'required|max:255',
            'service.slug' => [
                'required',
                'max:255',
                Rule::unique('services', 'slug')
                    ->ignore($this->request->get('service')['id'] ?? null),
            ],
            'service.service_category_id' => 'integer|required',
            'service.description' => 'required|max:255',
            'service.tags' => 'required|max:255',

            'page.body' => 'required',
            'page.meta_title' => 'required|max:255',
            'page.meta_description' => 'required|max:400',
        ];
    }

    private function preparePrices()
    {
        $prices = $this->request->get('prices');
        $items = [];
        foreach ([0 => 'jur', 1 => 'phys'] as $category_id => $type) {
            preg_match_all('/<td.*>(.+)<\/td>.*<td.*>(.+)<\/td>/U', str_replace(PHP_EOL, '', $prices[$type]), $found);
            foreach ($found[1] as $i => $item) {
                $items[] = [
                    'name' => trim(strip_tags($item)),
                    'price' => $found[2][$i] ?? '', // strip_tags(preg_replace('/\D/', '', $found[2][$i] ?? '')),
                    'category_id' => $category_id,
                ];
            }
        }
        $this->request->set('prices', $items);
    }
}
