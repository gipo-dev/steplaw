<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        preg_match('/(https:\/\/[a-zA-Z0-9\/\.\_\-]+)/', $this->request->get('video')['src'] ?? '', $links);
        $video = $this->request->get('video');
        $video['src'] = $links[1] ?? '';
        $this->request->set('video', $video);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video.src' => 'required|max:255',
            'video.text' => 'required',
        ];
    }
}
