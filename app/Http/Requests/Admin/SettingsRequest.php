<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'settings.phone_1' => 'required|max:255',
            'settings.phone_2' => 'required|max:255',
            'settings.email' => 'required|email|max:255',
            'settings.address' => 'required|max:255',
        ];
    }
}
