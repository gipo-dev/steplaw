<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MentionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mention.link' => 'required|max:255',
            'mention.text' => 'required|max:255',
            'mention.year' => 'required|size:4',
            'mention.source_id' => 'required|integer',
        ];
    }
}
