<?php

namespace App\Http\Requests\Admin;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $post = $this->request->get('post');
        $page = $this->request->get('page');

        if (!$post['slug']) {
            $post['slug'] = Str::slug($post['name']);
            if (Post::where('slug', $post['slug'])->where('id', '!=', $post['id'] ?? -1)->count() > 0)
                $post['slug'] .= '_' . Str::random(4);
        }

        if (!isset($post['author_id']))
            $post['author_id'] = null;

        if ((!$page['meta_description'] ?? false) && isset($post['data']['description'])) {
            $page['meta_description'] = mb_substr($post['data']['description'] ?? '', 0, 254);
        }

        if (isset($page['body'])) {
            $page['body'] = json_decode($page['body']);
        }

        $this->request->set('post', $post);
        $this->request->set('page', $page);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post.name' => 'required|max:255',
            'post.slug' => 'required|max:255',
            'post.data.description' => 'required|max:500',

            'page.meta_title' => 'required|max:255',
            'page.meta_description' => 'required|max:255',
        ];
    }
}
