<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $comment = $this->request->get('comment');

        if (!isset($comment['reply_to']))
            $comment['reply_to'] = null;

        $this->request->set('comment', $comment);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment.name' => 'required|max:255',
            'comment.text' => 'required',
//            'comment.post_id' => 'required',
            'comment.created_at' => 'required',
        ];
    }
}
