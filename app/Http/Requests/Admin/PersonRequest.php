<?php

namespace App\Http\Requests\Admin;

use App\Models\Person;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

/**
 * Class PersonRequest
 * @package App\Http\Requests\Admin
 *
 * @property $person
 * @property $media
 */
class PersonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $person = $this->request->get('person');

        if (!$person['slug']) {
            $person['slug'] = Str::slug($person['name']);
            if (Person::where('slug', $person['slug'])->where('id', '!=', $person['id'] ?? -1)->count() > 0)
                $person['slug'] .= '_' . Str::random(4);
        }

        $this->request->set('person', $person);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'person.name' => 'required|max:255',
            'person.slug' => [
                'required',
                'max:255',
                Rule::unique('people', 'slug')
                    ->ignore($this->request->get('person')['id'] ?? null),
            ],
            'person.position' => 'required|max:255',
            'person.about' => 'required|max:255',

            'media.image' => 'required',
            'media.face' => 'required',

            'page.meta_title' => 'required|max:255',
            'page.meta_description' => 'required|max:255',
        ];
    }
}
