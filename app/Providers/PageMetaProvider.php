<?php

namespace App\Providers;

use App\Services\PageGenerator\PageMetaGenerator;
use Illuminate\Support\ServiceProvider;

class PageMetaProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PageMetaGenerator::class, function () {
            return new PageMetaGenerator();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
