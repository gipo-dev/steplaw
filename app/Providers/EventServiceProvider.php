<?php

namespace App\Providers;

use App\Events\Admin\CasePageSaved;
use App\Events\CallbackLeft;
use App\Listeners\Admin\GenerateCasePageDocumentPreview;
use App\Listeners\Callback\CheckSpam;
use App\Listeners\NotifyByEmail;
use App\Listeners\NotifyByTelegram;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        CallbackLeft::class => [
            CheckSpam::class,
            NotifyByTelegram::class,
            NotifyByEmail::class,
        ],

        CasePageSaved::class => [
            GenerateCasePageDocumentPreview::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
