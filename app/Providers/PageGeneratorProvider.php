<?php

namespace App\Providers;

use App\Services\LinkManager;
use App\Services\PageGenerator\PageGenerator;
use App\Services\PageGenerator\PageGeneratorInterface;
use Illuminate\Support\ServiceProvider;

class PageGeneratorProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('page_generator', function () {
            return new PageGenerator();
        });

        $this->app->singleton('link_manager', function () {
            return new LinkManager();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
