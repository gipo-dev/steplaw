<?php

namespace App\Providers;

use App\Services\MetricService;
use Illuminate\Support\ServiceProvider;

class MetricProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MetricService::class, function () {
            return new MetricService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
