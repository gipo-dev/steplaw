<?php

namespace App\Providers;

use App\Services\PageHandler;
use Illuminate\Support\ServiceProvider;

class PageHandlerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('page_handler', function () {
            return new PageHandler();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
