<?php

namespace App\Providers;

use App\Composers\MenuComposer;
use App\Composers\FooterComposer;
use App\Composers\ServiceComposer;
use App\Composers\VideoComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['partials.header'], MenuComposer::class);
        View::composer(['partials.footer'], FooterComposer::class);
        View::composer(['partials.blocks.carousel.videos'], VideoComposer::class);
        View::composer(['partials.blocks.services.services_related'], ServiceComposer::class);
    }
}
