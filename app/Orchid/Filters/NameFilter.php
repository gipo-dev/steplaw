<?php

namespace App\Orchid\Filters;

use App\Models\ServiceCategory;
use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;

class NameFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = ['name'];

    /**
     * @return string
     */
    public function name(): string
    {
        return 'Название';
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->where('name', 'like', '%' . $this->request->get('name') . '%');;
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [
            Input::make('name')
                ->type('text')
                ->value($this->request->get('name'))
                ->placeholder('Название...')
                ->title('Название:'),
        ];
    }
}
