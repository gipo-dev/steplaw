<?php

namespace App\Orchid\Filters;

use App\Models\ServiceCategory;
use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Select;

class ServiceCategoryFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = ['service_category_id'];

    /**
     * @return string
     */
    public function name(): string
    {
        return 'service_category_id';
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->whereIn('service_category_id', $this->request->get('service_category_id'));
    }

    /**
     * @return Field[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function display(): array
    {
        return [
            Select::make('service_category_id')
                ->multiple()
                ->value(array_map(function ($el) {
                    return (int)$el;
                }, $this->request->get('service_category_id') ?? []))
                ->fromModel(ServiceCategory::class, 'name')
                ->title('Категории:'),
        ];
    }
}
