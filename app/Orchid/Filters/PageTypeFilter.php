<?php

namespace App\Orchid\Filters;

use Illuminate\Database\Eloquent\Builder;
use Orchid\Filters\Filter;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;

class PageTypeFilter extends Filter
{
    /**
     * @var array
     */
    public $parameters = ['type'];

    /**
     * @return string
     */
    public function name(): string
    {
        return 'Тип страницы';
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function run(Builder $builder): Builder
    {
        return $builder->where('pageable_type', $this->request->type);
    }

    /**
     * @return Field[]
     */
    public function display(): array
    {
        return [
            Select::make('type')->options(__('loops.page.type'))->title('Тип страницы'),
        ];
    }
}
