<?php


namespace App\Orchid;


use Illuminate\Support\Facades\Route;
use Tabuna\Breadcrumbs\Trail;

class CrudRoutes
{
    public static function routes($singleKey, $manyKey, $listClass, $editClass)
    {
        // Platform > Class > Edit
        Route::screen("$manyKey/{" . $singleKey . "}/edit", $editClass)
            ->name("platform.$manyKey.edit")
            ->breadcrumbs(function (Trail $trail) use ($manyKey) {
                return $trail
                    ->parent("platform.$manyKey")
                    ->push('Редактирование');
            });

        // Platform > Class > Add
        Route::screen("$manyKey/add", $editClass)
            ->name("platform.$manyKey.add")
            ->breadcrumbs(function (Trail $trail) use ($manyKey, $singleKey) {
                return $trail
                    ->parent("platform.$manyKey")
                    ->push('Новая ' . __("admin.$singleKey.add"));
            });

        // Platform > Class
        Route::screen($manyKey, $listClass)
            ->name("platform.$manyKey")
            ->breadcrumbs(function (Trail $trail) use ($manyKey, $singleKey) {
                return $trail
                    ->parent('platform.index')
                    ->push(__("admin.$singleKey.list"), route("platform.$manyKey"));
            });
    }
}
