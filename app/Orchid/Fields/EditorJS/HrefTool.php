<?php


namespace App\Orchid\Fields\EditorJS;

use AlexSabur\OrchidEditorJSField\Fields\EditorJS\Tool;

class HrefTool extends Tool
{
    /**
     * @var string
     */
    protected $class = 'Hyperlink';
}
