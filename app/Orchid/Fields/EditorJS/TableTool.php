<?php


namespace App\Orchid\Fields\EditorJS;


use AlexSabur\OrchidEditorJSField\Fields\EditorJS\Tool;

class TableTool extends Tool
{
    /**
     * @var string
     */
    public $class = 'CustomTableTool';
}
