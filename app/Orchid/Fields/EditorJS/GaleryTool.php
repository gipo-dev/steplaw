<?php

namespace App\Orchid\Fields\EditorJS;

use AlexSabur\OrchidEditorJSField\Fields\EditorJS\Tool;

class GaleryTool extends Tool
{
    /**
     * @var string
     */
    protected $class = 'Carousel';
}
