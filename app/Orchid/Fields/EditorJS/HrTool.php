<?php

namespace App\Orchid\Fields\EditorJS;

use AlexSabur\OrchidEditorJSField\Fields\EditorJS\Tool;

class HrTool extends Tool
{
    /**
     * @var string
     */
    protected $class = 'HrTool';
}
