<?php

namespace App\Orchid\Fields\EditorJS;

use AlexSabur\OrchidEditorJSField\Fields\EditorJS\Tool;

class PersonTool extends Tool
{
    /**
     * @var string
     */
    protected $class = 'PersonTool';
}
