<?php


namespace App\Orchid\Fields;


use Orchid\Screen\Field;

class ServiceTypeUpdaterField extends Field
{
    protected $view = "orchid.fields.service_updater_field";
}
