<?php


namespace App\Orchid\Fields;


use AlexSabur\OrchidEditorJSField\Fields\EditorJS as EditorJSAlias;

class EditorJS extends EditorJSAlias
{
    /**
     * @var string
     */
    protected $view = 'orchid.fields.editorjs';
}
