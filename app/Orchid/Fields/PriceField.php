<?php

namespace App\Orchid\Fields;

use Orchid\Screen\Field;

/**
 * Class PriceField
 * @method PriceField value($value = true)
 */
class PriceField extends Field
{
    /**
     * @var string
     */
    protected $view = 'orchid.fields.price';

    /**
     * Default attributes value.
     *
     * @var array
     */
    protected $attributes = [
        'class' => 'form-control',
        'value' => [],
    ];

    /**
     * Attributes available for a particular tag.
     *
     * @var array
     */
    protected $inlineAttributes = [
        'accesskey',
        'autofocus',
        'disabled',
        'form',
        'placeholder',
        'name',
        'required',
        'size',
        'tabindex',
        'data-maximum-selection-length',
    ];


}
