<?php

namespace App\Orchid\Layouts;

use App\Models\Mention;
use App\Models\Thank;
use App\Models\WorkCase;
use OrchidCommunity\TinyMCE\TinyMCE;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class PersonPageLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Страница';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        return [
            Group::make([
                TextArea::make('page.meta_title')->maxlength(255)
                    ->rows(3)->title('Мета-заголовок (title)')->required(),
                TextArea::make('page.meta_description')->title('Мета-описание (description)')
                    ->maxlength(255)->rows(3)->required(),
            ]),
            Group::make([
                TextArea::make('page.body.about')
                    ->value($this->person->page->body['about'] ?? '')
                    ->rows(6)->required()
                    ->title('Краткое описание'),
                TextArea::make('page.body.quote')
                    ->value($this->person->page->body['quote'] ?? '')
                    ->rows(6)->required()
                    ->title('Цитата'),
            ]),
            Group::make([
                Relation::make('cases')->multiple()
                    ->fromModel(WorkCase::class, 'name')
                    ->title('Дела'),
                Relation::make('mentions')->multiple()
                    ->fromModel(Mention::class, 'text')
                    ->title('Умпоминания в СМИ'),
            ]),

            TinyMCE::make('page.body.profile')
                ->value($this->person->page->body['profile'] ?? '')->required()
                ->title('Специализация'),

            TinyMCE::make('page.body.practice')
                ->value($this->person->page->body['practice'] ?? '')->required()
                ->title('Практика'),

            TinyMCE::make('page.body.education')
                ->value($this->person->page->body['education'] ?? '')->required()
                ->title('Образование'),
        ];
    }
}
