<?php

namespace App\Orchid\Layouts;

use AlexSabur\OrchidEditorJSField\Fields\EditorJS\ChecklistTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\EmbedTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\HeaderTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\ImageTool;
use App\Orchid\Fields\EditorJS\GaleryTool;
use App\Orchid\Fields\EditorJS\HrefTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\ListTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\MarkerTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\ParagraphTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\QuoteTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\TableTool;
use AlexSabur\OrchidEditorJSField\Fields\EditorJS\Tool;
use App\Orchid\Fields\EditorJS\HrTool;
use App\Orchid\Fields\EditorJS\PersonTool;

class DefaultEditorJSLayout //extends EditorJS
{
    /**
     * Data source.
     *
     * @var string
     */
    public $target = null;

    /**
     *
     * @return Tool[]
     */
    public function tools(): array
    {
        return [
            ParagraphTool::make('paragraph'),
            ChecklistTool::make('check-list'),
            HeaderTool::make('header'),
            ImageTool::make('image'),
//            LinkTool::make('link'),
            ListTool::make('list'),
            MarkerTool::make('marker'),
            ParagraphTool::make('paragraph'),
            QuoteTool::make('quote'),
            TableTool::make('table'),
            HrTool::make('hr'),
            EmbedTool::make('embed')->config([
                'services' => [
                    'youtube' => true,
                    'twitter' => true,
                    'instagram' => true,
                ],
            ]),
            HrefTool::make('link'),
            PersonTool::make('person'),
            GaleryTool::make('gallery')->config([
                'endpoints' => [
                    'byFile' => route('platform.systems.editorjs.image-by-file'),
                ],
            ]),
        ];
    }
}
