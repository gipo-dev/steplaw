<?php

namespace App\Orchid\Layouts;

use App\Orchid\Filters\PageTypeFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class PageFilters extends Selection
{
    /**
     * @var string
     */
    public $template = self::TEMPLATE_LINE;

    /**
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            PageTypeFilter::class,
        ];
    }
}
