<?php

namespace App\Orchid\Layouts\CasePage;

use App\Orchid\Screens\CasePage\CasePageEditScreen;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Support\Facades\Layout;
use stdClass;

class ArticleFields extends \Orchid\Screen\Layouts\Rows
{
    private static $lastIndex = 0;

    protected function fields(): array
    {
        return [
            Group::make([
                Input::make('number')
                    ->title('№ статьи')
                    ->type('number')
                    ->required(),
                Input::make('part')
                    ->title('Часть')
                    ->type('number'),
                Select::make('category')
                    ->title('Категория')
                    ->options(
                        collect(__('pages.case_page.categories'))->map(function ($category) {
                            return $category['small'];
                        })->keyBy(function ($item, $key) {
                            return '_' . $key;
                        })->toArray()
                    )
                    ->required(),
            ]),
        ];
    }
}
