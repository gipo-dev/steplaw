<?php


namespace App\Orchid\Layouts;


use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Color;

class EditScreenButtons
{
    /**
     * @param bool $showDelete
     * @param string|null $view_route
     * @return array
     */
    public static function defaultButtons(int $deleteId = null, string $viewRoute = null): array
    {
        return [
            Button::make('Удалить')
                ->icon('trash')
                ->confirm('Подтверждаете удаление?')
                ->method('delete')
                ->type(Color::DANGER())
                ->canSee(!is_null($deleteId))
                ->parameters([
                    'id' => $deleteId,
                ]),

            Link::make('Перейти')
                ->icon('arrow-right-circle')
                ->type(Color::PRIMARY())
                ->href($viewRoute ?? '')
                ->target('_blank')
                ->canSee(!is_null($viewRoute)),

            Button::make('Сохранить')
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),
        ];
    }
}
