<?php

namespace App\Orchid\Layouts\Person;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Layouts\Rows;

class SkillLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = '';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        return [
            Group::make([
                DateTimer::make('date')->allowInput()->title('Дата')->required(),
                Input::make('name')->title('Название')->required(),
            ]),
            Picture::make('logo')
                ->title('Логотип')->required(),
        ];
    }
}
