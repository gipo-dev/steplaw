<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class PageMetaLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Мета описание';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        return [
            Textarea::make('page.meta_title')
                ->maxlength(255)->rows(3)
                ->title('Мета заголовок (title)')->required(),

            TextArea::make('page.meta_description')
                ->maxlength(255)
                ->title('Мета описание (description)')->rows(5)->required(),
        ];
    }
}
