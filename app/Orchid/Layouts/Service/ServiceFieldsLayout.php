<?php

namespace App\Orchid\Layouts\Service;

use App\Models\CasePage;
use App\Models\Service;
use App\Models\ServiceCategory;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class ServiceFieldsLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title = 'Описание услуги';

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        $service = $this->query->get('service');
        return [
            Group::make([
                Input::make('service.name')->title('Название')->required(),
                Input::make('service.slug')->title('Ссылка')
                    ->popover('Если оставить пустым - url сгенерируется автоматически'),
                Relation::make('service.service_category_id')
                    ->fromModel(ServiceCategory::class, 'name')
                    ->title('Категория')->required(),
                Select::make('service.status_id')
                    ->options(__('admin.service.status'))->title('Статус'),
                Input::make('service.order')->type('number')
                    ->title('№ сортировки'),
            ]),
            Group::make([
                Relation::make('service.parent_id')
                    ->fromModel(Service::class, 'name')
//                    ->applyScope('parentsOnly', [])
                    ->title('Родительская категория'),
                Relation::make('childs.')
                    ->fromModel(Service::class, 'name')
                    ->multiple()
                    ->value($service->childs ?? [])
                    ->title('Подкатегории'),
                Relation::make('case_pages.')
                    ->fromModel(CasePage::class, 'name')
                    ->multiple()
                    ->value($service->casePages ?? [])
                    ->title('Дела'),
            ]),
            Input::make('service.id')->hidden(),
        ];
    }
}
