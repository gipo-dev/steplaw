<?php

namespace App\Orchid\Layouts\Service;

use App\Orchid\Filters\NameFilter;
use App\Orchid\Filters\ServiceCategoryFilter;
use Orchid\Filters\Filter;
use Orchid\Screen\Layouts\Selection;

class ServiceFilterLayout extends Selection
{
    /**
     * @var string
     */
    public $template = self::TEMPLATE_LINE;

    /**
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            NameFilter::class,
            ServiceCategoryFilter::class,
        ];
    }
}
