<?php

namespace App\Orchid;

use App\Models\Callback;
use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemMenu;
use Orchid\Platform\ItemPermission;
use Orchid\Platform\OrchidServiceProvider;
use Orchid\Support\Color;

class PlatformProvider extends OrchidServiceProvider
{
    /**
     * @param Dashboard $dashboard
     */
    public function boot(Dashboard $dashboard): void
    {
        parent::boot($dashboard);

        // ...
    }

    /**
     * @return ItemMenu[]
     */
    public function registerMainMenu(): array
    {
        $menu = [
            ItemMenu::label('Обратная связь')
                ->icon('phone')
                ->route('platform.callbacks')
                ->title('Заявки')
                ->badge(function () {
                    return Callback::where('status_id', Callback::STATUS_NEW)->count();
                }),


            ItemMenu::label('Список услуг')
                ->icon('money')
                ->route('platform.services')
                ->title('Услуги'),

            ItemMenu::label('Категории')
                ->icon('list')
                ->route('platform.service_categories'),

            ItemMenu::label('Вопрос-ответ')
                ->icon('question')
                ->route('platform.faqs'),

            ItemMenu::label('Адвокаты')
                ->icon('user')
                ->route('platform.persons'),


            ItemMenu::label('Кейсы')
                ->icon('briefcase')
                ->route('platform.cases')
                ->title('Кейсы'),

            ItemMenu::label('Дела')
                ->icon('notebook')
                ->route('platform.casepages'),

            ItemMenu::label('Отзывы от компаний')
                ->icon('docs')
                ->route('platform.reviews'),

            ItemMenu::label('Благодарности от людей')
                ->icon('like')
                ->route('platform.thanks'),

            ItemMenu::label('Фотоотзывы')
                ->icon('photo')
                ->route('platform.photoreviews'),

            ItemMenu::label('FAQ')
                ->icon('question')
                ->route('platform.qas'),


            ItemMenu::label('Публикации в СМИ')
                ->icon('feed')
                ->route('platform.mentions')
                ->title('СМИ'),

            ItemMenu::label('Источники СМИ')
                ->icon('social-soundcloud')
                ->route('platform.mention_sources'),

            ItemMenu::label('Видео')
                ->icon('social-youtube')
                ->route('platform.videos'),


            ItemMenu::label('Публикации')
                ->icon('note')
                ->route('platform.posts')
                ->title('Публикации'),

            ItemMenu::label('Категории публикаций')
                ->icon('map')
                ->route('platform.post_categories'),

            ItemMenu::label('Комментарии')
                ->icon('bubbles')
                ->route('platform.comments'),


            ItemMenu::label('Страницы')
                ->icon('browser')
                ->route('platform.pages')
                ->title('Страницы'),

            ItemMenu::label('Фотоальбомы')
                ->icon('picture')
                ->route('platform.albums'),


            ItemMenu::label('Настройки')
                ->icon('settings')
                ->route('platform.settings')
                ->title('Настройки'),
        ];

        foreach ($menu as $i => $item) {
            $item->sort($i * 100);
        }

        return $menu;
    }

    /**
     * @return ItemMenu[]
     */
    public function registerProfileMenu(): array
    {
        return [
            ItemMenu::label('Profile')
                ->route('platform.profile')
                ->icon('user'),
        ];
    }

    /**
     * @return ItemMenu[]
     */
    public function registerSystemMenu(): array
    {
        return [
            ItemMenu::label(__('Access rights'))
                ->icon('lock')
                ->slug('Auth')
                ->active('platform.systems.*')
                ->permission('platform.systems.index')
                ->sort(1000),

            ItemMenu::label(__('Users'))
                ->place('Auth')
                ->icon('user')
                ->route('platform.systems.users')
                ->permission('platform.systems.users')
                ->sort(1000)
                ->title(__('All registered users')),

            ItemMenu::label(__('Roles'))
                ->place('Auth')
                ->icon('lock')
                ->route('platform.systems.roles')
                ->permission('platform.systems.roles')
                ->sort(1000)
                ->title(__('A Role defines a set of tasks a user assigned the role is allowed to perform.')),
        ];
    }

    /**
     * @return ItemPermission[]
     */
    public function registerPermissions(): array
    {
        return [
            ItemPermission::group(__('Systems'))
                ->addPermission('platform.systems.roles', __('Roles'))
                ->addPermission('platform.systems.users', __('Users')),
        ];
    }

    /**
     * @return string[]
     */
    public function registerSearchModels(): array
    {
        return [
            // ...Models
            // \App\Models\User::class
        ];
    }
}
