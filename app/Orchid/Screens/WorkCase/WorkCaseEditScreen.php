<?php

namespace App\Orchid\Screens\WorkCase;

use App\Http\Requests\Admin\CaseRequest;
use App\Models\Service;
use App\Models\WorkCase;
use App\Orchid\Layouts\EditScreenButtons;
use Orchid\Screen\Actions\Link;
use Orchid\Support\Color;
use OrchidCommunity\TinyMCE\TinyMCE;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class WorkCaseEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование кейса';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';
    /**
     * @var \App\Models\WorkCase
     */
    private WorkCase $case;

    /**
     * Query data.
     *
     * @param \App\Models\WorkCase $case
     * @return array
     */
    public function query(WorkCase $case): array
    {
        $this->case = $case;

        if (!$this->case->exists)
            $this->name = 'Добавление кейса';

        return [
            'case' => $case,
            'services' => $case->services,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        $buttons = [];
//        if ($this->case->page ?? false) {
//            $buttons[] = Link::make('Перейти к делу')
//                ->href(route('platform.casepages.edit', $this->case->page->id))
//                ->type(Color::WARNING())->icon('briefcase');
//        }
        return array_merge($buttons, EditScreenButtons::defaultButtons($this->case->id));
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [

            Layout::rows([
                Group::make([
                    Input::make('case.name')->title('Название')->required(),
                    Select::make('case.type')->options([
                        WorkCase::TYPE_INDEX => 'Для главной',
                        WorkCase::TYPE_SERVICE => 'Для услуги',
                    ])->title('Тип')->required(),
                    Relation::make('services')->fromModel(Service::class, 'name')
                        ->multiple()->title('Услуги'),
                ]),
                Group::make([
                    TinyMCE::make('case.body')->title('Описание')->required(),
                    TinyMCE::make('case.result')->title('Результат')->required(),
                ]),
            ])->title('Данные'),
        ];
    }

    /**
     * @param \App\Models\WorkCase $case
     * @param \App\Http\Requests\Admin\CaseRequest $request
     */
    public function save(WorkCase $case, CaseRequest $request)
    {
        $case->fill($request->case)->save();
        $case->services()->sync($request->services);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        WorkCase::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.cases'));
    }
}
