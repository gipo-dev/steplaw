<?php

namespace App\Orchid\Screens\PhotoGroup;

use App\Models\Person;
use App\Models\PhotoGroup;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\ModalToggle;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PhotoGroupListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Альбомы';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'photos' => PhotoGroup::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.albums.add'),

            ModalToggle::make('Массовая загрузка')
                ->icon('plus')->type(Color::PRIMARY())
                ->modal('modal_mass')
                ->method('save')
                ->modalTitle('Массовая загрузка фотографий'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('photos', [

                TD::make('id', 'Фотографии')
                    ->render(function (PhotoGroup $photoGroup) {
                        return view('orchid.fields.image_group', [
                            'link' => route('platform.albums.edit', $photoGroup),
                            'images' => $photoGroup->getImages(),
                        ]);
                    })->sort(),

                TD::make('title', 'Заголовок')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (PhotoGroup $photoGroup) {
                        return Link::make($photoGroup->title)->route('platform.albums.edit', $photoGroup);
                    }),

                TD::make('description', 'Описание')
                    ->sort()->filter(TD::FILTER_TEXT),

                TD::make('', 'Услуги')
                    ->render(function (PhotoGroup $photoGroup) {
                        return $photoGroup->serviceCategories->pluck('name')->implode('; ');
                    }),

                TD::make('id', 'Добавлен')->render(function ($photoGroup) {
                    return $photoGroup->created_at->format('d.m.Y в H:i');
                })->sort(),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (PhotoGroup $photoGroup) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.albums.edit', $photoGroup)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $photoGroup->id,
                                    ]),
                            ]);
                    }),
            ]),

            Layout::modal('modal_mass', [
                Layout::rows([
                    Upload::make('images')->title('Фотографии'),
                    Select::make('group_images')->options([
                        0 => 'Нет',
                        1 => 'Да',
                    ])->title('Сгруппировать фото?'),
                ]),
            ])->applyButton('Загрузить фотографии'),

        ];
    }

    public function save(Request $request)
    {
        $groups = [];
        if ($request->group_images) {
            $groups[] = $request->images;
        } else {
            foreach ($request->images as $image) {
                $groups[] = [$image];
            }
        }

        foreach ($groups as $group) {
            PhotoGroup::create([
                'images' => $group,
            ]);
        }
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        PhotoGroup::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
