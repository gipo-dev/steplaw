<?php

namespace App\Orchid\Screens\PhotoGroup;

use App\Http\Requests\Admin\PersonRequest;
use App\Models\Person;
use App\Models\PhotoGroup;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\Thank;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\Person\SkillLayout;
use App\Orchid\Layouts\PersonPageLayout;
use App\Orchid\Layouts\PhotoGroup\PhotoLayout;
use App\Orchid\Layouts\SaveButtonLayout;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Nakukryskin\OrchidRepeaterField\Fields\Repeater;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use OrchidCommunity\TinyMCE\TinyMCE;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PhotoGroupEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование альбома';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';
    /**
     * @var \App\Models\PhotoGroup
     */
    private PhotoGroup $photoGroup;

    /**
     * Query data.
     *
     * @param \App\Models\PhotoGroup $photoGroup
     * @return array
     */
    public function query(PhotoGroup $photoGroup): array
    {
        $this->photoGroup = $photoGroup;

        if (!$this->photoGroup->exists)
            $this->name = 'Добавление альбома';

        return [
            'album' => $photoGroup,
            'media' => $photoGroup->media,
            'service_categories' => $photoGroup->serviceCategories,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->photoGroup->id,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::columns([

                Layout::rows([

                    Input::make('album.title')->title('Заголовок'),
                    TextArea::make('album.description')
                        ->rows(5)->title('Описание'),
                    Relation::make('service_categories')
                        ->fromModel(ServiceCategory::class, 'name')
                        ->multiple()->title('Категории услуг'),

                ])->title('Данные'),

                Layout::rows([

                    Upload::make('album.images')
                        ->acceptedFiles('image/*'),

                ])->title('Фотографии'),

            ]),

            SaveButtonLayout::class,
        ];
    }

    /**
     * @param \App\Models\Person $person
     * @param \App\Http\Requests\Admin\PersonRequest $request
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded
     */
    public function save(PhotoGroup $photoGroup, Request $request)
    {
        $photoGroup->fill($request->album)->save();
        $photoGroup->serviceCategories()->sync($request->service_categories);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        PhotoGroup::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.albums'));
    }
}
