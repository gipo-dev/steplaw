<?php

namespace App\Orchid\Screens\ServiceCategory;

use App\Http\Requests\Admin\ServiceCategoryRequest;
use App\Models\ServiceCategory;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\PageMetaLayout;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ServiceCategoryEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование категории услуги';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';
    /**
     * @var \App\Models\ServiceCategory
     */
    private ServiceCategory $category;

    /**
     * Query data.
     *
     * @param \App\Models\ServiceCategory $category
     * @return array
     */
    public function query(ServiceCategory $category): array
    {
        $this->category = $category;

        if (!$category->exists)
            $this->name = 'Добавление категории услуги';

        return [
            'category' => $category,
            'page' => $category->page,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->category->exists,
            $this->category->id ? route('page.services.category', $this->category) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Input::make('category.name')->title('Название')->required(),
                    Input::make('category.slug')->title('Ссылка')->required(),
                    Input::make('category.id')->hidden(),
                ])->title('Данные'),
                PageMetaLayout::class,
            ]),
        ];
    }

    /**
     * @param \App\Models\ServiceCategory $category
     * @param \App\Http\Requests\Admin\ServiceCategoryRequest $request
     */
    public function save(ServiceCategory $category, ServiceCategoryRequest $request)
    {
        $category->fill($request->category)->save();
        $category->page()->firstOrNew()->fill($request->page)->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        ServiceCategory::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.service_categories'));
    }
}
