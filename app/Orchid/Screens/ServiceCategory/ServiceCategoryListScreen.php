<?php

namespace App\Orchid\Screens\ServiceCategory;

use App\Models\ServiceCategory;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ServiceCategoryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Категории услуг';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'categories' => ServiceCategory::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.service_categories.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('categories', [
                TD::make('name', 'Название')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (ServiceCategory $category) {
                        return Link::make($category->name)->route('platform.service_categories.edit', $category);
                    }),
                TD::make('slug', 'Ссылка')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (ServiceCategory $category) {
                        return $category->slug;
                    }),
                TD::make('created_at', 'Добавлена')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (ServiceCategory $category) {
                        return $category->created_at ? $category->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (ServiceCategory $category) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.service_categories.edit', $category)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $category->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    public function delete($id)
    {
        ServiceCategory::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
