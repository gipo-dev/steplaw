<?php

namespace App\Orchid\Screens\QA;

use App\Models\QA;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;

class QaListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'FAQ';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'items' => QA::with(['services'])
                ->filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.qas.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('items', [
                TD::make('id', 'Название')->render(function ($item) {
                    return Link::make(mb_substr($item->name ?? '', 0, 100) . '...')
                        ->route('platform.qas.edit', $item);
                }),
//                TD::make('', 'Документ')->render(function ($case) {
//                    return count($case->files ?? []) > 0 ? 'Есть' : 'Нет';
//                }),
//                TD::make('', 'Услуга')->render(function ($case) {
//                    return $case->services->pluck('name')->implode("<br>");
//                }),
                TD::make('', 'Добавлен')->render(function ($item) {
                    return $item->created_at->format('d.m.Y в H:i');
                }),
            ]),
        ];
    }
}
