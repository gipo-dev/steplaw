<?php

namespace App\Orchid\Screens\QA;

use App\Events\Admin\CasePageSaved;
use App\Http\Requests\CasePageRequest;
use App\Http\Requests\QaRequest;
use App\Models\CasePage;
use App\Models\Person;
use App\Models\QA;
use App\Models\Service;
use App\Models\WorkCase;
use App\Orchid\Fields\BigTitlesField;
use App\Orchid\Layouts\CasePage\ArticleFields;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\PageMetaLayout;
use App\Orchid\Layouts\SaveButtonLayout;
use Nakukryskin\OrchidRepeaterField\Fields\Repeater;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use OrchidCommunity\TinyMCE\TinyMCE;

class QaEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование FAQ';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\CasePage
     */
    private Qa $qa;

    /**
     * Query data.
     *
     * @param \App\Models\CasePage $casePage
     * @return array
     */
    public function query(QA $qa): array
    {
        $this->qa = $qa;

        if (!$qa->exists)
            $this->name = 'Добавление FAQ';

        return [
            'item' => $qa,
            'services' => $qa->services ?? [],
            'persons' => $qa->persons ?? [],
            'page' => $qa->page,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->qa->id,
            $this->qa->exists ? route('page.qa.item', $this->qa) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [

            Layout::columns([
                Layout::rows([

                    Relation::make('services.')
                        ->fromModel(Service::class, 'name')
                        ->multiple()->title('Услуги'),

                    Group::make([
                        Input::make('item.properties.questioner')
                            ->title('Имя и город задающего вопрос')
                            ->maxlength(255),
                        DateTimer::make('item.properties.date')->format('d.m.Y')
                            ->title('Дата'),
                    ]),

                    Group::make([
                        Relation::make('persons.')
                            ->fromModel(Person::class, 'name')
                            ->multiple()->title('Адвокаты'),
                    ]),

                ])->title('Дополнительные данные'),


                Layout::rows([
                    Textarea::make('page.meta_title')
                        ->maxlength(255)->rows(3)
                        ->title('Мета заголовок (title)')->required(),

                    TextArea::make('page.meta_description')
                        ->maxlength(255)
                        ->title('Мета описание (description)')->rows(5),
                ])->title('Мета описание'),
            ]),

            Layout::rows([
                Textarea::make('page.body.question')
                    ->maxlength(255)->rows(3)
                    ->title('Вопрос - h1')
                    ->value($this->casePage->page->body['question'] ?? '')
                    ->class('form-control no-resize title_big')
                    ->rows(2)->required(),

                Textarea::make('page.body.question_description')
                    ->value($this->casePage->page->body['question_description'] ?? '')
                    ->title('Расширенное описание вопроса')->rows(6)
                    ->class('form-control no-resize title_big'),

                TinyMCE::make('page.body.answer')
                    ->value($this->casePage->page->body['answer'] ?? '')
                    ->title('Ответ на вопрос')
                    ->class('title_big')
                    ->required(),

                BigTitlesField::make('big_titles'),
            ]),
            SaveButtonLayout::class,
        ];
    }

    /**
     * @param \App\Models\CasePage $casePage
     * @param \App\Http\Requests\CasePageRequest $request
     * @return void
     */
    public function save(QA $item, QaRequest $request)
    {
        $item->fill($request->item)->save();
        $item->page()->firstOrNew()->fill($request->page)->save();
        $item->services()->sync($request->services);
        $item->persons()->sync($request->persons);

        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        QA::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.qas'));
    }
}
