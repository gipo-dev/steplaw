<?php

namespace App\Orchid\Screens\Mention;

use App\Http\Requests\Admin\MentionRequest;
use App\Models\Mention;
use App\Models\MentionSource;
use App\Orchid\Layouts\EditScreenButtons;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class MentionEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование публикации';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Mention
     */
    private Mention $mention;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Mention $mention): array
    {
        $this->mention = $mention;

        if (!$this->mention->exists)
            $this->name = 'Добавление публикации';

        return [
            'mention' => $mention,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->mention->id);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('mention.link')->title('Ссылка')->required(),
                    Input::make('mention.year')->title('Год')
                        ->type('number')->min(2000)->max(now()->year)->required(),
                    Relation::make('mention.source_id')->title('Источник')
                        ->fromModel(MentionSource::class, 'name')->required(),
                ]),
                TextArea::make('mention.text')->title('Текст')
                    ->rows(3)->maxlength(255)->required(),
            ]),
        ];
    }

    /**
     * @param \App\Models\Mention $mention
     * @param \App\Http\Requests\Admin\MentionRequest $request
     */
    public function save(Mention $mention, MentionRequest $request)
    {
        $mention->fill($request->get('mention'))->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Mention::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.mentions'));
    }
}
