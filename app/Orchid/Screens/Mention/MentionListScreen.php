<?php

namespace App\Orchid\Screens\Mention;

use App\Models\Mention;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class MentionListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Публикации в СМИ';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'mentions' => Mention::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.mentions.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('mentions', [

                TD::make('text', 'Текст')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Mention $mention) {
                        return Link::make($mention->text)->route('platform.mentions.edit', $mention)
                            ->style('max-width: 20vw;display: inline-block;white-space: normal;');;
                    }),
                TD::make('source', 'Источник')
                    ->sort()
                    ->render(function (Mention $mention) {
                        return $mention->source->name ?? '';
                    }),
                TD::make('year', 'Год')
                    ->sort()
                    ->filter(TD::FILTER_NUMERIC),
                TD::make('created_at', 'Добавлен')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (Mention $mention) {
                        return $mention->created_at ? $mention->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Mention $mention) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.mentions.edit', $mention)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $mention->id,
                                    ]),
                            ]);
                    }),
            ])
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Mention::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
