<?php

namespace App\Orchid\Screens\Person;

use App\Http\Requests\Admin\PersonRequest;
use App\Models\Person;
use App\Models\Thank;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\Person\SkillLayout;
use App\Orchid\Layouts\PersonPageLayout;
use App\Orchid\Layouts\SaveButtonLayout;
use Illuminate\Support\Str;
use Nakukryskin\OrchidRepeaterField\Fields\Repeater;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Upload;
use OrchidCommunity\TinyMCE\TinyMCE;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PersonEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование адвоката';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';
    /**
     * @var \App\Models\Person
     */
    private Person $person;

    /**
     * Query data.
     *
     * @param \App\Models\Person $person
     * @return array
     */
    public function query(Person $person): array
    {
        $this->person = $person;

        if (!$this->person->exists)
            $this->name = 'Добавление адвоката';

        return [
            'person' => $person,
            'page' => $person->page,
            'cases' => $person->cases,
            'mentions' => $person->mentions,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->person->id,
            $this->person->exists ? route('page.persons.person', $this->person->slug) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('person.name')->title('Имя')->required(),
                    Input::make('person.slug')->title('Ссылка')
                        ->popover('Если оставить пустым - url сгенерируется автоматически'),
                    Input::make('person.position')->title('Должность')->required(),
                    Input::make('person.data.post')->title('Род занятий')->required(),
                ]),
                Group::make([
                    Input::make('person.data.name.gen')->title('Имя в род. падеже')->required(),
                    TextArea::make('person.about')->title('Краткое описание')
                        ->rows(3)->maxlength(255)->required(),
                    Input::make('person.order')->type('number')
                        ->title('№ сортировки'),
                ]),
                Input::make('person.id')->hidden(),
            ])->title('Данные'),

            PersonPageLayout::class,

            Layout::columns([
                Layout::rows([
                    Repeater::make('person.data.skills')
                        ->layout(SkillLayout::class)->title('Повышение квалификации'),
                ])->title('Повышения квалификации'),

                Layout::rows([
                    Relation::make('person.data.reviews')
                        ->fromModel(Thank::class, 'name')
                        ->multiple()->title('Отзывы об адвокате'),
                    Upload::make('person.data.certificates')->acceptedFiles('image/*')
                        ->title('Грамоты и сертификаты'),
                ])->title('Дополнительные поля'),
            ]),

            Layout::rows([
                Group::make([
                    Picture::make('media.image')->title('Фото в полный рост')->required()
                        ->value($this->person->getFirstMediaUrl('image'))->targetRelativeUrl(),
                    Picture::make('media.face')->title('Фото лица')->required()
                        ->value($this->person->getFirstMediaUrl('face'))->targetRelativeUrl(),
                ]),
            ])->title('Изображения'),

            SaveButtonLayout::class,
        ];
    }

    /**
     * @param \App\Models\Person $person
     * @param \App\Http\Requests\Admin\PersonRequest $request
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded
     */
    public function save(Person $person, PersonRequest $request)
    {
        $personData = $request->person;

        if(!$personData['order']) {
            $maxOrder = Person::max('order') ?? 1;
            $personData['order'] = $maxOrder + 1;
        }

        $person->fill($personData)->save();
        $person->page()->firstOrNew()->fill($request->page)->save();
        $person->cases()->sync($request->cases);
        $person->mentions()->sync($request->mentions);
        $this->saveMedia($person, $request->media);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Person::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.persons'));
    }

    /**
     * @param \App\Models\Person $person
     * @param array $media
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\MediaCannotBeDeleted
     */
    private function saveMedia(Person $person, array $media)
    {
        foreach ($media as $collection => $image) {
            if (Str::startsWith($image, ['http://', 'https://']))
                continue;
            if ($mediaItem = ($person->getFirstMedia($collection) ?? false))
                $person->deleteMedia($mediaItem);
            if ($image)
                $person->addMedia(public_path($image))->preservingOriginal()->toMediaCollection($collection);
        }
    }
}
