<?php

namespace App\Orchid\Screens\Comment;

use App\Http\Requests\Admin\CommentRequest;
use App\Models\Comment;
use App\Models\Post;
use App\Orchid\Layouts\EditScreenButtons;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CommentEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование комментария';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Comment
     */
    private Comment $comment;

    /**
     * Query data.
     *
     * @param \App\Models\Comment $comment
     * @return array
     */
    public function query(Comment $comment): array
    {
        $this->comment = $comment;

        return [
            'comment' => $comment,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return array_merge([
            Link::make('Ответы')->route('platform.comments', [
                'filter[reply_to]' => $this->comment->id
            ])
                ->type(Color::WARNING())->icon('layers'),
        ], EditScreenButtons::defaultButtons($this->comment->id,
            $this->comment->post ? $this->comment->post->getLink() : null));
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('comment.name')->maxlength(255)->title('Имя')->required(),
//                    Relation::make('comment.post_id')->fromModel(Post::class, 'name')
//                        ->title('Пост')->required(),
                    Relation::make('comment.reply_to')->fromModel(Comment::class, 'text')
                        ->title('Ответ к комментарию'),
                ]),
                Group::make([
                    TextArea::make('comment.text')->title('Текст комментария')
                        ->rows(10)->required(),
                    DateTimer::make('comment.created_at')->enableTime()
                        ->title('Дата добавления')->required(),
                ]),
            ]),
        ];
    }

    /**
     * @param \App\Models\Comment $comment
     * @param \App\Http\Requests\Admin\CommentRequest $request
     */
    public function save(Comment $comment, CommentRequest $request)
    {
        $comment->fill($request->comment)->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Comment::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.comments'));
    }
}
