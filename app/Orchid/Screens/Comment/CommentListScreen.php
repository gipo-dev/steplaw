<?php

namespace App\Orchid\Screens\Comment;

use App\Models\Comment;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CommentListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Комментарии';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'comments' => Comment::filters()->defaultSort('id', 'desc')->paginate(50),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
//            Link::make('Добавить')
//                ->icon('plus')->type(Color::SUCCESS())
//                ->route('platform.comments.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('comments', [
                TD::make('text', 'Текст')->render(function (Comment $comment) {
                    return Link::make(mb_substr($comment->text, 0, 50))
                        ->route('platform.comments.edit', $comment);
                })->sort()->filter(TD::FILTER_TEXT),
                TD::make('reply_to', 'Ответ на комментарий')->render(function (Comment $comment) {
                    if ($comment->parent) {
                        return Link::make(mb_substr($comment->parent->text, 0, 50))
                            ->route('platform.comments.edit', $comment->parent);
                    }
                    return '';
                })->sort()->filter(TD::FILTER_TEXT),
                TD::make('post_id', 'Пост')->render(function (Comment $comment) {
                    if ($comment->commentable) {
                        return $comment->commentable->name;
//                        return Link::make($comment->post->name ?? '')
//                            ->href($comment->post->getLink())->target('_blank');
                    }
                    return '';
                })->sort()->filter(TD::FILTER_TEXT),

                TD::make('name', 'Имя')->render(function (Comment $comment) {
                    if ($comment->person) {
                        return Link::make('Адвокат: ' . $comment->person->name)
                            ->route('platform.persons.edit', $comment->person->id);
                    }
                    return $comment->name;
                })->sort()->filter(TD::FILTER_TEXT),

                TD::make('created_at', 'Добавлен')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (Comment $comment) {
                        return $comment->created_at ? $comment->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Comment $comment) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.comments.edit', $comment)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $comment->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    public function delete($id)
    {
        Comment::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
