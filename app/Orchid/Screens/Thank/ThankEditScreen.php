<?php

namespace App\Orchid\Screens\Thank;

use App\Http\Requests\Admin\ThankRequest;
use App\Models\CasePage;
use App\Models\Service;
use App\Models\Thank;
use App\Orchid\Layouts\EditScreenButtons;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ThankEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование благодарности';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Thank
     */
    private Thank $thank;

    /**
     * Query data.
     *
     * @param \App\Models\Thank $thank
     * @return array
     */
    public function query(Thank $thank): array
    {
        $this->thank = $thank;

        if (!$this->thank->exists)
            $this->name = 'Добавление благодарности';

        return [
            'thank' => $thank,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->thank->id);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('thank.name')->title('Имя')
                        ->maxlength(255)->required(),
                    Input::make('thank.city')->title('Город')
                        ->maxlength(255)->required(),
                    Relation::make('thank.service_id')
                        ->fromModel(Service::class, 'name')
                        ->title('Услуга'),
                    Relation::make('thank.case_page_id')
                        ->fromModel(CasePage::class, 'name')
                        ->title('Дело'),
                ]),
                TextArea::make('thank.body')->title('Текст')
                    ->rows(5)->required(),
            ]),
        ];
    }

    /**
     * @param \App\Models\Thank $thank
     * @param \App\Http\Requests\Admin\ThankRequest $request
     */
    public function save(Thank $thank, ThankRequest $request)
    {
        $thank->fill($request->thank)->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Thank::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.thanks'));
    }
}
