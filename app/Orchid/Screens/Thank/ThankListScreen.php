<?php

namespace App\Orchid\Screens\Thank;

use App\Models\Thank;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ThankListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Благодарности';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'thanks' => Thank::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.thanks.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('thanks', [
                TD::make('name', 'Имя')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Thank $thank) {
                        return Link::make($thank->name)->route('platform.thanks.edit', $thank);
                    }),
                TD::make('service.name', 'Услуга')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Thank $thank) {
                        if ($thank->service_id)
                            return Link::make($thank->service->name)->route('platform.services.edit', $thank->service_id);
                        return '';
                    }),
                TD::make('created_at', 'Добавлен')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (Thank $thank) {
                        return $thank->created_at ? $thank->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Thank $thank) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.thanks.edit', $thank)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $thank->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Thank::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
