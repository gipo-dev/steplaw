<?php

namespace App\Orchid\Screens\PostCategory;

use App\Http\Requests\Admin\PostCategoryRequest;
use App\Models\PostCategory;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\PageMetaLayout;
use Illuminate\Support\Str;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PostCategoryEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование категории';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\PostCategory
     */
    private PostCategory $category;

    /**
     * Query data.
     *
     * @return array
     */
    public function query(PostCategory $category): array
    {
        $this->category = $category;

        if (!$this->category->exists)
            $this->name = 'Добавление категории';

        return [
            'category' => $category,
            'page' => $category->page,
            'media' => $category->media,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->category->id,
            $this->category->exists ? route('page.posts.category', $this->category->slug) : null);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Input::make('category.id')->type('hidden'),
                    Input::make('category.name')->title('Название')->required(),
                    Input::make('category.slug')->title('Ссылка (url)')
                        ->popover('Оставьте пустым для автоматической генерации'),
                ])->title('Данные'),

                PageMetaLayout::class,

                Layout::rows([
                    Cropper::make('media.image')->title('Изображение')
                        ->width(365)->height(220)
                        ->value($this->category->getFirstMediaUrl('image'))->targetRelativeUrl(),
                ])->title('Изображение'),
            ]),
        ];
    }

    /**
     * @param \App\Models\PostCategory $category
     * @param \App\Http\Requests\Admin\PostCategoryRequest $request
     */
    public function save(PostCategory $category, PostCategoryRequest $request)
    {
        $category->fill($request->category)->save();
        $category->page()->firstOrNew()->fill($request->page)->save();
        $this->saveMedia($category, $request->media);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param \App\Models\PostCategory $category
     * @param array $media
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\MediaCannotBeDeleted
     */
    private function saveMedia(PostCategory $category, array $media)
    {
        foreach ($media as $collection => $image) {
            if (Str::startsWith($image, ['http://', 'https://']))
                continue;
            if ($category->getFirstMedia($collection)->id ?? false)
                $category->deleteMedia($category->getFirstMedia($collection)->id);
            if ($image)
                $category->addMedia(public_path($image))->preservingOriginal()->toMediaCollection($collection);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        PostCategory::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.post_categories'));
    }
}
