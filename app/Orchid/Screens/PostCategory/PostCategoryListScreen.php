<?php

namespace App\Orchid\Screens\PostCategory;

use App\Models\PostCategory;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PostCategoryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Категории публикаций';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'categories' => PostCategory::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.post_categories.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('categories', [

                TD::make('name', 'Название')->render(function (PostCategory $category) {
                    return Link::make($category->name)->route('platform.post_categories.edit', $category);
                })->sort()->filter(TD::FILTER_TEXT),

                TD::make('slug', 'Ссылка')->render(function (PostCategory $category) {
                    return Link::make($category->slug)->route('page.posts.category', $category->slug)
                        ->target('_blank');
                })->sort()->filter(TD::FILTER_TEXT),

                TD::make('created_at', 'Добавлена')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (PostCategory $category) {
                        return $category->created_at ? $category->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (PostCategory $category) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.post_categories.edit', $category)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $category->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $category = PostCategory::find($id);
        $category->delete();
        Toast::success('Успешно удалено!');
    }
}
