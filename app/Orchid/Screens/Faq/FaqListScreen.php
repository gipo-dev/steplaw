<?php

namespace App\Orchid\Screens\Faq;

use App\Models\Faq;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FaqListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Вопрос-ответ';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'faqs' => Faq::defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.faqs.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('faqs', [
                TD::make('faq.title', 'Вопрос')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Faq $faq) {
                        return Link::make($faq->title)->route('platform.faqs.edit', $faq)
                            ->style('max-width: 20vw;display: inline-block;white-space: normal;');
                    }),
                TD::make('faq.services', 'Услуги')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Faq $faq) {
                        return $faq->services->pluck('name')->implode(', ');
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Faq $faq) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.faqs.edit', $faq)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $faq->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Faq::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
