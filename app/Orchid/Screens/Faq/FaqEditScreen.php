<?php

namespace App\Orchid\Screens\Faq;

use App\Http\Requests\Admin\FaqRequest;
use App\Models\Faq;
use App\Models\FaqTag;
use App\Models\Service;
use App\Orchid\Layouts\EditScreenButtons;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FaqEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование вопроса';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Thank
     */
    private Faq $faq;

    /**
     * Query data.
     *
     * @param \App\Models\Faq $faq
     * @return array
     */
    public function query(Faq $faq): array
    {
        $this->faq = $faq;

        if (!$this->faq->exists)
            $this->name = 'Новый вопрос';

        return [
            'faq' => $faq,
            'services' => $faq->services,
            'tags' => $faq->tags,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return array_merge([
            Link::make('Настройка тегов')
                ->icon('arrow-right-circle')
                ->type(Color::PRIMARY())
                ->route('platform.faqtags')
                ->target('_blank')
        ], EditScreenButtons::defaultButtons($this->faq->id));
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('faq.title')->title('Вопрос')->maxlength(255)->required(),
                TextArea::make('faq.text')->title('Ответ')->rows(5)->required(),
                Relation::make('tags')->fromModel(FaqTag::class, 'name')
                    ->multiple()->title('Теги'),
                Relation::make('services')->fromModel(Service::class, 'name')
                    ->multiple()->title('Услуги'),
            ]),
        ];
    }

    /**
     * @param \App\Models\Faq $faq
     * @param \App\Http\Requests\Admin\FaqRequest $request
     */
    public function save(Faq $faq, FaqRequest $request)
    {
        $faq->fill($request->faq)->save();
        $faq->services()->sync($request->services);
        $faq->tags()->sync($request->tags);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Faq::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.faqs'));
    }
}
