<?php

namespace App\Orchid\Screens\Page;

use App\Models\InnerPage;
use App\Models\Page;
use App\Models\PageBlock;
use App\Orchid\Filters\PageTypeFilter;
use App\Orchid\Layouts\PageFilters;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Selection;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PageListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Страницы';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Радактирование страниц сайта';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'pages' => InnerPage::withoutGlobalScope('type')
                ->whereIn('pageable_type', [InnerPage::class, PageBlock::class])
                ->filters()->filtersApplySelection(PageFilters::class)->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить блок')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.pages.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
//            PageFilters::class,
            Layout::table('pages', [

                TD::make('inner_name', 'Название')->render(function (Page $page) {
                    return Link::make($page->inner_name)->route('platform.pages.edit', $page);
                }),

                TD::make('meta_title', 'Заголовок')
                    ->sort()->filter(TD::FILTER_TEXT),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Page $page) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.pages.edit', $page)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->hidden($page->isInner())
                                    ->parameters([
                                        'id' => $page->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        /** @var Page $page */
        $page = Page::find($id);

        if (!$page->isDeletable()) {
            Toast::error('Внутренние страницы нельзя удалять!');
            return;
        }

        $page->delete();
        Toast::success('Успешно удалено!');
    }
}
