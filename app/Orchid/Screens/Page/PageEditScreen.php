<?php

namespace App\Orchid\Screens\Page;

use App\Http\Requests\Admin\PageRequest;
use App\Models\Page;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\SaveButtonLayout;
use App\Services\InnerPage\DefaultPageTemplate;
use Illuminate\Support\Facades\Route;
use Orchid\Screen\Fields\Select;
use OrchidCommunity\TinyMCE\TinyMCE;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PageEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование страницы';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Page
     */
    private Page $page;

    /**
     * @var \App\Services\InnerPage\AbstractPageTemplate
     */
    private $template;

    /**
     * Query data.
     *
     * @param \App\Models\Page $page
     * @return array
     */
    public function query(Page $page): array
    {
        $this->page = $page;
        $this->template = $page->getPageTemplate();

        if (!$this->page->exists)
            $this->name = 'Новая страница';

        return [
            'page' => $page,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        if (Route::has($this->page->slug ?? ''))
            return EditScreenButtons::defaultButtons($this->page->isDeletable() ? $this->page->id : null, route($this->page->slug));
        return EditScreenButtons::defaultButtons();
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return array_merge([
            Layout::rows([
                Group::make([
                    Input::make('page')->readonly()->value(function (Page $page) {
                        return $page->inner_name;
                    })->title('Страница'),
                    TextArea::make('page.meta_title')->rows(3)
                        ->maxlength(255)->title('Мета-заголовок (title)')->required(),
                    TextArea::make('page.meta_description')->rows(3)
                        ->maxlength(255)->title('Мета-описание (description)')->required(),
                ]),
            ])->title('Описание')
        ], $this->template->getLayout($this->page), [SaveButtonLayout::class]);
    }

    /**
     * @param \App\Models\Page $page
     * @param \App\Http\Requests\Admin\PageRequest $request
     */
    public function save(Page $page, PageRequest $request)
    {
        $page->fill($request->get('page'))->save();
        Toast::success('Данные успешно сохранены');
    }
}
