<?php

namespace App\Orchid\Screens\Page;

use App\Http\Requests\Admin\PageRequest;
use App\Models\Page;
use App\Models\PageBlock;
use App\Models\Service;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\SaveButtonLayout;
use App\Services\InnerPage\DefaultPageTemplate;
use App\Services\InnerPage\ServiceRecommendationsTemplate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use OrchidCommunity\TinyMCE\TinyMCE;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class BlockEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование блока';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Page
     */
    private Page $page;

    /**
     * @var \App\Services\InnerPage\AbstractPageTemplate
     */
    private $template;

    /**
     * Query data.
     *
     * @param \App\Models\Page $page
     * @return array
     */
    public function query(Page $page): array
    {
        $this->page = $page;
        $this->template = new ServiceRecommendationsTemplate();

        if (!$this->page->exists)
            $this->name = 'Новый блок';

        return [
            'page' => $page,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        if (Route::has($this->page->slug ?? ''))
            return EditScreenButtons::defaultButtons($this->page->isDeletable() ? $this->page->id : null, route($this->page->slug));
        return EditScreenButtons::defaultButtons($this->page->isDeletable() ? $this->page->id : null);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return array_merge([
            Layout::rows([
                Group::make([
                    Input::make('page')->readonly()->value(function (Page $page) {
                        return $page->inner_name;
                    })->hidden(),
                    TextArea::make('page.meta_title')->rows(3)
                        ->maxlength(255)->hidden(),
                    TextArea::make('page.meta_description')->rows(3)
                        ->maxlength(255)->hidden(),
                ]),
            ])
        ], $this->template->getLayout($this->page), [SaveButtonLayout::class]);
    }

    /**
     * @param \App\Models\Page $page
     * @param \App\Http\Requests\Admin\PageRequest $request
     */
    public function save(Page $page, Request $request)
    {
        $service = Service::find($request->get('service'));

        $page->fill($request->get('page'));

        $page->meta_title = "Блок для страницы '$service->name'";
        $page->meta_description = '';
        $page->template = ServiceRecommendationsTemplate::class;
        $page->pageable_type = PageBlock::class;
        $page->pageable_id = $request->get('service');

        $page->save();

        Toast::success('Данные успешно сохранены');
    }
}
