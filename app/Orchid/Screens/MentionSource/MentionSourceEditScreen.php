<?php

namespace App\Orchid\Screens\MentionSource;

use App\Http\Requests\Admin\MentionSourceRequest;
use App\Models\MentionSource;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Support\Str;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class MentionSourceEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование источника';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\MentionSource
     */
    private MentionSource $mentionSource;

    /**
     * Query data.
     *
     * @param \App\Models\MentionSource $source
     * @return array
     */
    public function query(MentionSource $mentionSource): array
    {
        $this->mentionSource = $mentionSource;

        if (!$this->mentionSource->exists)
            $this->name = 'Добавление источника';

        return [
            'source' => $mentionSource,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->mentionSource->id);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('source.name')->title('Название')->maxlength(255)->required(),
                    Picture::make('media.image')->title('Изображение')->required()
                        ->value($this->mentionSource->getFirstMediaUrl('image'))->targetRelativeUrl(),
                ]),
            ]),
        ];
    }

    /**
     * @param \App\Models\MentionSource $source
     * @param \App\Http\Requests\Admin\MentionSourceRequest $request
     */
    public function save(MentionSource $source, MentionSourceRequest $request)
    {
        $source->fill($request->get('source'))->save();
        $this->saveMedia($source, $request->get('media'));
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param \App\Models\MentionSource $source
     * @param array $media
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\MediaCannotBeDeleted
     */
    private function saveMedia(MentionSource $source, array $media)
    {
        foreach ($media as $collection => $image) {
            if (Str::startsWith($image, ['http://', 'https://']))
                continue;
            if ($mediaItem = ($source->getFirstMedia($collection) ?? false))
                $source->deleteMedia($mediaItem);
            if ($image)
                $source->addMedia(public_path($image))->preservingOriginal()->toMediaCollection($collection);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        MentionSource::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.mention_sources'));
    }
}
