<?php

namespace App\Orchid\Screens\MentionSource;

use App\Models\MentionSource;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class MentionSourceListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Источники СМИ';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'sources' => MentionSource::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.mention_sources.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('sources', [
                TD::make('name', 'Имя')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (MentionSource $source) {
                        return Link::make($source->name)->route('platform.mention_sources.edit', $source);
                    }),
                TD::make('created_at', 'Добавлен')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (MentionSource $source) {
                        return $source->created_at ? $source->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (MentionSource $source) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.mention_sources.edit', $source)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $source->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        MentionSource::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
