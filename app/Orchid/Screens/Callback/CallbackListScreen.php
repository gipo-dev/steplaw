<?php

namespace App\Orchid\Screens\Callback;

use App\Models\Callback;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CallbackListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Запросы';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $callbacks = Callback::filters()->defaultSort('id', 'desc');
        if (!request()->spam)
            $callbacks = $callbacks->where('status_id', '!=', Callback::STATUS_SPAM);
        return [
            'callbacks' => $callbacks->paginate(20),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Показать спам')->href('?spam=1')->icon('ghost')->type(Color::DANGER())->hidden(request()->spam == 1),
            Link::make('Скрыть спам')->route('platform.callbacks')->icon('ghost')->type(Color::SUCCESS())->hidden(!request()->spam),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('callbacks', [

                TD::make('status_id', 'Телефон')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Callback $callback) {
                        if (in_array($callback->status_id, [Callback::STATUS_NEW]))
                            $type = Color::SUCCESS();
                        else if (in_array($callback->status_id, [Callback::STATUS_PROCESS]))
                            $type = Color::WARNING();
                        else if (in_array($callback->status_id, [Callback::STATUS_SPAM]))
                            $type = Color::DANGER();
                        else if (in_array($callback->status_id, [Callback::STATUS_PROCESSED]))
                            $type = Color::PRIMARY();
                        else
                            $type = Color::LIGHT();
                        return Link::make($callback->status_name)->route('platform.callbacks.edit', $callback)->type($type);
                    }),
                TD::make('phone', 'Телефон')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Callback $callback) {
                        return Link::make($callback->phone)->route('platform.callbacks.edit', $callback);
                    }),
                TD::make('name', 'Имя')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Callback $callback) {
                        return Link::make($callback->name)->route('platform.callbacks.edit', $callback);
                    }),
                TD::make('service_id', 'Услуга')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Callback $callback) {
                        if ($callback->service)
                            return Link::make($callback->service->name ?? '')->route('platform.services.edit', $callback->service);
                        return '';
                    }),
                TD::make('created_at', 'Добавлен')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (Callback $callback) {
                        return $callback->created_at ? $callback->created_at->format('d.m.Y в H:i') : '';
                    }),


                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Callback $callback) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make('Просмотреть')
                                    ->route('platform.callbacks.edit', $callback)
                                    ->type(Color::WARNING())
                                    ->icon('eye'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $callback->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Callback::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
