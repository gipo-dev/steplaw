<?php

namespace App\Orchid\Screens\Callback;

use App\Http\Requests\Admin\CallbackRequest;
use App\Models\Callback;
use App\Models\Service;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class CallbackViewScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Запрос обратного звонка';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Callback
     */
    private Callback $callback;

    /**
     * Query data.
     *
     * @param \App\Models\Callback $callback
     * @return array
     */
    public function query(Callback $callback): array
    {
        $this->callback = $callback;

        $this->description = 'Запрос создан ' . $callback->created_at->format('d.m.Y H:i');

        return [
            'callback' => $callback,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Удалить')
                ->icon('trash')
                ->confirm('Подтверждаете удаление?')
                ->method('delete')
                ->type(Color::DANGER())
                ->parameters([
                    'id' => $this->callback->id,
                ]),

            Button::make('Сохранить')
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('callback.name')->title('Имя')->required(),
                    Input::make('callback.phone')->title('Телефон')->required(),
                    Relation::make('callback.service_id')
                        ->fromModel(Service::class, 'name')
                        ->title('Услуга'),
                ]),
                TextArea::make('callback.message')->rows(5)->title('Сообщение от пользователя'),
            ]),
            Layout::rows([
                Group::make([
                    Select::make('callback.status_id')
                        ->options(__('loops.callback.status'))
                        ->title('Статус')->required(),
                    TextArea::make('callback.admin_comment')
                        ->rows(8)->title('Комментарий администратора'),
                ]),
            ])->title('Администрирование'),
        ];
    }

    /**
     * @param \App\Models\Callback $callback
     * @param \App\Http\Requests\Admin\CallbackRequest $request
     */
    public function save(Callback $callback, CallbackRequest $request)
    {
        $callback->fill($request->get('callback'))->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Callback::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.callbacks'));
    }
}
