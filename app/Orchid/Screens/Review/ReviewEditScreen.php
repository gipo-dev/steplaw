<?php

namespace App\Orchid\Screens\Review;

use App\Http\Requests\Admin\ReviewRequest;
use App\Models\Person;
use App\Models\Review;
use App\Models\Service;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Support\Str;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ReviewEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование отзыва';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';
    /**
     * @var \App\Models\Review
     */
    private Review $review;

    /**
     * Query data.
     *
     * @param \App\Models\Review $review
     * @return array
     */
    public function query(Review $review): array
    {
        $this->review = $review;

        if (!$this->review->exists)
            $this->name = 'Добавление отзыва';

        return [
            'review' => $review,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->review->id);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('review.name')->title('Название (внутреннее)')->required(),
                    Relation::make('review.service_id')
                        ->fromModel(Service::class, 'name')->title('Услуга'),
                    Relation::make('review.person_id')
                        ->fromModel(Person::class, 'name')->title('Адвокат'),
                ]),
                Picture::make('media.image')->title('Фото отзыва')->required()
                    ->value($this->review->getFirstMediaUrl('image'))->targetRelativeUrl(),
            ])->title('Данные'),
        ];
    }

    /**
     * @param \App\Models\Review $review
     * @param \App\Http\Requests\Admin\ReviewRequest $request
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\MediaCannotBeDeleted
     */
    public function save(Review $review, ReviewRequest $request)
    {
        $review->fill($request->review)->save();
        $this->saveMedia($review, $request->media);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Review::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.reviews'));
    }

    /**
     * @param \App\Models\Person $person
     * @param array $media
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\MediaCannotBeDeleted
     */
    private function saveMedia(Review $review, array $media)
    {
        foreach ($media as $collection => $image) {
            if (Str::startsWith($image, ['http://', 'https://']))
                continue;
            if ($review->getFirstMedia($collection)->id ?? false)
                $review->deleteMedia($review->getFirstMedia($collection)->id);
            $review->addMedia(public_path($image))->preservingOriginal()->toMediaCollection($collection);
        }
    }
}
