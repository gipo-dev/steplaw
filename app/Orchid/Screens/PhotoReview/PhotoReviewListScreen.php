<?php

namespace App\Orchid\Screens\PhotoReview;

use App\Models\PhotoReview;
use App\Models\Review;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PhotoReviewListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Фотоотзывы';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'reviews' => PhotoReview::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.photoreviews.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('reviews', [
                TD::make('name', 'Фото')
                    ->render(function (PhotoReview $review) {
                        return view('orchid.fields.image_group', [
                            'link' => route('platform.photoreviews.edit', $review),
                            'images' => [$review->getFirstMediaUrl('image') => ''],
                        ]);
                    })->width(130),

                TD::make('name', 'Название')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (PhotoReview $review) {
                        return Link::make($review->name)
                            ->route('platform.photoreviews.edit', $review);
                    }),

                TD::make('service', 'Услуга')
                    ->render(function (PhotoReview $review) {
                        if ($review->service) {
                            return Link::make($review->service->name ?? '')
                                ->route('platform.services.edit', $review->service);
                        }
                    }),

                TD::make('created_at', 'Добавлен')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (PhotoReview $review) {
                        return $review->created_at ? $review->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (PhotoReview $review) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.photoreviews.edit', $review)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $review->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        PhotoReview::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
