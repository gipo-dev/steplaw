<?php

namespace App\Orchid\Screens\Setting;

use App\Http\Requests\Admin\SettingsRequest;
use App\Models\Setting;
use Illuminate\Http\Request;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class SettingScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Настройки';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Настройка сайта';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'settings' => Setting::all()->pluck('value', 'name'),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Сохранить')
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('settings.phone_1')->title('Телефон 1')->max(255)->required(),
                    Input::make('settings.phone_2')->title('Телефон 2')->max(255)->required(),
                    Input::make('settings.email')->title('E-mail')->max(255)->required(),
                ]),
                Group::make([
                    Input::make('settings.telegram')->title('Telegram')->max(255)->required(),
                    Input::make('settings.whatsapp')->title('Whatsapp')->max(255)->required(),
                    Input::make('settings.work_time')->title('Часы работы')->max(255)->required(),
                ]),
                Group::make([
                    TextArea::make('settings.address')->title('Адрес')->max(255)->required(),
//                    TextArea::make('settings.subway')->title('Метро')->max(255)->required(),
                    TextArea::make('settings.duty_lawyer')->title('Дежурный адвокат')->max(255)->required(),
                ]),
                Group::make([
                    TextArea::make('settings.requisites')->rows(5)->title('Реквизиты бюро')->max(255)->required(),
                ]),
            ]),
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function save(SettingsRequest $request)
    {
        foreach ($request->settings as $name => $value) {
            Setting::updateOrCreate(['name' => $name], ['value' => $value]);
        }
        Toast::success('Настройки успешно сохранены');
    }
}
