<?php

namespace App\Orchid\Screens\Service;

use App\Models\Service;
use App\Orchid\Filters\NameFilter;
use App\Orchid\Layouts\Service\ServiceFilterLayout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Orchid\Icons\IconFinder;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class ServiceListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Услуги';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'services' => Service::filters()->filtersApplySelection(ServiceFilterLayout::class)
                ->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Вопрос-ответ')
                ->icon('question')->type(Color::PRIMARY())
                ->route('platform.faqs'),

            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.services.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            ServiceFilterLayout::class,

            Layout::table('services', [
                TD::make('name', 'Название')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Service $service) {
                        return implode(' ', [
                            Link::make($service->name)->route('platform.services.edit', $service)
                                ->style('width: 15vw;display: inline-block;white-space: normal;'),
                            $service->status_id === 0 ? '<span class="badge btn-danger">Скрыто</span>' : '',
                        ]);
                    }),
                TD::make('slug', 'Ссылка')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Service $service) {
                        return Link::make($service->slug)
                            ->href($service->getLink())->target('_blank')
                            ->style('max-width: 20vw;display: inline-block;white-space: normal;');;
                    }),
                TD::make('service_category_id', 'Категория/родитель')
                    ->sort()
                    ->render(function (Service $service) {
                        if ($service->parent)
                            return Link::make($service->parent->name ?? '')
                                ->route('platform.services.edit', $service->parent)
                                ->style('max-width: 15vw;display: inline-block;white-space: normal;');
                        else if ($service->category)
                            return Link::make($service->category->name)
                                ->route('platform.service_categories.edit', $service)
                                ->style('max-width: 20vw;display: inline-block;white-space: normal;');;
                    }),
                TD::make('created_at', 'Добавлена')
                    ->sort()
                    ->filter(TD::FILTER_TEXT)
                    ->render(function (Service $service) {
                        return $service->created_at ? $service->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Service $service) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.services.edit', $service)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $service->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        Service::find($id)->delete();
        Toast::success('Успешно удалено!');
    }
}
