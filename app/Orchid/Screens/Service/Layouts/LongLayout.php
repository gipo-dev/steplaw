<?php

namespace App\Orchid\Screens\Service\Layouts;

use App\Models\Person;
use App\Orchid\Layouts\Service\ServiceQALayout;
use App\Orchid\Layouts\Service\ServiceTabLayout;
use Nakukryskin\OrchidRepeaterField\Fields\Repeater;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;
use OrchidCommunity\TinyMCE\TinyMCE;
use stdClass;

class LongLayout
{
    public function isValid($layout)
    {
        return $layout == 'long';
    }

    public function addRows(&$rows, &$service)
    {
        /** Заголовок услуги */
        $rows[] = Layout::rows([
            Input::make('page.body.long.header.title')
                ->title('Заголовок услуги')->required(),
            Relation::make('page.body.long.header.person')
                ->fromModel(Person::class, 'name')
                ->placeholder('Без фото')
                ->title('Фото адвоката'),
            TinyMCE::make('page.body.long.header.text')
                ->value($service->page->body['old']['result']['text_1'] ?? '')
                ->title('Текст внутри блока заголовка (пусто = по умолчанию)'),
        ]);

        /** Какими семейными делами мы занимаемся? */
        $rows[] = Layout::columns([
            Layout::rows([
                Input::make('page.body.long.block2.title')
                    ->title('Заголовок блока')->required(),
                TinyMCE::make('page.body.long.block2.text'),
            ])->title('Блок 2'),
            Layout::rows([
                Input::make('page.body.long.spec.title')
                    ->title('Заголовок блока')->required(),
                TextArea::make('page.body.long.spec.items')->rows(8)
                    ->title('Каждый пункт с новой строки')->required(),
            ])->title('Специализация'),
        ]);

        /** Мы занимаемся семейными делами более 13 лет */
        $rows[] = Layout::rows([
            Input::make('page.body.long.features.title')
                ->title('Заголовок блока преимуществ')->required(),
        ])->title('Блок преимущества');

        /** Полезно знать */
        $block3 = $service->page->body['long']['block3'] ?? [];
        if (count($block3) === 0) {
            $block3 = [
                [
                    'title' => '',
                    'content' => '',
                ],
                [
                    'title' => __('pages.service.long.content.links.#page-content-block-text-2'),
                    'content' => __('pages.service.long.content.default.features'),
                ],
                [
                    'title' => __('pages.service.long.content.links.#page-content-block-text-3'),
                    'content' => __('pages.service.long.content.default.statistics'),
                ],
            ];
        }

        $rows[] = Layout::rows([
            Input::make('page.body.long.block3_title')
                ->title('Заголовок основного текстового блока')->required(),
            Repeater::make('page.body.long.block3')
                ->layout(ServiceTabLayout::class)
                ->value($block3)
                ->title('Вкладка')->min(1)
                ->button_label('Добавить вкладку'),
        ])->title('Блок 3 (основной текстовый)');

        /** Вопрос-ответ */
        $rows[] = Layout::rows([
            Repeater::make('page.body.long.qa')
                ->layout(ServiceQALayout::class)
                ->title('Вопрос')
                ->button_label('Добавить вопрос'),
        ])->title('Вопрос-ответ');

        /** Отзывы - семейные дела */
        $rows[] = Layout::rows([
            Input::make('page.body.long.reviews.title')
                ->title('Заголовок блока отзывы')->required(),
        ])->title('Блок отзывы');

        /** Как выбрать хорошего адвоката по семейным спорам? */
        $rows[] = Layout::rows([
            Input::make('page.body.long.recommendations.title')
                ->title('Заголовок блока рекомендации')->required(),
            TinyMCE::make('page.body.long.recommendations.text')
                ->title('Текст внутри блока "Важно"'),
        ])->title('Блок рекомендации');

    }

    private function getPersonsOptions()
    {
        $options = new stdClass;
        foreach (Person::get() as $k => $v) {
            $options->{(string)$v->id} = $v->name;
        }
        return (array)$options;
    }

}
