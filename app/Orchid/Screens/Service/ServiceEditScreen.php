<?php

namespace App\Orchid\Screens\Service;

use App\Http\Requests\Admin\ServiceRequest;
use App\Models\Mention;
use App\Models\Person;
use App\Models\Price;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\WorkCase;
use App\Orchid\Fields\ServiceTypeUpdaterField;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\SaveButtonLayout;
use App\Orchid\Layouts\Service\ServiceFieldsLayout;
use App\Orchid\Screens\Service\Layouts\LongLayout;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use OrchidCommunity\TinyMCE\TinyMCE;

class ServiceEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование услуги';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Service
     */
    private Service $service;

    /**
     * Query data.
     *
     * @param \App\Models\Service $service
     * @return array
     */
    public function query(Service $service): array
    {
        $this->service = $service;

        if (!$service->exists)
            $this->name = 'Добавление услуги';

        return [
            'service' => $service,
            'page' => $service->page,
            'prices' => $service->prices,
            'cases' => $service->cases,
            'mentions' => $service->mentions,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->service->id,
            $this->service->exists ? $this->service->getLink() : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        $rows = [
            ServiceFieldsLayout::class,

            Layout::columns([

                Layout::rows([

                    ServiceTypeUpdaterField::make(''),

                    TextArea::make('service.description')->title('Краткое описание')
                        ->maxlength(255)->rows(5),

                    Input::make('service.tags')->title('Теги')->maxlength(255)->required(),

                    Input::make('service.victories')->title('Дел выиграно')
                        ->type('number')->max(5000)->required(),

                    Relation::make('cases')->fromModel(WorkCase::class, 'name')
                        ->applyScope('service')->multiple()->title('Дела'),

                    Relation::make('mentions')->fromModel(Mention::class, 'text')
                        ->multiple()->title('Упоминания в СМИ'),

                    Group::make([

                        CheckBox::make('page.body.options.show_reviews_category')
                            ->placeholder('Отображать категорию в отзывах')->sendTrueOrFalse(),

                        CheckBox::make('page.body.options.show_positions')
                            ->placeholder('Отображать должности адвокатов')->sendTrueOrFalse(),

                    ]),

                ])->title('Дополнительные поля'),

                Layout::rows([
                    Textarea::make('page.meta_title')
                        ->maxlength(255)->rows(3)
                        ->title('Мета заголовок (title)')->required(),
                    TextArea::make('page.meta_description')
                        ->maxlength(400)
                        ->title('Мета описание (description)')->rows(5)->required(),
                    Input::make('page.body.advantages.title')->title('Заголовок достижений')->required(),
                ])->title('Мета-описание'),

            ]),

            Layout::rows([
                Select::make('page.body.template.type')->options([
                    "new" => 'Шаблон 1',
                    "old" => 'Шаблон 2',
                    "long" => 'Шаблон 3',
                ])->value($this->service->page->body['template']['type'] ?? '')->title('Шаблон')->required(),
            ]),
        ];

        foreach ($this->service->getPageTemplate() as $i => $field) {
            if (!is_int($i))
                continue;

            $row = [
                Input::make("page.body.$i.title")->value($field['title'] ?? ''),
            ];

            if ($i == 0) {
                $row[] = TinyMCE::make("page.body.101.text")
                    ->title('Текст внутри блока заголовка (пусто = по умолчанию)')
                    ->value($this->service->page['body'][101]['text'] ?? '');
            }

            $row = array_merge($row, [
                TinyMCE::make("page.body.$i.text")->value($field['text'] ?? ''),
                Button::make('Сохранить')
                    ->icon('check')
                    ->type(Color::SUCCESS())
                    ->method('save')
                    ->right(),
            ]);

            $rows[] = Layout::rows($row);
        }

        foreach ([
                     '0' => [
                         'title' => $this->service->page->body['old']['service']['text']['0']['title'] ?? '',
                         'text' => $this->service->page->body['old']['service']['text']['0']['text'] ?? '',
                     ],
                     '1' => [
                         'title' => $this->service->page->body['old']['service']['text']['1']['title'] ?? '',
                         'text' => $this->service->page->body['old']['service']['text']['1']['text'] ?? '',
                     ]
                 ] as $i => $field) {

            $row = [
                Input::make("page.body.old.service.text.$i.title")->value($field['title'] ?? ''),
            ];

            if ($i == '0') {
                $row[] = TinyMCE::make("page.body.old.header.comment")
                    ->title('Текст внутри блока заголовка (пусто = по умолчанию)')
                    ->value($this->service->page['body']['old']['header']['comment'] ?? '');
            }

            $row = array_merge($row, [
                TinyMCE::make("page.body.old.service.text.$i.text")->value($field['text'] ?? ''),
                Button::make('Сохранить')
                    ->icon('check')
                    ->type(Color::SUCCESS())
                    ->method('save')
                    ->right(),
            ]);

            $rows[] = Layout::rows($row);
        }

        $rows[] = Layout::columns([
            Layout::rows([
                Relation::make('page.body.old.person.id')->fromModel(Person::class, 'name')
                    ->value($this->service->page->body['old']['person']['id'] ?? '')
                    ->title('Цитируемый адвокат'),
                TextArea::make('page.body.old.person.text')
                    ->value($this->service->page->body['old']['person']['text'] ?? '')
                    ->rows(10)->title('Текст цитаты'),
            ])->title('Цитата адвоката'),
            Layout::rows([
                TextArea::make('page.body.old.result.text_1')
                    ->value($this->service->page->body['old']['result']['text_1'] ?? '')
                    ->rows(6)->title('Результат 1'),
                TextArea::make('page.body.old.result.text_2')
                    ->value($this->service->page->body['old']['result']['text_2'] ?? '')
                    ->rows(6)->title('Результат 2'),
            ])->title('Результат'),
        ]);

        foreach ([
                     LongLayout::class,
                 ] as $layout) {

            $layout = (new $layout);

//            if ($layout->isValid($this->service->page->body['template']['type'] ?? 'new')) {
            $layout->addRows($rows, $this->service);
//            }
        }

        $rows[] = Layout::rows([
            TinyMCE::make("page.body.qa.text")->value($this->service->page->body['qa']['text'] ?? '')
                ->title('Вопрос-ответ')->help("Заполняется в формате Q: A:"),

            Button::make('Сохранить')
                ->icon('check')
                ->type(Color::SUCCESS())
                ->method('save')
                ->right(),

            Group::make([
                TinyMCE::make('prices.phys')->value($this->service->getPricesTable(1))->title('Для физ. лиц'),
                TinyMCE::make('prices.jur')->value($this->service->getPricesTable(0))->title('Для юр. лиц'),
            ]),
            TinyMCE::make('page.body.after_price.text')
                ->value($this->service->page->body['after_price']['text'] ?? '')->title('Текст после блока цены'),
        ])->title('Цены и вопросы');

        $rows[] = SaveButtonLayout::class;

//        dd($rows);

        return $rows;
    }

    /**
     * @param \App\Models\Service $service
     * @param \App\Http\Requests\Admin\ServiceRequest $request
     */
    public function save(Service $service, ServiceRequest $request)
    {
        $service->fill($request->service)->save();
        $service->page()->firstOrNew()->fill($request->page)->save();
        $service->cases()->sync($request->cases);
        $service->mentions()->sync($request->mentions);
        $service->casePages()->sync($request->case_pages);
        $service->prices()->delete();
        $service->prices()->saveMany(collect($request->prices)->map(function ($item) {
            return new Price($item);
        }));
        if (count($request->childs ?? []) > 0) {
            Service::where('parent_id', $service->id)->update(['parent_id' => null]);
            Service::where('id', '!=', $service->id)
                ->whereIn('id', $request->childs)->update(['parent_id' => $service->id]);
        }

        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Service::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.services'));
    }
}
