<?php

namespace App\Orchid\Screens\Video;

use App\Http\Requests\Admin\VideoRequest;
use App\Models\Video;
use App\Orchid\Layouts\EditScreenButtons;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use OrchidCommunity\TinyMCE\TinyMCE;

class VideoEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование видео';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Video
     */
    private Video $video;

    /**
     * Query data.
     *
     * @param \App\Models\Video $video
     * @return array
     */
    public function query(Video $video): array
    {
        $this->video = $video;

        if (!$this->video->exists)
            $this->name = 'Добавление видео';

        return [
            'video' => $video,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->video->id);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('video.title')->title('Заголовок видео')->required(),
                TextArea::make('video.src')->title('Ссылка на видео')
                    ->rows(3)->required()
                    ->help('Вставляйте код видео из "Поделиться -> Встроить", система сама выберет нужную ссылку'),
                TinyMCE::make('video.text')->title('Описание')
                    ->required()->theme('Silver'),
            ]),
        ];
    }

    /**
     * @param \App\Models\Video $video
     * @param \App\Http\Requests\Admin\VideoRequest $request
     */
    public function save(Video $video, VideoRequest $request)
    {
        $video->fill($request->get('video'))->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Video::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.videos'));
    }
}
