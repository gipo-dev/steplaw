<?php

namespace App\Orchid\Screens\CasePage;

use App\Models\CasePage;
use App\Models\WorkCase;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;

class CasePageListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Дела';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'cases' => CasePage::with(['services'])->filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.casepages.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('cases', [
                TD::make('id', 'Название')->render(function ($case) {
                    return Link::make(mb_substr($case->name ?? '', 0, 50) . '...')
                        ->route('platform.casepages.edit', $case);
                }),
                TD::make('', 'Документ')->render(function ($case) {
                    return count($case->files ?? []) > 0 ? 'Есть' : 'Нет';
                }),
                TD::make('', 'Услуга')->render(function ($case) {
                    return '<span style="width: 20vw;display: inline-block;white-space: normal;">'
                        . $case->services->pluck('name')->implode("<br>") . '</span>';
                }),
                TD::make('', 'Добавлен')->render(function ($case) {
                    return $case->created_at->format('d.m.Y в H:i');
                }),
            ]),
        ];
    }
}
