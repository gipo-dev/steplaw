<?php

namespace App\Orchid\Screens\CasePage;

use App\Events\Admin\CasePageSaved;
use App\Http\Requests\CasePageRequest;
use App\Models\CasePage;
use App\Models\Person;
use App\Models\Service;
use App\Models\WorkCase;
use App\Orchid\Fields\BigTitlesField;
use App\Orchid\Layouts\CasePage\ArticleFields;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\PageMetaLayout;
use App\Orchid\Layouts\SaveButtonLayout;
use Nakukryskin\OrchidRepeaterField\Fields\Repeater;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Screen;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use OrchidCommunity\TinyMCE\TinyMCE;

class CasePageEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование дела';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\CasePage
     */
    private CasePage $casePage;

    /**
     * @var array
     */
//    private $articles = [];

    public static $currentCasePage;

    /**
     * Query data.
     *
     * @param \App\Models\CasePage $casePage
     * @return array
     */
    public function query(CasePage $casePage): array
    {
        $this->casePage = $casePage;
        static::$currentCasePage = $casePage;
        event(new CasePageSaved($casePage));
//        foreach ($this->casePage->articles ?? [] as $article)
//            $this->articles[$article] = $article;

        if (!$casePage->exists)
            $this->name = 'Добавление дела';

        return [
            'case' => $casePage,
            'services' => $casePage->services ?? [],
            'page' => $casePage->page,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons(
            $this->casePage->id,
            $this->casePage->exists ? route('page.cases.case', $this->casePage) : null,
        );
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('case.name')->title('Название')
                        ->maxlength(255)->required(),
                    Select::make('case.category_id')->title('Категория')
                        ->options(
                            collect(__('pages.case_page.categories'))->map(function ($category) {
                                return $category['small'] . ' - ' . $category['full'];
                            })->toArray()
                        )->required(),
                ]),
                Group::make([
                    Relation::make('case.person_id')->fromModel(Person::class, 'name')
                        ->title('Адвокат'),
                    Relation::make('services.')->fromModel(Service::class, 'name')
                        ->multiple()->title('Услуги'),
                ]),
            ])->title('Описание дела'),

            Layout::columns([
                Layout::rows([
                    Repeater::make('case.articles')
                        ->layout(ArticleFields::class)
                        ->title('Номер статьи')
                        ->button_label('Добавить статью'),

                    Input::make('case.properties.court')
                        ->title('Суд')->maxlength(255),
                    Group::make([
                        Input::make('case.properties.case_number')
                            ->title('Номер дела')->maxlength(255),
                        DateTimer::make('case.properties.date')->format('d.m.Y')
                            ->title('Дата'),
                    ]),
                    Upload::make('case.files')
                        ->value($this->casePage->files ?? [])
                        ->groups('documents')
                        ->acceptedFiles('.pdf')
                        ->title('Документы'),
                ])->title('Дополнительные поля'),
                PageMetaLayout::class,
            ]),

            Layout::rows([
                TinyMCE::make('page.body.description')
                    ->value($this->casePage->page->body['description'] ?? '')
                    ->title('Описание дела')->class('title_big'),
                TinyMCE::make('page.body.threatening')
                    ->value($this->casePage->page->body['threatening'] ?? '')
                    ->title('Что грозило клиенту')->class('title_big'),
                TinyMCE::make('page.body.result')
                    ->value($this->casePage->page->body['result'] ?? '')
                    ->title('Результат работы')->class('title_big'),
                Group::make([
                    Input::make('page.body.review_name')
                        ->title('Имя клиента')->maxlength(255)->class('form-control title_big'),
                    DateTimer::make('page.body.review_date')
                        ->title('Дата отзыва')->maxlength(255)->class('form-control title_big'),
                ]),
                TinyMCE::make('page.body.review')
                    ->value($this->casePage->page->body['review'] ?? '')
                    ->title('Отзыв клиента')->class('title_big'),
                BigTitlesField::make('big_titles'),
            ]),
            SaveButtonLayout::class,
        ];
    }

    /**
     * @param \App\Models\CasePage $casePage
     * @param \App\Http\Requests\CasePageRequest $request
     * @return void
     */
    public function save(CasePage $casePage, CasePageRequest $request)
    {
        $casePage->fill($request->case)->save();
        $casePage->page()->firstOrNew()->fill($request->page)->save();
        $casePage->services()->sync($request->services);

        event(new CasePageSaved($casePage));
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        CasePage::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.casepages'));
    }
}
