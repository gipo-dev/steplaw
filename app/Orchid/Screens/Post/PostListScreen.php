<?php

namespace App\Orchid\Screens\Post;

use App\Models\Post;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Actions\DropDown;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;
use Orchid\Support\Color;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PostListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Публикации';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'posts' => Post::filters()->defaultSort('id', 'desc')->paginate(30),
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Добавить')
                ->icon('plus')->type(Color::SUCCESS())
                ->route('platform.posts.add'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::table('posts', [

                TD::make('name', 'Заголовок')->render(function (Post $post) {
                    return Link::make($post->name)->route('platform.posts.edit', $post)
                        ->style('max-width: 20vw;display: inline-block;white-space: normal;');
                })->sort()->filter(TD::FILTER_TEXT),

                TD::make('slug', 'Ссылка')->render(function (Post $post) {
                    return Link::make($post->slug)->href($post->getLink())->target('_blank')
                        ->style('max-width: 20vw;display: inline-block;white-space: normal;');
                })->sort()->filter(TD::FILTER_TEXT),

                TD::make('', 'Категории')->render(function (Post $post) {
                    $links = [];
                    foreach ($post->categories as $category) {
                        $links[] = Link::make($category->name)
                            ->route('platform.post_categories.edit', $category->id)
                            ->target('_blank');
                    }
                    return implode(', ', $links);
                })->sort(),

                TD::make('created_at', 'Добавлен')
                    ->sort()
                    ->filter(TD::FILTER_DATE)
                    ->render(function (Post $post) {
                        return $post->created_at ? $post->created_at->format('d.m.Y в H:i') : '';
                    }),

                TD::make('Действия')
                    ->align(TD::ALIGN_CENTER)
                    ->width('100px')
                    ->render(function (Post $post) {
                        return DropDown::make()
                            ->icon('options-vertical')
                            ->list([

                                Link::make(__('Edit'))
                                    ->route('platform.posts.edit', $post)
                                    ->type(Color::PRIMARY())
                                    ->icon('pencil'),

                                Button::make('Удалить')
                                    ->icon('trash')
                                    ->type(Color::DANGER())
                                    ->method('delete')
                                    ->confirm('Подтверждаете удаление?')
                                    ->parameters([
                                        'id' => $post->id,
                                    ]),
                            ]);
                    }),
            ]),
        ];
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        $post = Post::find($id);
        $post->delete();
        Toast::success('Успешно удалено!');
    }
}
