<?php

namespace App\Orchid\Screens\Post;

use App\Http\Requests\Admin\PostRequest;
use App\Models\Person;
use App\Models\Post;
use App\Models\PostCategory;
use App\Models\Tag;
use App\Orchid\Fields\EditorJS;
use App\Orchid\Layouts\DefaultEditorJSLayout;
use App\Orchid\Layouts\EditScreenButtons;
use App\Orchid\Layouts\SaveButtonLayout;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Orchid\Screen\Fields\Cropper;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Radio;
use Orchid\Screen\Fields\RadioButtons;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class PostEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Редактирование поста';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Post
     */
    private Post $post;

    /**
     * Query data.
     *
     * @param \App\Models\Post $post
     * @return array
     */
    public function query(Post $post): array
    {
        $this->post = $post;

        if (!$this->post->exists)
            $this->name = 'Добавление поста';

        return [
            'post' => $post,
            'categories' => $post->categories,
            'page' => $post->page,
            'tags' => $post->tags,
            'media' => $post->media,
            'persons' => $post->persons,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->post->id,
            $this->post->exists ? $this->post->getLink() : null);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::columns([
                Layout::rows([
                    Input::make('post.name')->title('Заголовок (H1)')->maxlength(255)->required(),
                    Input::make('post.slug')->title('Ссылка')->maxlength(255)
                        ->popover('Оставьте пустым для автоматической генерации'),
                    Relation::make('categories')->fromModel(PostCategory::class, 'name')
                        ->multiple()->title('Категории')->required(),
                    Select::make('tags')->fromModel(Tag::class, 'name', 'name')
                        ->title('Теги')->multiple()->taggable(),
                    Relation::make('post.author_id')->fromModel(Person::class, 'name')
                        ->title('Автор'),
                    RadioButtons::make('post.show_on_person')->options([
                        0 => 'Выводить на главной',
                        1 => 'Новость адвоката',
                    ])->title('Где выводить')->value(1),
                    Relation::make('persons')->fromModel(Person::class, 'name')
                        ->title('Адвокаты для вывода')->multiple(),
                ]),

                Layout::rows([
                    Select::make('post.status_id')->options(__('loops.post.status'))
                        ->title('Статус')->required(),
                    Input::make('page.meta_title')->title('Мета-заголовок (title)')
                        ->maxlength(255)->required(),
                    TextArea::make('page.meta_description')->rows(4)->maxlength(255)
                        ->title('Мета-описание (description)'),
                    Input::make('post.data.read_time')->title('Время чтения')
                        ->maxlength(255)->help('Пример: 3 мин'),
                ]),
                Layout::rows([
                    Cropper::make('media.image')->title('Изображение')
                        ->width(365)->height(220)
                        ->value($this->post->getFirstMediaUrl('image'))->targetRelativeUrl(),
                ]),
            ]),

            Layout::rows([
                Group::make([
                    TextArea::make('post.data.description')->rows(3)
                        ->maxlength(500)->title('Аннотация')
                        ->popover('Отображается в маленьком посте, перед текстом и в мета-описании, если оно не задано')
                        ->required(),
                    TextArea::make('post.data.comment')->rows(3)
                        ->maxlength(500)->title('Текст перед комментариями'),
                ]),
                EditorJS::make('page.body')
                    ->tools(DefaultEditorJSLayout::class)->title('Страница'),
            ]),

            SaveButtonLayout::class,
        ];
    }

    /**
     * @param \App\Models\Post $post
     * @param \App\Http\Requests\Admin\PostRequest $request
     */
    public function save(Post $post, PostRequest $request)
    {
        $post->fill($request->post)->save();
        $post->page()->firstOrNew()->fill($request->page)->save();
        $post->categories()->sync($request->categories);
        $post->persons()->sync($request->persons);
        $this->saveTags($post, $request->tags);
        $this->saveMedia($post, $request->media);
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        Post::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.posts'));
    }

    /**
     * @param \App\Models\Post $post
     * @param array|null $tags
     */
    private function saveTags(Post $post, ?array $tags)
    {
        if ($tags) {
            $exists = collect([]);
            foreach ($tags as $tag) {
                try {
                    $exists[] = Tag::firstOrCreate([
                        'name' => $tag,
                    ], [
                        'slug' => Str::slug($tag),
                    ]);
                } catch (\Exception $exception) {
                    $exists[] = Tag::create([
                        'name' => $tag,
                        'slug' => Str::slug($tag) . '_' . Str::random(4),
                    ]);
                }
            }
            $post->tags()->sync($exists->pluck('id'));
        } else {
            $post->tags()->delete();
        }
    }

    /**
     * @param \App\Models\Post $post
     * @param array $media
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\MediaCannotBeDeleted
     */
    private function saveMedia(Post $post, array $media)
    {
        foreach ($media as $collection => $image) {
            if (Str::startsWith($image, ['http://', 'https://']))
                continue;
            if ($mediaItem = ($post->getFirstMedia($collection) ?? false))
                $post->deleteMedia($mediaItem);
            if ($image)
                $post->addMedia(public_path($image))->preservingOriginal()->toMediaCollection($collection);
        }
    }
}
