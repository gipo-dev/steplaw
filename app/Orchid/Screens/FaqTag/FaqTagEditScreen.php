<?php

namespace App\Orchid\Screens\FaqTag;

use App\Models\FaqTag;
use App\Orchid\Layouts\EditScreenButtons;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class FaqTagEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Вопрос-ответ: Редактирование тега';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = '';

    /**
     * @var \App\Models\Thank
     */
    private FaqTag $faq;

    /**
     * Query data.
     *
     * @param \App\Models\FaqTag $faq
     * @return array
     */
    public function query(FaqTag $faq): array
    {
        $this->faq = $faq;

        if (!$this->faq->exists)
            $this->name = 'Вопрос-ответ: Новый тег';

        return [
            'faq' => $faq,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return EditScreenButtons::defaultButtons($this->faq->id);
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('faq.name')->title('Имя тега')->maxlength(255)->required(),
            ]),
        ];
    }

    /**
     * @param \App\Models\FaqTag $faq
     * @param \Illuminate\Http\Client\Request $request
     */
    public function save(FaqTag $faq, Request $request)
    {
        $faq->fill($request->faq)->save();
        Toast::success('Данные успешно сохранены');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        FaqTag::find($id)->delete();
        Toast::success('Успешно удалено!');
        return redirect(route('platform.faqtags'));
    }
}
