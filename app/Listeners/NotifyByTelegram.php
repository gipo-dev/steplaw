<?php

namespace App\Listeners;

use App\Events\CallbackLeft;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\Laravel\Facades\Telegram;

class NotifyByTelegram
{
    private $recipients = [
        630513132 => true,
        1343051491 => true,
        165601672 => true,
    ];

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CallbackLeft $event
     * @return void
     */
    public function handle(CallbackLeft $event)
    {
        if (!config('telegram.active'))
            return;
        $callback = $event->callback->refresh();
        if ($callback->status_id == $callback::STATUS_SPAM)
            return;
        foreach ($this->recipients as $recipient => $full) {
            Telegram::sendMessage([
                'chat_id' => $recipient,
                'text' => implode(PHP_EOL, [
                    'Новый запрос',
                    $callback->name,
                    $full ? $callback->phone : '',
                    $callback->message ? 'Сообщение: ' . $callback->message : '',
                    $callback->service ? ($callback->service->name ?? '') : '',
                    $callback->url ? ($callback->url) : '',
                ]),
            ]);
        }
    }
}
