<?php

namespace App\Listeners\Callback;

use App\Events\CallbackLeft;
use App\Models\Callback;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;

class CheckSpam
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CallbackLeft $event
     * @return void
     */
    public function handle(CallbackLeft $event)
    {
        if ($this->checkUrlExistsInAttibutes($event->callback->only(['name', 'phone', 'message']))) {
            $event->callback->update(['status_id' => Callback::STATUS_SPAM]);
            return;
        }
        if (!$this->isEmail($event->callback->phone)) {
            if ($this->isPhoneSpam($event->callback->phone)) {
                $event->callback->update(['status_id' => Callback::STATUS_SPAM]);
            }
        }
    }

    /**
     * @return bool
     */
    public function isPhoneSpam($phone)
    {
        preg_match('/(\+7) \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/', $phone, $found);
        return count($found) < 1;
    }

    /**
     * @param $data
     * @return bool
     */
    private function isEmail($data)
    {
        return filter_var($data, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * @param $data
     * @return bool
     */
    private function checkUrlExistsInAttibutes($data)
    {
        foreach ($data as $datum) {
            if (Str::contains($datum, 'http'))
                return true;
        }
        return false;
    }
}
