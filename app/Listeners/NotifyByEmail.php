<?php

namespace App\Listeners;

use App\Events\CallbackLeft;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyByEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CallbackLeft  $event
     * @return void
     */
    public function handle(CallbackLeft $event)
    {
        //
    }
}
