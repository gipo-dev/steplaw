<?php

namespace App\Services;

use App\Models\FaqTag;
use App\Models\Service;

class QaService
{
    /**
     * @param \App\Models\Service $service
     * @return array
     */
    public function getTags(Service $service)
    {
        $tags = [];
        $service->faqs()->get()->pluck('tags')
            ->each(function ($items) use (&$tags) {
                $items->each(function ($tag) use (&$tags) {
                    $tags[$tag->id] = $tag;
                });
            })->values();
        return $tags;
    }

    /**
     * @param \App\Models\Service $service
     * @param $page
     * @param $limit
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getItems(Service $service, $tag = -1, $page = 1, $limit = 5)
    {
        $faqs = $service->faqs();
        if ($tag != -1) {
            $faqs = $faqs->whereHas('tags', function ($q) use ($tag) {
                $q->where('id', $tag);
            });
        }
        return $faqs->latest()->take($limit)
            ->offset(($page - 1) * $limit)->get();
    }

    /**
     * @param \App\Models\Service $service
     * @return array
     */
    public function getTotal(Service $service)
    {
        return $service->faqs()->count();
    }

    /**
     * @param \App\Models\Service $service
     * @param $tag
     * @param $page
     * @param $limit
     * @return bool
     */
    public function hasMore(Service $service, $tag = -1, $page = 1, $limit = 5) {
        return count($this->getItems($service, $tag, $page, $limit)) > 0;
    }
}
