<?php


namespace App\Services\Media;


use Illuminate\Support\Arr;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\DefaultPathGenerator;

class CustomPathGenerator extends DefaultPathGenerator
{
    protected function getBasePath(Media $media): string
    {
        $path = mb_strtolower(Arr::last(explode('\\', $media->model_type))) . '/' . (($media->created_at ?? now())->format('Y/m/d') . '/' . $media->model->id);
        if ($media->collection_name && $media->collection_name != 'default')
            $path .= '/' . $media->collection_name;
        return $path;
    }
}
