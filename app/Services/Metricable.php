<?php


namespace App\Services;


use App\Models\Metric;
use App\Services\Metric\MetricRepository;

trait Metricable
{
    /**
     * @var MetricRepository
     */
    public $metrics;

    protected static function bootMetricable()
    {
        static::retrieved(function (HasMetricsInterface $model) {
            /** @var \Illuminate\Database\Eloquent\Model|\App\Services\Metricable $model */
            $model->metrics = new MetricRepository($model);
        });
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function metrics(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Metric::class, 'metricable');
    }

    /**
     * @param int $type
     * @param int $count
     */
    public function addMetricValue(int $type, int $count = 1)
    {
        /** @var Metric $metric */
        $metric = $this->metrics()->firstOrNew([
            'type' => $type
        ]);
        $metric->val += $count;
        $metric->save();
    }
}
