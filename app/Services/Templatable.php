<?php

namespace App\Services;

use App\Services\InnerPage\DefaultPageTemplate;
use Orchid\Attachment\Models\Attachment;

trait Templatable
{
    /**
     * @return \App\Services\InnerPage\AbstractPageTemplate
     */
    public function getPageTemplate()
    {
        if ($this->template)
            return new $this->template();
        return new DefaultPageTemplate();
    }

    /**
     * @param $attachments
     * @return Attachment[]|\Illuminate\Support\Collection
     */
    public function getAttachmentsByArray($attachments) {
        return Attachment::whereIn('id', $attachments)->orderBy('sort')->get();
    }
}
