<?php


namespace App\Services;

use App\Models\Setting;

class Settings
{
    /**
     * @var \App\Models\Setting[]|\Illuminate\Database\Eloquent\Collection
     */
    private static $settings;

    /**
     * Settings constructor.
     */
    public function __construct()
    {
        static::$settings = Setting::all()->keyBy('name');
    }

    /**
     * @param $name
     * @return string
     */
    public function get($name): string
    {
        return static::$settings[$name]->value ?? '';
    }

    /**
     * @param $name
     * @return string
     */
    public function getPhoneLink($name): string
    {
        return preg_replace('/[^0-9\+]/', '', $this->get($name));
    }

}
