<?php

namespace App\Services;

use App\Models\PhotoGroup;

class PhotoService
{
    /**
     * @param $count
     * @return \Illuminate\Support\Collection
     */
    public function getPhotos($count = 16)
    {
        $photos = [];

        PhotoGroup::latest()->take(20)
            ->each(function ($photoGroup) use (&$photos, $count) {
                if (count($photos) >= $count)
                    return;

                foreach ($photoGroup->getImages() as $photo => $_)
                    $photos[] = [
                        'photo' => $photo,
                        'title' => $photoGroup->description,
                    ];
            });

        return collect($photos)->take($count);
    }
}
