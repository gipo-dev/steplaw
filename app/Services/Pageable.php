<?php


namespace App\Services;


trait Pageable
{
    /**
     * @return array
     */
    abstract protected function defaultPageTemplate(): array;

    /**
     * @return array
     */
    public function getPageTemplate(): array
    {
        $exists = $this->page->body ?? [];
        $blocks = [];

        foreach ($this->defaultPageTemplate() as $i => $block) {
            $blocks[] = [
                'title' => $exists[$i]['title'] ?? $block,
                'text' => $exists[$i]['text'] ?? '',
            ];
        }
        return $blocks;
    }
}
