<?php


namespace App\Services;


use App\Models\Metric;
use Illuminate\Database\Eloquent\Model;

class MetricService
{
    /**
     * @param \App\Services\HasMetricsInterface $model
     */
    public function addView(HasMetricsInterface $model)
    {
        /** @var Model|\App\Services\Metricable $model */
        $model->addMetricValue(Metric::TYPE_VIEW);
    }
}
