<?php
namespace App\Services;

use Illuminate\Support\Collection;

class PageHandler
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @param int $parts
     * @return \Illuminate\Support\Collection
     */
    public function spliceCollection(Collection $collection, int $parts = 2): Collection
    {
        $chunk_size = round($collection->count() / $parts);
        return $collection->chunk($chunk_size);
    }
}
