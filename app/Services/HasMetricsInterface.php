<?php


namespace App\Services;


interface HasMetricsInterface
{
    public function metrics();
}
