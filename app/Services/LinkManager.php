<?php

namespace App\Services;

use App\Models\Service;

class LinkManager
{
    /**
     * @param \App\Models\Service $service
     * @return string
     */
    public function generateServiceUrl(Service $service)
    {
        if ($service->category && $service->slug) {
            if ($service->parent) {
                $params = [
                    'category' => $service->category->slug ?? '',
                ];
                $current = $service->parent;
                $slugs = [];
                while (true) {
                    array_unshift($slugs, $current->slug);
                    if (!$current->parent)
                        break;
                    $current = $current->parent;
                }
                $slugs[] = $service->slug;
                foreach ($slugs as $i => $slug)
                    $params["subcategory" . ($i + 1)] = $slug;

                return route('page.services.service.subcategory', $params);
            } else {
                return route('page.services.service', [
                    'category' => $service->category->slug ?? '',
                    'service' => $service->slug ?? '',
                ]);
            }
        }
        return '';
    }
}
