<?php


namespace App\Services\EditorJs;


class HrBlock implements BlockInterface
{
    /**
     * @param string $view
     * @param string[] $options
     * @return \Illuminate\View\View
     */
    public function render(string $view, array $options = []): string
    {
        return view("partials.blocks.hr." . $view)->render();
    }

    /**
     * @return array
     */
    public function options(): array
    {
        return [
            'settings' => [
                [
                    'view' => 'hr_1',
                    'name' => 'Львы',
                ],
                [
                    'view' => 'hr_2',
                    'name' => 'Венок',
                ],
                [
                    'view' => 'hr_3',
                    'name' => 'Линия',
                ],
            ],
        ];
    }
}
