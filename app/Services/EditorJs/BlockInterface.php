<?php


namespace App\Services\EditorJs;


interface BlockInterface
{
    /**
     * @param string $key
     * @param string[] $options
     * @return string
     */
    public function render(string $view, array $options = []): string;

    /**
     * @return array
     */
    public function options(): array;
}
