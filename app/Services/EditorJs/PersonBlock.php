<?php

namespace App\Services\EditorJs;

use App\Models\Person;

class PersonBlock implements BlockInterface
{
    /**
     * @param string $view
     * @param string[] $options
     * @return \Illuminate\View\View
     */
    public function render(string $view, array $options = []): string
    {
        if ($options['person_id'] ?? false)
            $person = Person::find($options['person_id']);
        else
            $person = Person::first();
        return view("partials.blocks.quote.quote", [
            'person' => $person,
            'text' => $options['text'] ?? 'Текст...',
        ])->render();
    }

    /**
     * @return array
     */
    public function options(): array
    {
        return [
            'persons' => Person::get(['id', 'name'])->map(function ($person) {
                return [
                    'id' => $person->id,
                    'name' => $person->name,
                    'face' => $person->getFirstMediaUrl('face'),
                ];
            }),
        ];
    }
}
