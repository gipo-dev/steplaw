<?php

namespace App\Services\Metric;

use Illuminate\Database\Eloquent\Model;

class MetricRepository
{
    /**
     * @var \App\Services\HasMetricsInterface|\App\Services\Metricable|Model
     */
    private $model;

    /**
     * MetricRepository constructor.
     * @param \App\Services\HasMetricsInterface $model
     */
    public function __construct(\App\Services\HasMetricsInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @param int $type
     * @return int
     */
    private function getValue(int $type): int
    {
        return $this->model->metrics()->where('type', $type)->first()->val ?? 0;
    }

    /**
     * @return int
     */
    public function views(): int
    {
        return $this->getValue(\App\Models\Metric::TYPE_VIEW);
    }
}
