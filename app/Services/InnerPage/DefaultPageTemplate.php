<?php

namespace App\Services\InnerPage;

use App\Models\Page;
use Orchid\Screen\Fields\Input;
use Orchid\Support\Facades\Layout;
use OrchidCommunity\TinyMCE\TinyMCE;

class DefaultPageTemplate extends AbstractPageTemplate
{
    /**
     * @return \Orchid\Screen\Layout[]
     */
    function getLayout(?Page $page): array
    {
        return [
            Layout::rows([
                Input::make('page.body.title')
                    ->value($this->page->body['title'] ?? '')
                    ->title('Заголовок'),
                TinyMCE::make('page.body.content')
                    ->value($this->page->body['content'] ?? '')
                    ->title('Контент'),
            ])->title('Страница'),
        ];
    }

    /**
     * @param \App\Models\Page|null $page
     * @return string
     */
    function getView(?Page $page): string
    {
        return 'pages.default';
    }
}
