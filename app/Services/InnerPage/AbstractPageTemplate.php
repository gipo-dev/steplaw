<?php

namespace App\Services\InnerPage;

use App\Models\Page;

abstract class AbstractPageTemplate
{
    /**
     * @return \Orchid\Screen\Layout[]
     */
    abstract function getLayout(?Page $page): array;

    abstract function getView(?Page $page): string;
}
