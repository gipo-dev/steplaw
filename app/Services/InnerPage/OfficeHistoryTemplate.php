<?php

namespace App\Services\InnerPage;

use App\Models\CasePage;
use App\Models\Page;
use App\Models\Review;
use App\Models\Thank;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Upload;
use Orchid\Support\Facades\Layout;
use OrchidCommunity\TinyMCE\TinyMCE;

class OfficeHistoryTemplate extends AbstractPageTemplate
{
    /**
     * @return \Orchid\Screen\Layout[]
     */
    function getLayout(?Page $page): array
    {
        return [
            Layout::rows([
                Group::make([
                    Input::make('page.body.title')
                        ->title('Заголовок H1')->required(),
                    Relation::make('page.body.cases.')
                        ->fromModel(Thank::class, 'name')
                        ->value($page->body['cases'] ?? [])
                        ->multiple()
                        ->title('Отзывы')->required(),
                ]),
            ]),
            $this->getSteps(),
            Layout::rows([
                TextArea::make('page.body.comment')
                    ->rows(5)->required()
                    ->title('Сейчас (зеленый фон)'),
            ]),
            $this->getFeatures(),
            Layout::columns([
                Layout::rows([
                    Input::make('page.body.certificates.title')
                        ->title('Заголовок H1')->required(),
                    TinyMCE::make("page.body.certificates.text")
                        ->value($this->page->body['certificates']['text'] ?? '')
                        ->title('Контент')->required(),
                ])->title('Наши сертификаты'),
                Layout::rows([
                    Input::make('page.body.clients.title')
                        ->title('Заголовок H1')->required(),
                    TinyMCE::make('page.body.clients.text')
                        ->title('Текст')->required(),
                ])->title('Наши клиенты'),
            ]),
            Layout::columns([
                Layout::rows([
                    Upload::make('page.body.certificates.files')
                        ->acceptedFiles('image/*')
                        ->groups('certificates')
                        ->title('Фото сертификатов'),
                ]),
                Layout::rows([
                    Upload::make('page.body.thanks.files')
                        ->acceptedFiles('image/*')
                        ->groups('thanks')
                        ->title('Фото благодарственных писем'),
                ]),
            ]),
        ];
    }

    /**
     * @return \Orchid\Screen\Layouts\Tabs
     */
    private function getSteps()
    {
        $tabs = [];
        for ($i = 0; $i < 6; $i++) {
            $tabs['Шаг ' . ($i + 1)] = Layout::rows([
                Input::make("page.body.steps.$i.title")
                    ->title('Заголовок')->required(),
                TinyMCE::make("page.body.steps.$i.content")
                    ->title('Контент')->required(),
            ]);
        }
        return Layout::tabs($tabs);
    }

    /**
     * @return \Orchid\Screen\Layouts\Tabs
     */
    private function getFeatures()
    {
        $features = [
            'stamp' => 'Иконка со штампом',
            'scales' => 'Иконка с весами',
            'certificate' => 'Иконка с сертификатом',
        ];
        $tabs = [];
        foreach ($features as $key => $title) {
            $tabs[$title] = Layout::rows([
                Input::make("page.body.features.$key.title")
                    ->title('Заголовок')->required(),
                TinyMCE::make("page.body.features.$key.content")
                    ->title('Контент')->required(),
            ]);
        }
        return Layout::tabs($tabs);
    }

    /**
     * @param \App\Models\Page|null $page
     * @return string
     */
    function getView(?Page $page): string
    {
        return 'pages.history';
    }
}
