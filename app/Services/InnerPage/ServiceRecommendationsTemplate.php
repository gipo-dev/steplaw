<?php

namespace App\Services\InnerPage;

use App\Models\Page;
use App\Models\Service;
use Orchid\Screen\Fields\Group;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Relation;
use Orchid\Screen\Fields\TextArea;
use Orchid\Support\Facades\Layout;

class ServiceRecommendationsTemplate extends AbstractPageTemplate
{

    function getLayout(?Page $page): array
    {
        $rows = [];

        if ($page->slug != 'page.blocks.recommendations' && $page->id == null) {
            $rows[] = Layout::rows([
                Relation::make('service')->fromClass(Service::class, 'name')
                    ->title('Выберите услугу, к которой будет привязан блок')->required(),

            ]);
        }


        return array_merge($rows, [

            Layout::columns([

                Layout::rows([

                    Input::make('page.body.block.1.title')->title('Блок #1')->required(),
                    TextArea::make('page.body.block.1.text')->rows(10)
                        ->maxlength(255)->required(),

                    Input::make('page.body.block.3.title')->title('Блок #3')->required(),
                    TextArea::make('page.body.block.3.text')->rows(10)
                        ->maxlength(255)->required(),

                ]),

                Layout::rows([

                    Input::make('page.body.block.2.title')->title('Блок #2')->required(),
                    TextArea::make('page.body.block.2.text')->rows(10)
                        ->maxlength(255)->required(),

                    Input::make('page.body.block.4.title')->title('Блок #4')->required(),
                    TextArea::make('page.body.block.4.text')->rows(10)
                        ->maxlength(255)->required(),

                ]),

                Layout::rows([

                    Input::make('page.body.block_attention.title')->title('Блок Внимание')->required(),
                    TextArea::make('page.body.block_attention.text')->rows(10)
                        ->maxlength(255)->required(),

                ]),

            ]),
        ]);
    }

    function getView(?Page $page): string
    {
        return 'pages.history';
    }
}
