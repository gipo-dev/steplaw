<?php


namespace App\Services;


use App\Models\Post;

class PostRelationService
{
    public static function getRelatedPosts(Post $post, int $count = 3)
    {
        /** @var \Illuminate\Support\Collection $related */
        $related = Post::whereHas('tags', function ($q) use ($post) {
            return $q->whereIn('name', $post->tags->pluck('name'));
        })
            ->where('id', '!=', $post->id)
            ->limit($count)
            ->get();
        if (count($related) < $count) {
            $related = $related->merge(Post::whereHas('categories', function ($q) use ($post) {
                $q->whereIn('post_categories.id', $post->categories->pluck('id'));
            })
                ->whereNotIn('posts.id', array_merge([$post->id], $related->pluck('id')->toArray()))
                ->limit($count - count($related))->get());
        }
        return $related->sortByDesc('created_at');
    }
}
