<?php


namespace App\Services\PageGenerator;


use App\Models\Person;

class PersonBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return view('partials.blocks.quote.quote', [
            'person' => Person::find($this->data['person_id']),
            'text' => $this->data['text'] ?? '',
        ])->render();
    }
}
