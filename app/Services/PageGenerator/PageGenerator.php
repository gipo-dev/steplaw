<?php

namespace App\Services\PageGenerator;

class PageGenerator implements PageGeneratorInterface
{
    protected const DEFAULT_BLOCK_TYPE = 'paragraph';

    /**
     * @var \App\Services\PageGenerator\BlockInterface[]
     */
    protected static $blocks = [
        'paragraph' => ParagraphBlock::class,
        'image' => ImageBlock::class,
        'header' => HeaderBlock::class,
        'list' => ListBlock::class,
        'quote' => WarningBlock::class,
        'table' => TableBlock::class,
        'map' => MapBlock::class,
        'hr' => HrBlock::class,
        'person' => PersonBlock::class,
        'embed' => EmbedBlock::class,
        'gallery' => GalleryBlock::class,
    ];

    /**
     * @param array $data
     * @return string
     * @throws \Exception
     */
    public function render(array $data): string
    {
        $rendered = [];

        foreach ($data['blocks'] ?? [] as $id => $block) {
            $rendered[] = $this->renderBlock($block, $id);
        }

        return implode(PHP_EOL, $rendered);
    }

    /**
     * @param $block
     * @param $id
     * @return string
     */
    private function renderBlock($block, $id): string
    {
        if (isset(static::$blocks[$block['type']]))
            $type = $block['type'];
        else
            $type = static::DEFAULT_BLOCK_TYPE;

        return (new static::$blocks[$type]($block['data'], $id))->render();
    }

    /**
     * @param $data
     * @return string
     */
    public function renderMap($data): string
    {
        $data = array_filter($data['blocks'] ?? [], function ($block) {
            return $block['type'] == 'header' && $block['data']['level'] == 2;
        });
        return $this->renderBlock([
            'type' => 'map',
            'data' => $data,
        ], -1);
    }
}
