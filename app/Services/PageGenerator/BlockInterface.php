<?php


namespace App\Services\PageGenerator;


interface BlockInterface
{
    /**
     * BlockInterface constructor.
     * @param array $data
     */
    public function __construct(array $data, int $id);

    /**
     * @return string
     */
    public function render(): string;
}
