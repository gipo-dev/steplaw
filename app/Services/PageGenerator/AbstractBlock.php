<?php


namespace App\Services\PageGenerator;


abstract class AbstractBlock implements BlockInterface
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var int
     */
    protected int $id;

    public function __construct(array $data, int $id)
    {
        $this->data = $data;
        $this->id = $id;
    }
}
