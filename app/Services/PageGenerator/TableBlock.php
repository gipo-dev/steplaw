<?php


namespace App\Services\PageGenerator;


class TableBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return view('partials.blocks.table.table', ['data' => $this->data])->render();
    }
}
