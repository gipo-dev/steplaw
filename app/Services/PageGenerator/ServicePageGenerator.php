<?php


namespace App\Services\PageGenerator;


class ServicePageGenerator
{
    /**
     * @param $html
     * @return array|string|string[]|null
     */
    public static function generate($html)
    {
        $html = preg_replace('/(<p>)(<a(.|\s)+<img(.|\s)+>)((.|\s)+)(<\/a>)/U', '<p class="external_link">$2<span class="text">$5</span>$7', $html);
        return $html;
    }
}
