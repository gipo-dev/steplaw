<?php


namespace App\Services\PageGenerator;


class HeaderBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return view('partials.blocks.header.header', ['data' => $this->data, 'id' => $this->id])->render();
    }
}
