<?php


namespace App\Services\PageGenerator;


class ParagraphBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return "<p>" . ($this->data['text'] ?? '') . "</p>";
    }
}
