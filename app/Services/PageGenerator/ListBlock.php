<?php


namespace App\Services\PageGenerator;


class ListBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        $style = $this->data['style'] ?? 'unordered';

        return view('partials.blocks.list.' . $style, ['data' => $this->data])->render();
    }
}
