<?php


namespace App\Services\PageGenerator;


use App\Models\Person;
use Illuminate\Support\Facades\View;

class EmbedBlock extends AbstractBlock
{
    private static $codes = [
        'youtube' => '<iframe width="560" height="315" src="{url}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
    ];

    /**
     * @inheritDoc
     */
    public function render(): string
    {
        try {
            $view = 'partials.blocks.embed.' . $this->data['service'];
            if (View::exists($view)) {
                return view($view, [
                    'code' => $this->data['embed'],
                    'text' => $this->data['caption'],
                ])->render();
            } else
                return '';
        } catch (\Exception $exception) {
            return '';
        }
    }
}
