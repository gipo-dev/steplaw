<?php


namespace App\Services\PageGenerator;


class ImageBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return view('partials.blocks.image.image', ['data' => $this->data, 'id' => $this->id])->render();
    }
}
