<?php

namespace App\Services\PageGenerator;

interface PageGeneratorInterface
{
    /**
     * Метод отрисовки страницы
     *
     * @param array $data
     * @return string
     */
    public function render(array $data): string;
}
