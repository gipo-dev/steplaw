<?php


namespace App\Services\PageGenerator;


class HrBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        $view = $this->data['view'] ?? 'hr_1';

        return view('partials.blocks.hr.' . $view)->render();
    }
}
