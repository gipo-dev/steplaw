<?php


namespace App\Services\PageGenerator;


class GalleryBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return view('partials.blocks.gallery.gallery', ['images' => $this->data, 'id' => $this->id])->render();
    }
}
