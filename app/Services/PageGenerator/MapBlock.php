<?php


namespace App\Services\PageGenerator;


class MapBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return view('partials.blocks.maps.map', ['data' => $this->data])->render();
    }
}
