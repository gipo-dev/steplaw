<?php


namespace App\Services\PageGenerator;


class WarningBlock extends AbstractBlock
{
    /**
     * @inheritDoc
     */
    public function render(): string
    {
        return view('partials.blocks.warning.warning', ['data' => $this->data])->render();
    }
}
