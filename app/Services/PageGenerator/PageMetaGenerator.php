<?php


namespace App\Services\PageGenerator;

use App\Models\InnerPage;

/**
 * Class PageMetaGenerator
 * @package App\Services\PageGenerator
 */
class PageMetaGenerator
{
    /**
     * @param string|null $routeName
     * @return \App\Models\InnerPage|null
     */
    public function getPage(string $routeName = null): ?InnerPage
    {
        if (!$routeName)
            $routeName = request()->route()->getName();

        return InnerPage::search($routeName);
    }

    /**
     * @param string|null $routeName
     */
    public function setMetas(string $routeName = null)
    {
        $page = $this->getPage($routeName);
        if (!$page)
            return;

        \Meta::title($page->meta_title);
        \Meta::set('description', $page->meta_description);
    }
}
